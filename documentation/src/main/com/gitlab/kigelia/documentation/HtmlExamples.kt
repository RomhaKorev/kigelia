package com.gitlab.kigelia.documentation

import com.gitlab.kigelia.core.dsl.common.Renderer
import com.gitlab.kigelia.core.dsl.html.HtmlStructure
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.documentation.exampletodoc.DocConfiguration
import com.gitlab.kigelia.documentation.exampletodoc.DocumentationGenerator
import com.gitlab.kigelia.documentation.exampletodoc.serializers.HtmlSerializer
import com.gitlab.kigelia.documentation.resources.FileSystem

abstract class HtmlExamples: DocumentationGenerator<HtmlStructure.HtmlBody>(
    DocConfiguration.fromEnvProperties(),
    FileSystem(),
    {snippet -> HtmlSerializer(snippet) }
) {
    override fun `the snippet`(f: HtmlStructure.HtmlBody.() -> Unit): ExampleBuilder {
        return ExampleBuilder(
            {
                val u = mutableListOf <Renderer>()
                html {
                    body {
                        f()
                        this.forEach { u.add(it) }
                    }
                }
                u.joinToString("\n") {it.render()}
            }, exampleSerializer)
    }
}

