package com.gitlab.kigelia.documentation.exampletodoc

import com.gitlab.kigelia.documentation.example.ExampleType
import com.gitlab.kigelia.documentation.example.Snippet
import com.gitlab.kigelia.documentation.exampletodoc.serializers.ExampleSerializer
import com.gitlab.kigelia.documentation.resources.FileResources
import com.gitlab.kigelia.documentation.sourcecode.GetSourceCode
import org.assertj.core.api.Assertions.assertThat


fun String.remarge(): String {
    return this.split("\n").joinToString("\n") { "|$it"}
}

abstract class DocumentationGenerator<T>(val configuration: DocConfiguration,
                                         repository: FileResources,
                                         val exampleSerializer: (snippet: Snippet) -> ExampleSerializer
    ) {
    val documentation = Documentation(configuration, repository)
    val getSourceCode = GetSourceCode(this, configuration, repository)

    inner class ExampleBuilder(private val methodToTest: () -> String,
                               private val exampleSerializer: (snippet: Snippet) -> ExampleSerializer
    ) {
        infix fun `will produce`(info: () -> String): ExampleBuilder {
            assertThat(methodToTest()).containsIgnoringWhitespaces(info())
            return this
        }

        infix fun `will be documented`(f: Snippet.() -> Unit) {
            if (!configuration.enabled)
                return

            val info = getSourceCode.getExampleInfo()

            val snippet = Snippet(
                subject = info.subject,
                category = "",
                title = "",
                description = "",
                sources = info.sources,
                highlight = "",
                type = ExampleType.Image,
                methodToTest = methodToTest
            )

            snippet.f()

            documentation.save(exampleSerializer(snippet))
        }
    }

    abstract fun `the snippet`(f: T.() -> Unit): ExampleBuilder
}


