package com.gitlab.kigelia.documentation.exampletodoc.serializers

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.elements.div
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.documentation.example.ExampleType
import com.gitlab.kigelia.documentation.example.Snippet
import com.gitlab.kigelia.documentation.exampletodoc.ExtraFileToCreate
import java.io.File

class HtmlSerializer(override val snippet: Snippet): ExampleSerializer {
    override fun resultToDisplay(file: File): Pair<String, List<ExtraFileToCreate>> {
        if (snippet.type == ExampleType.Nothing) {
            return "" to emptyList()
        }
        lateinit var u: HtmlElement
        lateinit var content: HtmlElement
        html {
            content = body {
                -snippet.methodToTest()
            }
        }
        html {
            u = body {
                val d = div {
                    "style"("padding: 16px;")
                }
                content.forEach {
                    d.add(it)
                }
            }
        }
        val contentHtml = snippet.methodToTest()
        val bodyHtml = u.render()
        val result = mutableListOf<String>()
        if (snippet.type == ExampleType.Image) {
            result.add("""
                |<iframe onload="{this.style.height = this.contentWindow.document.documentElement.scrollHeight + 'px';}" style="width: 100%; height: 50px; overflow: hidden; border: none" srcdoc='<html><head><link rel="stylesheet" href="https://bulma.io/css/bulma-docs.min.css?v=202304030945"></head>${bodyHtml}'></iframe>
                """
                .trimIndent().split("\n").joinToString("") { it.trim() }
            )
        }
        if (snippet.type in ExampleType.Code or ExampleType.CodeAndImage) {
            result.add("""
                |```html
                ${contentHtml.split("\n").joinToString("\n") { "|$it"}}
                |```
            """.trimIndent())
        }
        return result.joinToString("\n").trimMargin() to emptyList()
    }

}