package com.gitlab.kigelia.documentation.exampletodoc.serializers

import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.invoke
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.documentation.example.ExampleType
import com.gitlab.kigelia.documentation.example.Snippet
import com.gitlab.kigelia.documentation.exampletodoc.ExtraFileToCreate
import com.gitlab.kigelia.documentation.exampletodoc.remarge
import java.io.File
import java.nio.file.Path

fun Path.invariant(): String {
    return this.toFile().invariantSeparatorsPath
}

class SvgSerializer(override val snippet: Snippet): ExampleSerializer {
    override fun resultToDisplay(file: File): Pair<String, List<ExtraFileToCreate>> {
        if (snippet.type == ExampleType.Nothing) {
            return "" to emptyList()
        }

        val result = mutableListOf<String>()
        val extraFiles = mutableListOf<ExtraFileToCreate>()
        if (snippet.type in ExampleType.Image or ExampleType.CodeAndImage) {
            val name = "${snippet.subject}-${snippet.category}-${snippet.title}".replace(" ", "_")
            val image = createImageIn(file.parent, name)
            val uri = Path.of("./${name.lowercase()}.svg")
            result.add("""
                |![${name}](${uri.invariant()} "${snippet.title}")
            """.trimIndent())
            extraFiles.add(image)
        }
        if (snippet.type in ExampleType.Code or ExampleType.CodeAndImage) {
            result.add("""
                |```xml
                ${
                "" {
                    - snippet.methodToTest()
                }.render().split("\n").joinToString("\n") {"|$it"}
                }
                |```
                """.trimMargin().remarge())
        }
        return result.joinToString("\n").trimMargin() to extraFiles
    }

    private fun createImageIn(parent: String, name: String): ExtraFileToCreate {
        val imageFilename = "${name.lowercase()}.svg"
        val path = Path.of(parent, imageFilename)
        return ExtraFileToCreate(
            path,
            svg {
                rect {
                    x(0)
                    y(0)
                    width(200)
                    height(200)
                    "rx"(10)
                    fill("white")
                }

                g {
                    stroke("black")
                    noFill()
                    -snippet.methodToTest()
                }

                if (geometry() == Geometry()) {
                    geometry {
                        x(0)
                        y(0)
                        width(200)
                        height(200)
                    }
                    viewbox {
                        minx(0)
                        miny(0)
                        width(200)
                        height(200)
                    }
                }
            }.render()
        )
    }
}


