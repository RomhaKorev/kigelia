package com.gitlab.kigelia.documentation.exampletodoc.serializers

import com.gitlab.kigelia.documentation.example.Snippet
import com.gitlab.kigelia.documentation.exampletodoc.ExtraFileToCreate
import java.io.File

interface ExampleSerializer {
    val snippet: Snippet
    fun sourceTodisplay() : String {
        return snippet.sources.joinToString("\n") { "|$it" }
    }
    fun resultToDisplay(file: File) : Pair<String, List<ExtraFileToCreate>>
}