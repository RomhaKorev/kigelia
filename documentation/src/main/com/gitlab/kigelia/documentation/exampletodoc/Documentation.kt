package com.gitlab.kigelia.documentation.exampletodoc

import com.gitlab.kigelia.documentation.exampletodoc.serializers.ExampleSerializer
import com.gitlab.kigelia.documentation.resources.FileResources
import java.io.File
import java.nio.file.Path

class Documentation(private val configuration: DocConfiguration, val repository: FileResources) {
    init {
        if (configuration.enabled) {
            repository.create(configuration.output)
            repository.create(configuration.output.resolve("docs"))
            val mkdocsFile = Path.of(Documentation::class.java.getResource("/mkdocs.yml")!!.toURI())
            val indexFile = Path.of(Documentation::class.java.getResource("/index.md")!!.toURI())
            repository.copy(mkdocsFile, configuration.output.resolve("mkdocs.yml"))
            repository.copy(indexFile, configuration.output.resolve("docs/index.md"))
        }
    }


    fun save(serializer: ExampleSerializer) {
        if (!configuration.enabled)
            return
        val currentPath = configuration.output.resolve("docs").resolve(serializer.snippet.subject.normalize())
        repository.create(currentPath)
        val f = File(currentPath.toFile(), "${serializer.snippet.category.lowercase()}.md")
        val formattedSources = serializer.sourceTodisplay()
        val (resultTodisplay, extraFiles) = serializer.resultToDisplay(f)
        repository.appendTo(f.toPath(),
        """
            |### ${serializer.snippet.title}
            ${serializer.snippet.description.split("\n").joinToString("\n") { "|$it" }}
            |```kotlin${if (serializer.snippet.highlight.isNotBlank()) """ hl_lines="${serializer.snippet.highlight}"""" else ""}
            $formattedSources
            |```
            ${resultTodisplay.split("\n").joinToString("\n") { "|$it"} }
            |
            |
            """.trimMargin("|")
            )

        extraFiles.forEach {
            (path, content) ->
            repository.write(path, content)
        }
    }
}

fun String.normalize(): String {
    return replace(" ", "_").lowercase()
}