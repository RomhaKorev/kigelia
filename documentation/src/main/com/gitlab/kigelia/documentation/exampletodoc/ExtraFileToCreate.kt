package com.gitlab.kigelia.documentation.exampletodoc

import java.nio.file.Path

data class ExtraFileToCreate(val path: Path, val content: String)