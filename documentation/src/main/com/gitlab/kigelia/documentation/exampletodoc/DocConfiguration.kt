package com.gitlab.kigelia.documentation.exampletodoc

import java.nio.file.Path

class DocConfiguration(private val outputEnv: String?, private val srcEnv: String?) {
    val enabled = outputEnv != null && srcEnv != null
            && outputEnv.isNotBlank() && srcEnv.isNotBlank()
    val output: Path
        get() = Path.of(outputEnv)
    val root: Path
        get() = Path.of(srcEnv)

    companion object {
        fun fromEnvProperties(): DocConfiguration {
            val outputEnv: String? = System.getProperty("output-dir")
            val srcEnv: String? = System.getProperty("root-dir")
            return DocConfiguration(outputEnv, srcEnv)
        }
    }
}