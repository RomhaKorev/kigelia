package com.gitlab.kigelia.documentation.example

enum class ExampleType {
    Code,
    CodeAndImage,
    Image,
    Nothing;

    infix fun or(other: ExampleType): List<ExampleType> {
        return listOf(this, other)
    }
}