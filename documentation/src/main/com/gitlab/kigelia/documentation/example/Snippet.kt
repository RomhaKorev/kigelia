package com.gitlab.kigelia.documentation.example

class Snippet(val subject: String, category: String, title: String, val sources: List<String>, description: String, highlight: String, type: ExampleType, val methodToTest: () -> String) {
    private var aCategory: String = category
    private var aTitle: String = title
    private var aDescription: String = description
    private var aHighlight: String = highlight
    private var aType: ExampleType = type

    val category: String
        get() = aCategory
    val title: String
        get() = aTitle
    val description: String
        get() = aDescription
    val highlight: String
        get() = aHighlight
    val type: ExampleType
        get() = aType

    fun `in the category`(s: String) {
        aCategory = s
    }

    fun titled(s: String) {
        aTitle = s
    }

    fun `described as`(s: String) {
        aDescription = s
    }

    fun `highlight the lines`(lines: String) {
        aHighlight = lines
    }

    fun `displayed as`(t: ExampleType) {
        aType = t
    }
}