package com.gitlab.kigelia.documentation

import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.TestMethodOrder

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
annotation class ExamplesFor(val subject: String, val description: String)