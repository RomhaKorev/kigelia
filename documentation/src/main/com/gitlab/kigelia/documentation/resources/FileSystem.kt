package com.gitlab.kigelia.documentation.resources

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption

class FileSystem: FileResources {
    override fun create(file: Path) {
        Files.createDirectories(file)
    }

    override fun copy(source: Path, destination: Path) {
        Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING)
    }

    override fun write(path: Path, content: String) {
        path.toFile().writeText(content)
    }

    override fun appendTo(path: Path, content: String) {
        path.toFile().appendText(content)
    }

    override fun read(path: Path): String {
        return path.toFile().readText()
    }
}