package com.gitlab.kigelia.documentation.resources

import java.nio.file.Path

interface FileResources {
    fun create(file: Path)
    fun copy(source: Path, destination: Path)
    fun write(path: Path, content: String)
    fun appendTo(path: Path, content: String)
    fun read(path: Path): String
}