package com.gitlab.kigelia.documentation

import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.invoke
import com.gitlab.kigelia.documentation.exampletodoc.DocConfiguration
import com.gitlab.kigelia.documentation.exampletodoc.DocumentationGenerator
import com.gitlab.kigelia.documentation.exampletodoc.serializers.SvgSerializer
import com.gitlab.kigelia.documentation.resources.FileSystem

abstract class SvgExamples: DocumentationGenerator<SvgElement>(
    DocConfiguration.fromEnvProperties(),
    FileSystem(),
    {snippet -> SvgSerializer(snippet) }
) {

    override fun `the snippet`(f: SvgElement.() -> Unit): ExampleBuilder {
        return ExampleBuilder({
            "" {
                f()
            }.render()
        }, exampleSerializer)
    }
}