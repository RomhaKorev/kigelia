package com.gitlab.kigelia.documentation.sourcecode

data class SourceCode(val subject: String, val name: String, val sources: List<String>)