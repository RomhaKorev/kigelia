package com.gitlab.kigelia.documentation.sourcecode

import com.gitlab.kigelia.documentation.exampletodoc.DocConfiguration
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.resources.FileResources
import io.github.classgraph.ClassGraph
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.kotlinFunction

class GetSourceCode(val parent: Any, val configuration: DocConfiguration, val repository: FileResources) {
        fun getExampleInfo(): SourceCode {
            val annotation = Example::class

            val (u, caller) = findCaller(annotation)

            val metainfo = (parent::class.annotations.firstOrNull { it is ExamplesFor } !!) as ExamplesFor

            val p = caller.className.replace(".", "/")
            val path = "src/test/kotlin/$p.kt"
            val mainPath = configuration.root.resolve(path)
            val sources = extractSources(repository.read(mainPath), caller.lineNumber)
            val name = u.name.replace("`", "")

            return SourceCode(metainfo.subject, name, sources)
        }

        fun findCaller(annotation: KClass<Example>): Pair<KFunction<*>, StackTraceElement> {
            val pkg = annotation.java.`package`.name
            val annotationName = annotation.java.canonicalName

            val stack = Thread.currentThread().stackTrace
            var i = 1
            lateinit var result: Pair<KFunction<*>, StackTraceElement>
            while (i != stack.size) {
                val caller = stack[i]
                val allFunctions = ClassGraph().acceptPackages(pkg).enableAllInfo().scan().use { scanResult ->
                    scanResult.getClassesWithMethodAnnotation(annotationName).flatMap { routeClassInfo ->
                        routeClassInfo.methodInfo.filter { function ->
                            function.hasAnnotation(annotation.java) && function.name == caller.methodName && function.className == caller.className
                        }.mapNotNull { method ->
                            method.loadClassAndGetMethod().kotlinFunction!!
                        }
                    }
                }
                if (allFunctions.isNotEmpty()) {
                    result = allFunctions.first() to caller
                    break
                }
                ++i
            }
            return result
        }

        companion object {
            fun extractSources(content: String, line: Int): List<String> {
                val lines = content.split("\n")
                var bracketsCount = 1
                var function = ""
                val sources =
                    lines.drop(line).filter { !it.endsWith("irrelevant", ignoreCase = true) }.joinToString("\n")
                run breaking@{
                    sources.forEach { ch ->
                        if (ch == '{') {
                            bracketsCount += 1
                        }
                        if (ch == '}') {
                            bracketsCount -= 1
                        }
                        if (bracketsCount == 0)
                            return@breaking
                        function += ch
                    }
                }
                return function.trimIndent().split("\n")
            }
        }
}