# Kigelia

Another framework to build DSL, SVG images, charts and other stuff

## How to use

### Maven

Add to your pom.xml:
```
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/33993395/packages/maven</url>
    </repository>
</repositories>
```

And in the dependencies:
```
<dependency>
    <groupId>com.gitlab.kigelia</groupId>
    <artifactId>core</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>com.gitlab.kigelia.core</groupId>
    <artifactId>dsl</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
