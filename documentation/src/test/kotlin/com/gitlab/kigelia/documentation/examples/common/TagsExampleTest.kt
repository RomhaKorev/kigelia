package com.gitlab.kigelia.documentation.examples.common

import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.HtmlExamples
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.example.ExampleType

@ExamplesFor(
    subject = "Common",
    description = "How to use the DSL to create xml-like content")
class TagsExampleTest: HtmlExamples() {

    @Example
    fun exampleTag1() = `the snippet` {
        "g" {}
    } `will produce` {
        """<g />"""
    } `will be documented` {
        `in the category`("Tags")
        titled("""Empty tag""")
        `highlight the lines`("")
        `displayed as`(ExampleType.Code)
    }

    @Example
    fun exampleTag2() = `the snippet` {
        "g" {
            "rect" {}
            "path" {}
        }
    } `will produce` {
        """
            <g>
                <rect />
                <path />
            </g>
        """
    } `will be documented` {
        `in the category`("Tags")
        titled("""Tag with children""")
        `highlight the lines`("")
        `displayed as`(ExampleType.Code)
    }

    @Example
    fun exampleTag4() = `the snippet` {
        "p" {
            - "foobar"
        }
    } `will produce` {
        """
            <p>
                foobar
            </p>
        """
    } `will be documented` {
        `in the category`("Tags")
        titled("""Tag with text""")
        `highlight the lines`("2")
        `displayed as`(ExampleType.Code)
    }
}