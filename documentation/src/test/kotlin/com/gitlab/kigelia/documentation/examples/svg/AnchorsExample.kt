package com.gitlab.kigelia.documentation.examples.svg

import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Rectangle
import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.SvgExamples

@ExamplesFor(
    subject = "SVG",
    description = "How to use the DSL to create SVG")
class AnchorsExample: SvgExamples() {
    @Example
    fun example1() = `the snippet` {
        Rectangle(Point(50, 50), Size(70, 100)).draw {
            id("source")
            stroke("black")
        }
        circle {
            r(10)
            val p = "source".top()
            cx(p.x)
            cy(p.y)
            stroke("red")
        }
    } `will produce` {
        """<circle r="10" cx="85" cy="50" stroke="red" />"""
    } `will be documented` {
        `in the category`("Anchors")
        titled("""How to get top anchor position""")
        `highlight the lines`("2")
    }
}
