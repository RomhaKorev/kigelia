package com.gitlab.kigelia.documentation.examples.html.bulma

import com.gitlab.kigelia.core.dsl.html.bulma.bulma
import com.gitlab.kigelia.core.dsl.html.elements.figure
import com.gitlab.kigelia.core.dsl.html.elements.img
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.HtmlExamples
import com.gitlab.kigelia.documentation.ExamplesFor
import org.junit.jupiter.api.Order

@ExamplesFor(
    subject = "html/Bulma",
    description = "How to use the DSL to create html content using Bulma CSS Framework")
class BulmaCardExample: HtmlExamples() {
    @Order(1)
    @Example
    fun exampleCard() = `the snippet` {
        bulma {
            card {
                title("John Smith")
                subtitle("@johnsmith")
            }
        }
    } `will produce` {
        """
        <div class="card">
            <div class="card-content">
                <div class="media">
                    <div class="media-content">
                        <p class="title is-4">John Smith</p>
                        <p class="subtitle is-6">@johnsmith</p>
                    </div>
                </div>
            </div>
        </div>
        """
    } `will be documented` {
        `in the category`("Card")
        titled("""Title and subtitle""")
        `highlight the lines`("3-4")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Image)
    }

    @Order(2)
    @Example
    fun `example content`() = `the snippet` {
        bulma {
            card {
                title("John Smith")
                subtitle("@johnsmith")

                content {
                    - """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                    <a href="#">#css</a> <a href="#">#responsive</a>
                    <br>
                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>"""
                }
            }
        }
    } `will produce` {
        """
        <div class="card">
            <div class="card-content">
                <div class="media">
                    <div class="media-content">
                        <p class="title is-4">John Smith</p>
                        <p class="subtitle is-6">@johnsmith</p>
                    </div>
                </div>
                <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                    <a href="#">#css</a> <a href="#">#responsive</a>
                    <br>
                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                </div>
            </div>
        </div>
        """
    } `will be documented` {
        titled("Content")
        `in the category`("Card")
    }

    @Order(3)
    @Example
    fun exampleCardWithPicto() = `the snippet` {
        bulma {
            card {
                title("John Smith")
                subtitle("@johnsmith")
                leftMedia {
                    figure {
                        cssClass("image", "is-48x48")
                        img("https://bulma.io/images/placeholders/96x96.png") {
                            alt("Placeholder image")
                        }
                    }
                }

                content {
                    - """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                    <a href="#">#css</a> <a href="#">#responsive</a>
                    <br>
                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>"""
                }
            }
        }
    } `will produce` {
        """
        <div class="card">
            <div class="card-content">
                <div class="media">
                    <div class="media-left">
                        <figure class="image is-48x48">
                            <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image" />
                        </figure>
                    </div>
                    <div class="media-content">
                        <p class="title is-4">John Smith</p>
                        <p class="subtitle is-6">@johnsmith</p>
                    </div>
                </div>
                <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                    <a href="#">#css</a> <a href="#">#responsive</a>
                    <br>
                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                </div>
            </div>
        </div>
        """
    } `will be documented` {
        `in the category`("Card")
        titled("""Left media""")
        `highlight the lines`("5-12")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Image)
    }
}


/*
<div class="card">
  <div class="card-image">
    <figure class="image is-4by3">
      <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
    </figure>
  </div>
  <div class="card-content">
    <div class="media">
      <div class="media-left">
        <figure class="image is-48x48">
          <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
        </figure>
      </div>
      <div class="media-content">
        <p class="title is-4">John Smith</p>
        <p class="subtitle is-6">@johnsmith</p>
      </div>
    </div>

    <div class="content">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Phasellus nec iaculis mauris. <a>@bulmaio</a>.
      <a href="#">#css</a> <a href="#">#responsive</a>
      <br>
      <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
    </div>
  </div>
</div>
 */