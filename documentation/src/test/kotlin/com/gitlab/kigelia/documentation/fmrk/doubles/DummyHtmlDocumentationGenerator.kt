package com.gitlab.kigelia.documentation.fmrk.doubles

import com.gitlab.kigelia.core.dsl.html.HtmlStructure
import com.gitlab.kigelia.core.dsl.html.elements.div
import com.gitlab.kigelia.core.dsl.html.elements.p
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.documentation.*
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.exampletodoc.DocConfiguration
import com.gitlab.kigelia.documentation.exampletodoc.DocumentationGenerator
import com.gitlab.kigelia.documentation.exampletodoc.serializers.HtmlSerializer
import com.gitlab.kigelia.documentation.resources.FileResources
import org.junit.jupiter.api.Disabled

@ExamplesFor(
    subject = "foobar",
    description = "Foo Bar")
@Disabled("Dummy tests used by documentation generator unit tests. Do not enable")
class DummyHtmlDocumentationGenerator(p: DocConfiguration, fs: FileResources): DocumentationGenerator<HtmlStructure.HtmlBody>(p, fs, { snippet -> HtmlSerializer(snippet) }) {
    @Example
    fun exampleWithHtml() = `the snippet` {
        div {
            p {
                - "foobar"
            }
        }
    } `will produce` {
        """
            <div>
                <p>foobar</p>
            </div>
        """
    } `will be documented` {
        `in the category`("Foo")
        titled("""example 2 elements""")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Image)
    }


    @Example
    fun exampleWithIrrelevant() = `the snippet` {
        div {
            div {// irrelevant
                p {
                    -"foobar"
                }
            } // irrelevant
        }
    } `will produce` {
        """
            <div>
                <div>
                    <p>foobar</p>
                </div>
            </div>
        """
    } `will be documented` {
        `in the category`("Foo")
        titled("""example 2 elements""")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Image)
    }

    override fun `the snippet`(f: HtmlStructure.HtmlBody.() -> Unit): ExampleBuilder {
        return ExampleBuilder(
            {
                html {
                    body {
                        f()
                    }
                }.render()
            }, exampleSerializer)
    }
}