package com.gitlab.kigelia.documentation.fmrk

import com.gitlab.kigelia.documentation.example.ExampleType
import com.gitlab.kigelia.documentation.exampletodoc.serializers.HtmlSerializer
import com.gitlab.kigelia.documentation.example.Snippet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

class HtmlSerializerTest {
    val snippet = Snippet(
        subject = "the subject",
        category = "the category",
        title = "the title",
        sources = listOf("sources line 1", "sources line 2"),
        description = "the description",
        highlight = "1",
        type = ExampleType.Image,
        methodToTest = { """
                <div>
                    <p>foobar</p>
                </div>
            """.trimIndent()}
    )

    @Test
    fun `should insert html render`() {
        val serializer = HtmlSerializer(snippet)
        val file = File("/foo/bar/doc.md")
        val (md, _) = serializer.resultToDisplay(file)

        assertThat(md).contains("<iframe")
    }

    @Test
    fun `should insert raw html`() {
        val serializer = HtmlSerializer(snippet.copy(type = ExampleType.Code))
        val file = File("/foo/bar/doc.md")
        val (md, _) = serializer.resultToDisplay(file)

        assertThat(md).contains("""
            ```html
            <div>
                <p>foobar</p>
            </div>
            ```
        """.trimIndent())
    }

    @Test
    fun `insert example with no snippet`() {
        val serializer = HtmlSerializer(snippet.copy(type = ExampleType.Nothing))
        val file = File("/foo/bar/doc.md")
        val (md, _) = serializer.resultToDisplay(file)

        assertThat(md).doesNotContain(
            """```html"""
        )
    }
}