package com.gitlab.kigelia.documentation.examples.common

import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.HtmlExamples
import com.gitlab.kigelia.documentation.ExamplesFor

@ExamplesFor(
    subject = "Common",
    description = "How to use the DSL to create xml-like content")
class CommonExampleTest: HtmlExamples() {
    private fun fibonacci(v: Int): String = "1, 1, 2, 3, 5"
    @Example
    fun exampleTag3() = `the snippet` {
        "text" {
            val i = 5
            - "${fibonacci(i)}"
        }
    } `will produce` {
        """
        <text>
            1, 1, 2, 3, 5
        </text>
        """
    } `will be documented` {
        `in the category`("Tags")
        titled("""The bracket is a Kotlin anonymous function""")
        `highlight the lines`("")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Code)
    }
}