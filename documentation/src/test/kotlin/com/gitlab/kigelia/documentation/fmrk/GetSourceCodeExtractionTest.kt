package com.gitlab.kigelia.documentation.fmrk

import com.gitlab.kigelia.documentation.sourcecode.GetSourceCode
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class GetSourceCodeExtractionTest {
    @Test
    fun `should remove lines marked as irrelevant`() {
        Assertions.assertThat(
            GetSourceCode.extractSources(
            """
            /*
            offset
            */
            fun example2() = `the snippet` {
                rect {
                    id(12) // irrelevant
                }
            }
            
            fun example3() = `the snippet` {
                rect {
                    // DON'T
                }
            }
            """.trimIndent(), 4
            )
        ).isEqualTo("""
            rect {
            }
        """.trimIndent().split("\n"))
    }


    @Test
    fun `should extract sources`() {
        Assertions.assertThat(
            GetSourceCode.extractSources(
            """
            /*
            offset
            */
            fun example2() = `the snippet` {
                rect {
                    id(12)
                }
            }
            """.trimIndent(), 4
            )
        ).isEqualTo("""
            rect {
                id(12)
            }
        """.trimIndent().split("\n"))
    }

    @Test
    fun `should extract sources when curly brackets are on same line`() {
        Assertions.assertThat(
            GetSourceCode.extractSources(
            """
            /*
            offset
            */
            fun example2() = `the snippet` {
                rect {}
            }
            """.trimIndent(), 4
            )
        ).isEqualTo("""
            rect {}
        """.trimIndent().split("\n"))
    }

    @Test
    fun `should extract sources when using snippet dsl`() {
        Assertions.assertThat(
            GetSourceCode.extractSources(
            """
            /*
            offset
            */
            `the snippet` {
                bulma {
                    div {}
                }
            } `will produce` {
                ""${'"'}
                <div />
                ""${'"'}.trimIndent()
            } `will be documented` {
                titled("Sizes")
            }
            """.trimIndent(), 4
            )
        ).isEqualTo("""
            bulma {
                div {}
            }
        """.trimIndent().split("\n"))
    }

    @Test
    fun `should continue if sources not found`() {
        Assertions.assertThat(
            GetSourceCode.extractSources(
                """
                """.trimIndent(), 4
            )
        ).isEqualTo(listOf(""))
    }
}