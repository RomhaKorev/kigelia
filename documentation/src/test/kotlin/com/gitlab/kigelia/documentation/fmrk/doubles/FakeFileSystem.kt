package com.gitlab.kigelia.documentation.fmrk.doubles

import com.gitlab.kigelia.documentation.resources.FileResources
import java.io.File
import java.nio.file.Path

class FakeFileSystem: FileResources {

    val created = mutableListOf<Path>()
    val copied = mutableListOf<Pair<Path, Path>>()
    val written = mutableMapOf<Path, String>()

    val files = mutableMapOf<Path, String>()

    override fun create(file: Path) {
        created.add(file)
    }

    fun create(file: Path, content: String) {
        created.add(file)
        files[file] =  content
    }

    override fun copy(source: Path, destination: Path) {
        copied.add(source to destination)
    }

    override fun write(path: Path, content: String) {
        written[path] = content
    }

    override fun appendTo(path: Path, content: String) {
        written[path] = written.getOrDefault(path, "") + content
    }

    override fun read(path: Path): String {
        if (files.contains(path))
            return files[path]!!
        if (path.toString().contains("DummyDocumentationGenerator.kt")) {
            // Don't forget to add this file to testResource in pom.xml
            val file = File(this.javaClass.getResource("/DummyDocumentationGenerator.kt")!!.file)
            return file.readText()
        }
        if (path.toString().contains("DummyHtmlDocumentationGenerator.kt")) {
            // Don't forget to add this file to testResource in pom.xml
            val file = File(this.javaClass.getResource("/DummyHtmlDocumentationGenerator.kt")!!.file)
            return file.readText()
        }
        return written[path] ?: ""
    }
}