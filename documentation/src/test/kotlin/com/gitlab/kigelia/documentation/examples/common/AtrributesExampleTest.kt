package com.gitlab.kigelia.documentation.examples.common

import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.SvgExamples

@ExamplesFor(
    subject = "Common",
    description = "How to use the DSL to create xml-like content")
class AtrributesExampleTest: SvgExamples() {

    @Example
    fun example0() = `the snippet` {
        g {
            "stroke"("red")
        }
    } `will produce` {
        """<g stroke="red" />"""
    } `will be documented` {
        `in the category`("Attributes")
        titled("Adding an attribute")
        `highlight the lines`("2")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Code)
    }

    @Example
    fun example1() = `the snippet` {
        "rect" {
            "x"("50")
            "y"("50")
            "stroke"("red")
            noFill()
            "width"("100px")
            val width = attribute("width").value()
            "height"(width)
        }
    } `will produce` {
        """<rect x="50" y="50" stroke="red" fill="none" width="100px" height="100px" />"""
    } `will be documented` {
        `in the category`("Attributes")
        titled("Retrieving an attribute")
        `highlight the lines`("6")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.CodeAndImage)

    }

    @Example
    fun example2() = `the snippet` {
        "g" {
            rect {
                id("source")
                "x"(20)
                "y"(20)
                "width"("100")
                "height"("50")
                stroke("red")
                noFill()
            }
            rect {
                "x"(40)
                "y"(100)
                "width"("source".valueOf("width"))
                "height"("source".valueOf("height"))
                stroke("green")
                noFill()
            }
        }
    } `will produce` {
        """
        <g>
            <rect id="source" x="20" y="20" width="100" height="50" stroke="red" fill="none" />
            <rect x="40" y="100" width="100" height="50" stroke="green" fill="none" />
        </g>"""
    } `will be documented` {
        `in the category`("Attributes")
        titled("""Attribute of a sibling""")
        `highlight the lines`("14 15")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.CodeAndImage)
    }
}