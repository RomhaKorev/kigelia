package com.gitlab.kigelia.documentation.examples.svg

import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.SvgExamples
import com.gitlab.kigelia.documentation.example.ExampleType

@ExamplesFor(
    subject = "SVG",
    description = "How to use the SVG DSL")
class GettingStarted: SvgExamples() {
    @Example
    fun `create a svg`() = `the snippet` {
        svg {

        }
    } `will be documented` {
        titled("Create a svg content")
        `in the category`("Getting started")
        `described as`("Use the function `svg {}` to start using the DSL")
        `displayed as`(ExampleType.Nothing)
    }
}