package com.gitlab.kigelia.documentation.fmrk

import com.gitlab.kigelia.documentation.example.ExampleType
import com.gitlab.kigelia.documentation.example.Snippet
import com.gitlab.kigelia.documentation.exampletodoc.serializers.SvgSerializer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Path

fun Snippet.copy(
    subject: String = this.subject,
    category: String = this.category,
    title: String = this.title,
    sources: List<String> = this.sources,
    description: String = this.description,
    highlight: String = this.highlight,
    type: ExampleType = this.type,
    methodToTest: () -> String = this.methodToTest): Snippet {
    return Snippet(subject, category, title, sources, description, highlight, type, methodToTest)
}

class SvgSerializerTest {

    val snippet = Snippet(
        subject = "the subject",
        category = "the category",
        title = "the title",
        sources = listOf("sources line 1", "sources line 2"),
        description = "the description",
        highlight = "1",
        type = ExampleType.Image,
        methodToTest = { """
                result line 1
                result line 2
            """.trimIndent()}
    )

    @Test
    fun `should build sources`() {
        val serializer = SvgSerializer(snippet)

        assertThat(serializer.sourceTodisplay())
            .isEqualTo("""
                |sources line 1
                |sources line 2
            """.trimIndent())
    }

    @Test
    fun `should provide markdown to insert the image`() {
        val serializer = SvgSerializer(snippet)
        val file = File("/foo/bar/doc.md")
        val (md, _) = serializer.resultToDisplay(file)
        assertThat(md)
            .isEqualTo("""
                ![the_subject-the_category-the_title](./the_subject-the_category-the_title.svg "the title")
            """.trimIndent())
    }

    @Test
    fun `should provide image to save`() {
        val serializer = SvgSerializer(snippet)
        val file = File("/foo/bar/doc.md")
        val (_, files) = serializer.resultToDisplay(file)
        assertThat(files.size).isEqualTo(1)

        val (path, content) = files.first()

        assertThat(path).isEqualTo(Path.of("/foo/bar/the_subject-the_category-the_title.svg"))

        assertThat(content).isEqualTo(
        """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" width="200" height="200" viewBox="0 0 200 200">
                <rect x="0" y="0" width="200" height="200" rx="10" fill="white" />
                <g stroke="black" fill="none">
                    result line 1
            result line 2
                </g>
            </svg>
        """.trimIndent()
        )
    }

    @Test
    fun `should provide xml as result`() {
        val serializer = SvgSerializer(snippet.copy(type = ExampleType.Code))
        val file = File("/foo/bar/doc.md")
        val (md, _) = serializer.resultToDisplay(file)
        assertThat(md)
            .isEqualTo("""
                |```xml
                |result line 1
                |result line 2
                |```
            """.trimMargin())
    }

    @Test
    fun `should provide xml and markdown to insert`() {
        val serializer = SvgSerializer(snippet.copy(type = ExampleType.CodeAndImage))
        val file = File("/foo/bar/doc.md")
        val (md, _) = serializer.resultToDisplay(file)
        assertThat(md)
            .isEqualTo("""
                ![the_subject-the_category-the_title](./the_subject-the_category-the_title.svg "the title")
                ```xml
                result line 1
                result line 2
                ```
            """.trimIndent())
    }
}