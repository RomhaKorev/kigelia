package com.gitlab.kigelia.documentation.fmrk.doubles

import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.dsl.svg.invoke
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Rectangle
import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.documentation.*
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.exampletodoc.DocConfiguration
import com.gitlab.kigelia.documentation.exampletodoc.DocumentationGenerator
import com.gitlab.kigelia.documentation.exampletodoc.serializers.SvgSerializer
import com.gitlab.kigelia.documentation.resources.FileResources
import org.junit.jupiter.api.Disabled

@ExamplesFor(
    subject = "foobar",
    description = "Foo Bar")
@Disabled("Dummy tests used by documentation generator unit tests. Do not enable")
class DummyDocumentationGenerator(p: DocConfiguration, fs: FileResources): DocumentationGenerator<SvgElement>(p, fs, { snippet -> SvgSerializer(snippet) }) {
    @Example
    fun example1() = `the snippet` {
        "g" {
        }
    } `will produce` {
        """<g />"""
    } `will be documented` {
        `in the category`("Foo")
        titled("""Example 1""")
        `highlight the lines`("6")
    }

    @Example
    fun example2() = `the snippet` {
        "g" {
        }
    } `will produce` {
        """<g />"""
    } `will be documented` {
        `in the category`("Foo")
        titled("""Example 2""")
        `highlight the lines`("1")
    }

    @Example
    fun exampleRaw() = `the snippet` {
        "g" {
            "text" {
                -"foobar"
            }
        }
    } `will produce` {
        """
            <g>
                <text>
                    foobar
                </text>
            </g>
        """
    } `will be documented` {
        `in the category`("Foo")
        titled("""Example Raw""")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Code)
    }

    @Example
    fun exampleboth() = `the snippet` {
        "g" {
            "text" {
                -"Both"
            }
        }
    } `will produce` {
        """
            <g>
                <text>
                    Both
                </text>
            </g>
        """
    } `will be documented` {
        `in the category`("Foo")
        titled("""example both""")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.CodeAndImage)
    }

    @Example
    fun exampleWith2Elements() = `the snippet` {
        Rectangle(Point(50, 50), Size(70, 100)).draw {
            stroke("black")
        }
        circle {
            cx(20)
            cy(30)
            r(10)
        }
    } `will produce` {
        """
            <rect x="50" y="50" width="70" height="100" stroke="black" />
            <circle cx="20" cy="30" r="10" />
        """
    } `will be documented` {
        `in the category`("Foo")
        titled("""example 2 elements""")
        `displayed as`(com.gitlab.kigelia.documentation.example.ExampleType.Code)
    }

    override fun `the snippet`(f: SvgElement.() -> Unit): ExampleBuilder {
        return ExampleBuilder({
            "" {
                f()
            }.render()
        }, exampleSerializer)
    }
}

