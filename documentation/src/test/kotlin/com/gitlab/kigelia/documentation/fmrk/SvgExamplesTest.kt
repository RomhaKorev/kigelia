package com.gitlab.kigelia.documentation.fmrk

import com.gitlab.kigelia.core.dsl.html.HtmlStructure
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.documentation.exampletodoc.DocConfiguration
import com.gitlab.kigelia.documentation.exampletodoc.DocumentationGenerator
import com.gitlab.kigelia.documentation.fmrk.doubles.DummyHtmlDocumentationGenerator
import com.gitlab.kigelia.documentation.fmrk.doubles.DummyDocumentationGenerator
import com.gitlab.kigelia.documentation.fmrk.doubles.FakeFileSystem
import com.gitlab.kigelia.documentation.exampletodoc.serializers.invariant
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.nio.file.Path
import kotlin.reflect.KFunction1


class SvgExamplesTest {
    @Test
    fun `should create the directory and copy configuration`() {
        val (fs, _) = generateSvgDocumentation(DummyDocumentationGenerator::example1)
        assertThat(fs.created).contains(Path.of("/fake"))
        assertThat(fs.copied.map { it.second }).contains(Path.of("/fake/mkdocs.yml"))
        assertThat(fs.copied.map { it.second }).contains(Path.of("/fake/docs/index.md"))
    }

    @Test
    fun `should create the file`() {
        val (fs, _) = generateSvgDocumentation(DummyDocumentationGenerator::example1)
        assertThat(fs.created).contains(Path.of("/fake/docs/foobar"))
    }

    @Test
    fun `should write examples and create images`() {
        val (fs, _) = generateSvgDocumentation(
            DummyDocumentationGenerator::example1,
                                            DummyDocumentationGenerator::example2)
        assertThat(fs.written.map { it.key }).contains(Path.of("/fake/docs/foobar/foo.md"))
        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            ### Example 1
            
            ```kotlin hl_lines="6"
            "g" {
            }
            ```
            ${image("Example 1", "./")}
        """.trimIndent())
        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            ### Example 2
            
            ```kotlin hl_lines="1"
            "g" {
            }
            ```
            ${image("Example 2", "./")}
        """.trimIndent())
        assertThat(fs.written[Path.of("/fake/docs/foobar/foobar-foo-example_1.svg")]).contains("""<g />""")
        assertThat(fs.written[Path.of("/fake/docs/foobar/foobar-foo-example_2.svg")]).contains("""<g />""")
    }


    @Test
    fun `should write examples as generated code`() {
        val (fs, _) = generateSvgDocumentation(DummyDocumentationGenerator::exampleRaw)
        assertThat(fs.written.map { it.key }).contains(Path.of("/fake/docs/foobar/foo.md"))
        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            ```xml
            <g>
                <text>
                    foobar
                </text>
            </g>
            ```
        """.trimIndent())
    }

    @Test
    fun `should write examples as generated code and image`() {
        val (fs, _) = generateSvgDocumentation(DummyDocumentationGenerator::exampleboth)
        assertThat(fs.written.map { it.key }).contains(Path.of("/fake/docs/foobar/foo.md"))
        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            ${image("example both", "./")}
            ```xml
            <g>
                <text>
                    Both
                </text>
            </g>
            ```
        """.trimIndent())
    }

    @Test
    fun `should include the html example`() {
        val (fs, _) = generateHtmlDocumentation(DummyHtmlDocumentationGenerator::exampleWithHtml)
        assertThat(fs.written.map { it.key }).contains(Path.of("/fake/docs/foobar/foo.md"))
        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            ```kotlin
            div {
                p {
                    - "foobar"
                }
            }
            ```
        """.trimIndent())

        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            <div><p>foobar</p></div>
        """.trimIndent())
    }

    @Test
    fun `should include the full example`() {
        val (fs, _) = generateSvgDocumentation(DummyDocumentationGenerator::exampleWith2Elements)
        assertThat(fs.written.map { it.key }).contains(Path.of("/fake/docs/foobar/foo.md"))
        assertThat(fs.written[Path.of("/fake/docs/foobar/foo.md")]).contains("""
            ```kotlin
            Rectangle(Point(50, 50), Size(70, 100)).draw {
                stroke("black")
            }
            circle {
                cx(20)
                cy(30)
                r(10)
            }
            ```
        """.trimIndent())
    }



    private fun image(name: String, path: String): String {
        val filename = name.replace(" ", "_")
        return "![foobar-Foo-${filename}](${Path.of(path).resolve("foobar-foo-${filename.lowercase()}").invariant()}.svg \"$name\")"
    }

    private fun generateSvgDocumentation(vararg funcs: KFunction1<DummyDocumentationGenerator, Unit>): Pair<FakeFileSystem, DocumentationGenerator<SvgElement>> {
        val fs = FakeFileSystem()
        val f = DummyDocumentationGenerator(DocConfiguration("/fake", "/src"), fs)
        funcs.forEach { func -> func.call(f) }
        return fs to f
    }

    private fun generateHtmlDocumentation(vararg funcs: KFunction1<DummyHtmlDocumentationGenerator, Unit>): Pair<FakeFileSystem, DocumentationGenerator<HtmlStructure.HtmlBody>> {
        val fs = FakeFileSystem()
        val f = DummyHtmlDocumentationGenerator(DocConfiguration("/fake", "/src"), fs)
        funcs.forEach { func -> func.call(f) }
        return fs to f
    }
}
