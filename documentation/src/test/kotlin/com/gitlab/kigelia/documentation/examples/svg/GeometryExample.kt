package com.gitlab.kigelia.documentation.examples.svg

import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.SvgExamples

@ExamplesFor(
    subject = "SVG",
    description = "How to use the DSL to create xml-like content")
class GeometryExample: SvgExamples() {
    @Example
    fun example3() = `the snippet` {
            "rect" {
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                stroke("red")
                noFill()
            }
    } `will produce` {
        """
            <rect x="20" y="20" width="100" height="50" stroke="red" fill="none" />
        """
    } `will be documented` {
        `in the category`("Geometry")
        titled("""How to set the geometry of an element""")
        `highlight the lines`("2-7")
    }

    @Example
    fun example4() = `the snippet` {
        "g" {
            "rect" {
                id("source")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                fill("green")
            }
            "rect" {
                geometry("source".geometry())
                "stroke"("red")
                noFill()
            }
        }
    } `will produce` {
        """
            <rect x="20" y="20" width="100" height="50" id="source" fill="green" />
            <rect x="20" y="20" width="100" height="50" stroke="red" fill="none" />
        """
    } `will be documented` {
        `in the category`("Geometry")
        titled("""How to get the geometry of an element""")
        `highlight the lines`("13")
    }
}