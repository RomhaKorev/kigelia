package com.gitlab.kigelia.documentation.examples.svg

import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.dsl.svg.elements.line
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.geometry.*
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.ExamplesFor
import com.gitlab.kigelia.documentation.SvgExamples

@ExamplesFor(
    subject = "SVG",
    description = "How to use the DSL to create SVG")
class Shapes: SvgExamples() {
    @Example
    fun example1() = `the snippet` {
            Rectangle(Point(50, 50), Size(70, 100)).draw { /***/ }
    } `will produce` {
        """
            <rect x="50" y="50" width="70" height="100" />
        """
    } `will be documented` {
        `in the category`("""Shapes""")
        titled("""How to create a rectangle""")
    }


    @Example
    fun example2() = `the snippet` {
        rect {
            geometry {
                x(50)
                y(50)
                width(70)
                height(100)
            }
        }
    } `will produce` {
        """<rect x="50" y="50" width="70" height="100" />"""
    } `will be documented` {
        `in the category`("""Shapes""")
        titled("""How to create a rectangle with the DSL""")
        `highlight the lines`("2")
    }

    @Example
    fun example3() = `the snippet` {
        Ellipse(Point(100, 100), Size(70, 100)).draw { /***/ }
    } `will produce` {
        """<ellipse cx="100" cy="100" rx="35" ry="50" />"""
    } `will be documented` {
        `in the category`("""Shapes""")
        titled("""How to create an ellipse""")
        `highlight the lines`("")
    }

    @Example
    fun example4() = `the snippet` {
        circle {
            cx(100)
            cy(100)
            r(50)
        }
    } `will produce` {
        """<circle cx="100" cy="100" r="50" />"""
    } `will be documented` {
        `in the category`("""Shapes""")
        titled("""How to create a circle with the DSL""")
        `highlight the lines`("")
    }

    @Example
    fun exampleLine() = `the snippet` {
        Line(p1=Point(12, 23), p2=Point(34, 45)).draw { /***/ }
    } `will produce` {
        """<line x1="12" y1="23" x2="34" y2="45" />"""
    } `will be documented` {
        `in the category`("""Shapes""")
        titled("""How to create a line""")
        `highlight the lines`("")
    }

    @Example
    fun exampleLineDSL() = `the snippet` {
        line {
            x1(12)
            y1(23)
            x2(34)
            y2(45)
        }
    } `will produce` {
        """<line x1="12" y1="23" x2="34" y2="45" />"""
    } `will be documented` {
        `in the category`("""Shapes""")
        titled("""How to create a line with the DSL""")
        `highlight the lines`("")
    }
}
