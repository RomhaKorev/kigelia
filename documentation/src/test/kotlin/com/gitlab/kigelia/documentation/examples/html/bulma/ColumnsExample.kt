package com.gitlab.kigelia.documentation.examples.html.bulma

import com.gitlab.kigelia.core.dsl.html.bulma.bulma
import com.gitlab.kigelia.core.dsl.html.bulma.columns.Column
import com.gitlab.kigelia.core.dsl.html.elements.div
import com.gitlab.kigelia.core.dsl.html.elements.p
import com.gitlab.kigelia.documentation.Example
import com.gitlab.kigelia.documentation.HtmlExamples
import com.gitlab.kigelia.documentation.ExamplesFor
import org.junit.jupiter.api.Order

@ExamplesFor(
    subject = "html/Bulma",
    description = "How to use the DSL to create html content using Bulma CSS Framework")

class ColumnsExample: HtmlExamples() {
    @Order(1)
    @Example
    fun exampleColumn() = `the snippet` {
        bulma {
            div {
                "style"("width: 100%")
                columns {
                    cssClass("is-mobile")
                    column {
                        p {
                            cssClass("bd-notification is-primary") // irrelevant
                            -"Column 1"
                        }
                    }
                    column {
                        p {
                            cssClass("bd-notification is-primary") // irrelevant
                            -"Column 2"
                        }
                    }
                    column {
                        p {
                            cssClass("bd-notification is-primary") // irrelevant
                            -"Column 3"
                        }
                    }
                }
            }
        }
    } `will produce` {
        """
        <div class="columns is-mobile">
           <div class="column">
               <p class="bd-notification is-primary">Column 1</p>
           </div>
           <div class="column">
               <p class="bd-notification is-primary">Column 2</p>
           </div>
           <div class="column">
               <p class="bd-notification is-primary">Column 3</p>
           </div>
        </div>
        """
    } `will be documented` {
        titled("Creating columns")
        `in the category`("Columns")
        `highlight the lines`("4 6 11 16")
    }

    @Order(2)
    @Example
    fun exampleColumnSize() = `the snippet` {
        bulma {
            div {
                "style"("width: 100%") // irrelevant
                listOf<Pair<String, Column.() -> Unit>>( // irrelevant
                    "`is three quarters`" to Column::`is three quarters`, // irrelevant
                    "`is two thirds`" to Column::`is two thirds`, // irrelevant
                    "`is half`" to Column::`is half`, // irrelevant
                    "`is one third`" to Column::`is one third`, // irrelevant
                    "`is one quarter`" to Column::`is one quarter`, // irrelevant
                    "`is full`" to Column::`is full`, // irrelevant
                    "`is four fifths`" to Column::`is four fifths`, // irrelevant
                    "`is three fifths`" to Column::`is three fifths`, // irrelevant
                    "`is two fifths`" to Column::`is two fifths`, // irrelevant
                    "`is one fifth`" to Column::`is one fifth`).forEach { (classname, size) -> // irrelevant
                    columns {
                        cssClass("is-mobile")
                        column {
                            `is three quarters`()
                            removeCssClass("is-three-quarters") // irrelevant
                            size() // irrelevant
                            p { // irrelevant
                                cssClass("bd-notification is-primary") // irrelevant
                                - "`is three quarters`()"
                                - "$classname()" // irrelevant
                            } // irrelevant
                        }
                        column {
                            p { // irrelevant
                                cssClass("bd-notification is-fojeisj") // irrelevant
                                -"Auto"
                            } // irrelevant
                        }
                        column {
                            p { // irrelevant
                                cssClass("bd-notification is-fojeisj")  // irrelevant
                                -"Auto"
                            } // irrelevant
                        }
                    }
                } // irrelevant
            }
        }
    } `will produce` {
        listOf(
            "is-three-quarters" to "`is three quarters`",
            "is-two-thirds" to "`is two thirds`",
            "is-half" to "`is half`",
            "is-one-third" to "`is one third`",
            "is-one-quarter" to "`is one quarter`",
            "is-full" to "`is full`",
            "is-four-fifths" to "`is four fifths`",
            "is-three-fifths" to "`is three fifths`",
            "is-two-fifths" to "`is two fifths`",
            "is-one-fifth" to "`is one fifth`").joinToString("\n") { (className, size) ->
            """
                <div class="columns is-mobile">
                <div class="column $className">
                    <p class="bd-notification is-primary">$size()</p>
                </div>
                <div class="column">
                    <p class="bd-notification is-fojeisj">Auto</p>
                </div>
                <div class="column">
                    <p class="bd-notification is-fojeisj">Auto</p>
                </div>
            </div>
            """.trimIndent()
        }
    } `will be documented` {
        `in the category`("Columns")
        titled("Defining columns sizes")
        `described as`(
            """
                Use the following classes to define the size of a column:
                """)
        `highlight the lines`("6")
    }
}