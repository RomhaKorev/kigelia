package com.gitlab.kigelia.gitflow.domain

import com.gitlab.kigelia.core.dsl.common.Tag
import org.junit.jupiter.api.Assertions
import java.io.File
import java.nio.file.Paths


infix fun Tag.`should be`(expected: Any) {

    val name = Thread.currentThread().stackTrace[2].methodName
    val filename = Paths.get("target", "generated-test-sources", "$name.svg").toString()
    `save as`(filename)
    Assertions.assertEquals(expected, this.render())
}

infix fun Any.`should be`(expected: Any) {
    Assertions.assertEquals(expected, this)
}

infix fun Tag.`save as`(fileName: String): Tag {
    val myfile = File(fileName)
    val path = myfile.toPath()
    val directory = path.parent.toFile()
    if (!directory.exists())
        directory.mkdirs()

    myfile.printWriter().use { out ->
        out.println(this.render())
    }
    return this
}
