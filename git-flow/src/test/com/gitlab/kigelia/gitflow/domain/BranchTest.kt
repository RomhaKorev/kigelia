package com.gitlab.kigelia.gitflow.domain

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.gitflow.domain.model.Repository
import org.junit.jupiter.api.Test

class BranchTest {
    @Test
    fun `should create a branch with commits`() {
        Repository("dummy").branch("master") {
            commit(1)
            commit(2)
        } `should be` Branch("dummy", "master", commits=mutableListOf(Commit(1, "dummy-master-1"), Commit(2, "dummy-master-2")))
    }
}

