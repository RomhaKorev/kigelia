package com.gitlab.kigelia.gitflow.domain

import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Commit
import org.junit.jupiter.api.Test

class TimelineTest {

    @Test
    fun `should reset commit index after`() {
        flow {
            commit("local/master")
            `in parallel` {
                commit("local/master")
            }
            commit("origin/master")
        }.repositories.flatMap { it.branches }
            .flatMap { it.commits } `should be`(
                listOf(
                        Commit(1, "local-master-1"),
                        Commit(2, "local-master-2"),
                        Commit(2, "origin-master-2")
                )
        )
    }
}
