package com.gitlab.kigelia.gitflow.domain

import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.gitflow.domain.model.Flow
import com.gitlab.kigelia.gitflow.domain.model.Repository
import org.junit.jupiter.api.Test

class RepositoryTest {
    private fun Commit(index: Int): Commit {
        return Commit(index, "origin-master-$index")
    }

    private fun Branch(name: String, commits: MutableList<Commit>): Branch {
        return Branch("origin", name, commits=commits)
    }

    @Test
    fun `should create a repository with branch and commits`() {
        Flow().repository("origin") {
            branch("master") {
                commit(1)
            }
        }  `should be` Repository("origin", mutableListOf(Branch("master", commits=mutableListOf(Commit(1)))))
    }

    @Test
    fun `should spare a branch to order them`() {
        flow {
            spareBranch("origin/feature-1")
            commit("origin/master")
        }  `should be` Flow(repositories = mutableListOf(Repository("origin", mutableListOf(Branch("feature-1", commits = mutableListOf()), Branch("master", commits=mutableListOf(Commit(1)))))))
    }
}
