package com.gitlab.kigelia.gitflow.domain

import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.*
import org.junit.jupiter.api.Test

class FlowTest {

    private fun branch(repo: String, name: String, vararg commits: Int): Branch {
        return Branch(repo, name, commits=commits.map { Commit(it, "$repo-$name-$it") }.toMutableList())
    }

    @Test
    fun `should create a flow with a repo`() {
        flow {
            repository("origin") {
                branch("master") {
                    commit(1)
                }
            }
        } `should be` Flow(mutableListOf(Repository("origin", mutableListOf(branch("origin", "master", 1)))))
    }

    @Test
    fun `should create a commit with right index`() {
        flow {
            repository("origin") {
                branch("master") {
                    commit()
                    commit()
                }
            }
        } `should be` Flow(mutableListOf(Repository("origin", mutableListOf(
                branch("origin","master", 1, 2)))))
    }


    @Test
    fun `should create a repo and branch from commit`() {
        flow {
            commit("origin/master")
            commit("origin/master")
        } `should be` Flow(mutableListOf(Repository("origin", mutableListOf(
                branch("origin", "master", 1, 2)
        ))))
    }

    @Test
    fun `should create multiple repo with multiple branches`() {
        flow {
            commit("origin/master")
            commit("bob/master")
            commit("origin/master")
            commit("bob/feature-1")
            commit("origin/feature-1")

        } `should be` Flow(mutableListOf(
                Repository("origin", mutableListOf(
                            branch("origin", "master", 1, 3),
                            branch("origin", "feature-1", 5)
                )),
                Repository("bob", mutableListOf(
                        branch("bob", "master", 2),
                        branch("bob", "feature-1", 4)
                )),
        ))
    }

    @Test
    fun `should push a branch to a remote repository`() {
        flow {
            commit("bob/feature-1")
            commit("bob/feature-1")
            commit("bob/feature-1")
            "bob/feature-1".push("origin/feature-1")

        } `should be` Flow(mutableListOf(
                Repository("bob", mutableListOf(
                        branch("bob", "feature-1", 1, 2, 3))
                ),
                Repository("origin", mutableListOf(
                        branch("origin", "feature-1", 6))
                )),
        )
    }

    @Test
    fun `should create a tag`() {
        flow {
            commit("bob/feature-1")
            commit("bob/feature-1")
            "bob/feature-1".tag("v1.0")
        }.repositories.first().tags `should be` mutableListOf(Tag("v1.0", Commit(2, "bob-feature-1-2")))
    }
}
