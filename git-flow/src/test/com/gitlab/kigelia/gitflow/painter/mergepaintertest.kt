package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Merge
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions
import org.junit.jupiter.api.Test

class MergePainterTest {

    @Test
    fun `should paint a merge`() {
        orphan {
            commit(3).toPainter().paint(this, Point(5.0, 40.0), PaintOptions())
            commit(4).toPainter().paint(this, Point(150.0, 80.0), PaintOptions())
            Merge(commit(3), commit(4), Branch("", "")).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        } `should paint` """<path d="M25 40 C165 40, 35 80, 175 80" fill="none" stroke-width="2" stroke=".+" />""".trimIndent()
    }

    @Test
    fun `should paint a push`() {
        orphan {
            flow {
                commit("local/master")
                "local/master".push("origin/master")

            }.toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        }`should paint` """
            <path d="M66 20 C101 20, 76 69, 111 69" fill="none" stroke-width="2" stroke=".+" />
        """.trimIndent()
    }

    @Test
    fun `should paint a checkout`() {
        orphan {
            flow {
                commit("local/master")
                "local/master".checkout("local/feature-1")

            }.toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        }`should paint` """
            <path d="M76.5 20 C111.5 20, 86.5 40, 121.5 40" fill="none" stroke-width="2" stroke="#5fc1b7" />
        """.trimIndent()
    }

    @Test
    fun `should paint a pull`() {
        orphan {
            flow {
                commit("origin/master")
                "local/master".pull("origin/master")
            }.toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        }`should paint` """
            <path d="M66 20 C101 20, 76 69, 111 69" fill="none" stroke-width="2" stroke=".+" />
        """.trimIndent()
    }
}
