package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Repository
import com.gitlab.kigelia.gitflow.painter.options.AuthorBasedColorSelector
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions
import org.junit.jupiter.api.Test

class OptionsTest {

    @Test
    fun `should specify the color of a branch`() {
        flow {
            repository("local") {
                branch("master") {
                    prior = true
                    later = true
                    commit()
                }
            }
        }.toSvg(PaintOptions().with(AuthorBasedColorSelector()) with {
            colors {
                branch("local/master", "yellow")
            }
        }) `should paint` """
            <g fill="yellow">
                <line x1="[0-9]+" y1="[0-9]+" x2="[0-9]+" y2="[0-9]+" stroke="yellow" stroke-width="2" stroke-dasharray="4, 1" />
                <line x1="[0-9]+" y1="[0-9]+" x2="[0-9]+" y2="[0-9]+" stroke="yellow" stroke-width="2" stroke-dasharray="4, 1" />
                <text x="0" y="22" font-size="0.75em">
                    master
                </text>
                <g stroke="yellow">
                    <circle id="local-master-1" fill="#ef5f61" cx="61" cy="20" r="5" />
                </g>
            </g>
            """.trimIndent()
    }

    @Test
    fun `should specify the color of an author`() {
        flow {
            commit("local/develop") by "Igor"
            commit("local/develop") by "Igor"
        }.toSvg(PaintOptions().with(AuthorBasedColorSelector()) with {
            colors {
                author("Igor", "green")
            }
        }) `should paint` """
            <g stroke="#ef5f61">
                    <circle id="local-develop-1" fill="green" cx="64.5" cy="20" r="5" />
                    <circle id="local-develop-2" fill="green" cx="79.5" cy="20" r="5" />
            </g>
            """.trimIndent()
    }

    @Test
    fun `should use the color specified by custom options`() {
            flow {
                repository("local") {
                    branch("master") {
                        prior = true
                        later = true
                    }
                }
                commit("local/master") by "Igor"
                commit("local/master") by "Ana"
                commit("local/master") by "Bob"
                commit("local/master") by "Ana"
                commit("local/master") by "Ana"
                commit("local/master") by "Igor"
            }.toSvg(PaintOptions().with(AuthorBasedColorSelector()) with {
                colors {
                    author("Igor", "blue")
                    author("Ana", "red")
                    branch("local/master", "yellow")
                }
            }) `should paint` """
                <g fill="yellow">
                <line x1="36" y1="20" x2="56" y2="20" stroke="yellow" stroke-width="2" stroke-dasharray="4, 1" />
                <line x1="161" y1="20" x2="141" y2="20" stroke="yellow" stroke-width="2" stroke-dasharray="4, 1" />
                <line x1="56" y1="20" x2="141" y2="20" stroke="yellow" stroke-width="2" />
                <text x="0" y="22" font-size="0.75em">
                    master
                </text>
                <g stroke="yellow">
                    <circle id="local-master-1" fill="blue" cx="61" cy="20" r="5" />
                    <circle id="local-master-2" fill="red" cx="76" cy="20" r="5" />
                    <circle id="local-master-3" fill="#ed8d46" cx="91" cy="20" r="5" />
                    <circle id="local-master-4" fill="red" cx="106" cy="20" r="5" />
                    <circle id="local-master-5" fill="red" cx="121" cy="20" r="5" />
                    <circle id="local-master-6" fill="blue" cx="136" cy="20" r="5" />
                </g>
            </g>
            """.trimIndent()
    }

    @Test
    fun `should hide names`() {
        orphan {
            Repository("origin",
                    mutableListOf(
                            Branch(
                                    "origin",
                                    "master",
                                    prior = true,
                                    commits = mutableListOf(commit(1), commit(2))
                            )
                    )).toPainter().paint(this, Point(0.0, 0.0), PaintOptions().withShowInformation(false))
        } `should paint` """
            <g>
                <g fill="#5fc1b7">
                    <line x1="0" y1="10" x2="20" y2="10" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                    <line x1="20" y1="10" x2="45" y2="10" stroke="#5fc1b7" stroke-width="2" />
                    <g stroke="#5fc1b7">
                        <circle id="origin-master-1" cx="25" cy="10" r="5" />
                        <circle id="origin-master-2" cx="40" cy="10" r="5" />
                    </g>
                </g>
            </g>
        """.trimIndent()
    }
}
