package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Flow
import com.gitlab.kigelia.gitflow.domain.model.Repository
import com.gitlab.kigelia.gitflow.painter.options.AuthorBasedColorSelector
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions
import org.junit.jupiter.api.Test

class FlowTest {
    @Test
    fun `flow with a repository`() {
       orphan {
            Flow(
               mutableListOf(Repository("origin",
                       mutableListOf(Branch("origin", "master", prior=true, later=true, commits = mutableListOf(commit(1), commit(2), commit(3), commit(5))))
               ))).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
           }  `should paint` """
               <g>
                   <rect x="-2" y="-2" width="150" height="34" fill="#314957" rx="5" ry="5" />
                   <g>
                       <text x="5" y="8" font-size="0.75em" fill="#e5e5e5">
                           origin
                       </text>
                       <g fill="#5fc1b7">
                           <line x1="36" y1="20" x2="56" y2="20" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                           <line x1="146" y1="20" x2="126" y2="20" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                           <line x1="56" y1="20" x2="126" y2="20" stroke="#5fc1b7" stroke-width="2" />
                           <text x="0" y="22" font-size="0.75em">
                               master
                           </text>
                           <g .+>
                           ${commit(61, 20)}
                           ${commit(76, 20)}
                           ${commit(91, 20)}
                           ${commit(121, 20)}
                           </g>
                       </g>
                   </g>
               </g>
           """.trimIndent()
       }

    @Test
    fun `flow with multiple repositories`() {
        orphan {
            Flow(
                    mutableListOf(Repository("origin",
                            mutableListOf(Branch("origin", "master", prior=true, later=true, commits = mutableListOf(
                                    commit(1), commit(2), commit(3), commit(5)
                            )))
                    ),
                    Repository("local",
                            mutableListOf(Branch("local", "master", prior=true, later=true, commits = mutableListOf(
                                    commit(1), commit(2), commit(3), commit(5)
                            )))
                    ))).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        }  `should paint` """
            <g>
                <rect x="-2" y="-2" width="150" height="34" fill="#314957" rx="5" ry="5" />
                <rect x="-2" y="47" width="150" height="[0-9]+" fill="#314957" rx="5" ry="5" />
                <g>
                    <text x="5" y="8" font-size="0.75em" fill="#e5e5e5">
                        origin
                    </text>
                    <g fill="#5fc1b7">
                        <line x1="36" y1="20" x2="56" y2="20" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                        <line x1="146" y1="20" x2="126" y2="20" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                        <line x1="56" y1="20" x2="126" y2="20" stroke="#5fc1b7" stroke-width="2" />
                        <text x="0" y="22" font-size="0.75em">
                            master
                        </text>
                        <g .+>
                            ${commit(61, 20)}
                            ${commit(76, 20)}
                            ${commit(91, 20)}
                            ${commit(121, 20)}
                        </g>
                    </g>
                </g>
                <g>
                    <text x="5" y="[0-9]+" font-size="0.75em" fill="#e5e5e5">
                        local
                    </text>
                    <g fill="#5fc1b7">
                        <line x1="36" y1="[0-9]+" x2="56" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                        <line x1="146" y1="[0-9]+" x2="126" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                        <line x1="56" y1="[0-9]+" x2="126" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" />
                        <text x="0" y="[0-9]+" font-size="0.75em">
                            master
                        </text>
                        <g .+>
                            ${commit(61, 69)}
                            ${commit(76, 69)}
                            ${commit(91, 69)}
                            ${commit(121, 69)}
                        </g>
                    </g>
                </g>
            </g>
        """.trimIndent()
    }

    @Test
    fun `flow with 2 repositories`() {
        orphan {
            Flow(
                    mutableListOf(
                            Repository("origin",
                                mutableListOf(Branch("origin", "master", commits = mutableListOf(commit(1), commit(5))))
                            ),
                            Repository("other",
                                mutableListOf(Branch("other", "master", commits = mutableListOf(commit(1), commit(8))))
                            ),
                            Repository("local",
                            mutableListOf(Branch("local", "master", commits = mutableListOf(commit(1))))
                    )
                    )
            ).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        }  `should paint` """
            <g>
                <rect x="-2" y="-2" width="175" .+ />
                <rect x="-2" y="[0-9]+" width="175" .+ />
                <rect x="-2" y="[0-9]+" width="175" .+ />
                <g>
                    <text .+>
                        origin
                    </text>
                    <g fill="#5fc1b7">
                        <line .+ />
                        <text .+>
                            master
                        </text>
                        <g .+>
                            <circle .+ />
                            <circle .+ />
                        </g>
                    </g>
                </g>
                <g>
                    <text .+>
                        other
                    </text>
                    <g fill="#5fc1b7">
                        <line .+ />
                        <text .+>
                            master
                        </text>
                        <g .+>
                            <circle .+ />
                            <circle .+ />
                        </g>
                    </g>
                </g>
                <g>
                    <text .+>
                        local
                    </text>
                    <g fill="#5fc1b7">
                        <text .+>
                            master
                        </text>
                        <g .+>
                        <circle .+ />
                        </g>
                    </g>
                </g>
            </g>
        """.trimIndent()
    }

    @Test
    fun `flow with a push`() {
        flow {
            commit("origin/master")
            commit("origin/master")
            commit("origin/master")
            commit("local/master")
            "local/master".push("origin/master")
        }.toSvg(PaintOptions()) `should paint` """
            <g>
                <rect x="-2" y="-2" width="160" height="34" fill="#314957" rx="5" ry="5" />
                <rect x="-2" y="47" width="160" height="34" fill="#314957" rx="5" ry="5" />
                <path d="M111 69 C146 69, 121 20, 156 20" fill="none" stroke-width="2" stroke="#5fc1b7" />
        """.trimIndent()
    }

    @Test
    fun `flow with 2 repositories with long branch name`() {
        svg {
            geometry {
                width(400)
                height(300)
            }
            Flow(
                    mutableListOf(
                            Repository("origin",
                                    mutableListOf(Branch("origin", "feature-12345", commits = mutableListOf(commit(1), commit(5))))
                            ),
                            Repository("other",
                                    mutableListOf(Branch("other", "master", commits = mutableListOf(commit(1), commit(8))))
                            ),
                            Repository("local",
                                    mutableListOf(Branch("local", "master", commits = mutableListOf(commit(1))))
                            )
                    )
            ).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        }  `should paint` """
                <g>
                    <rect x="-2" y="-2" width="199.5" .+ />
                    <rect x="-2" y="[0-9]+" width="199.5" .+ />
                    <rect x="-2" y="[0-9]+" width="199.5" .+ />
                    <g>
                        <text x="5" y="[0-9]+" font-size="0.75em" fill="#e5e5e5">
                            origin
                        </text>
                        <g fill="#ef5f61">
                            <line x1="80.5" y1="[0-9]+" x2="150.5" y2="[0-9]+" stroke="#ef5f61" stroke-width="2" />
                            <text x="0" y="[0-9]+" font-size="0.75em">
                                feature-12345
                            </text>
                            <g .+>
                                <circle id="origin-master-1" cx="85.5" cy="[0-9]+" r="5" />
                                <circle id="origin-master-5" cx="145.5" cy="[0-9]+" r="5" />
                            </g>
                        </g>
                    </g>
                    <g>
                        <text x="5" y="[0-9]+" font-size="0.75em" fill="#e5e5e5">
                            other
                        </text>
                        <g fill="#5fc1b7">
                            <line x1="80.5" y1="[0-9]+" x2="195.5" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" />
                            <text x="0" y="[0-9]+" font-size="0.75em">
                                master
                            </text>
                            <g .+>
                                <circle id="origin-master-1" cx="85.5" cy="[0-9]+" r="5" />
                                <circle id="origin-master-8" cx="190.5" cy="[0-9]+" r="5" />
                            </g>
                        </g>
                    </g>
                    <g>
                        <text x="5" y="[0-9]+" font-size="0.75em" fill="#e5e5e5">
                            local
                        </text>
                        <g fill="#5fc1b7">
                            <text x="0" y="[0-9]+" font-size="0.75em">
                                master
                            </text>
                            <g .+>
                                <circle id="origin-master-1" cx="85.5" cy="[0-9]+" r="5" />
                            </g>
                        </g>
                    </g>
                </g>
        """.trimIndent()
    }


    @Test
    fun `flow with a unused branch`() {
        orphan {
            Flow(
                    mutableListOf(
                            Repository("origin",
                                    mutableListOf(Branch("origin", "feature-12345", commits = mutableListOf(commit(1), commit(5))))
                            ),
                            Repository("local",
                                    mutableListOf(
                                            Branch("local", "feature-1", commits = mutableListOf()),
                                            Branch("local", "master", commits = mutableListOf(commit(1)))
                                    )
                            )
                    )
            ).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        } `should paint` """
                <g>
                    <rect x="-2" y="-2" width="154.5" height="34" fill="#314957" rx="5" ry="5" />
                    <rect x="-2" y="47" width="154.5" height="54" fill="#314957" rx="5" ry="5" />
                    <g>
                        <text x="5" y="8" font-size="0.75em" fill="#e5e5e5">
                            origin
                        </text>
                        <g fill=".+">
                            <line x1="80.5" y1="20" x2="150.5" y2="20" stroke=".+" stroke-width="2" />
                            <text x="0" y="22" font-size="0.75em">
                                feature-12345
                            </text>
                            <g .+>
                                <circle id="origin-master-1" cx="85.5" cy="20" r="5" />
                                <circle id="origin-master-5" cx="145.5" cy="20" r="5" />
                            </g>
                        </g>
                    </g>
                    <g>
                        <text x="5" y="57" font-size="0.75em" fill="#e5e5e5">
                            local
                        </text>
                        <g fill=".+">
                            <text x="0" y=".+" font-size="0.75em">
                                feature-1
                            </text>
                        </g>
                        <g fill=".+">
                            <text x="0" y=".+" font-size="0.75em">
                                master
                            </text>
                            <g .+>
                                <circle id="origin-master-1" cx="85.5" cy="89" r="5" />
                            </g>
                        </g>
                    </g>
                </g>
        """.trimIndent()
    }

    @Test
    fun `flow with a master-only flow`() {

        flow {
                commit("local/master") by "bob"
                commit("local/master") by "Igor"
                commit("local/master") by "Alice"
                commit("local/master") by "bob"
        }.toSvg(PaintOptions().with(AuthorBasedColorSelector())) `should paint` """
            <g stroke="#5fc1b7">
                    <circle id="local-master-1" fill="#ef5f61" cx="61" cy="20" r="5" />
                    <circle id="local-master-2" fill="#c8d71e" cx="76" cy="20" r="5" />
                    <circle id="local-master-3" fill="#ed8d46" cx="91" cy="20" r="5" />
                    <circle id="local-master-4" fill="#ef5f61" cx="106" cy="20" r="5" />
                </g>
        """.trimIndent()
    }

    @Test
    fun `demo test`() {
        flow {
            repository("david") {
                branch("master") {
                    later = true
                    prior = true
                }
            }
            repository("origin") {
                branch("feature-1") {}
                branch("master") {
                    later = true
                    prior = true
                }
            }
            commit("origin/master")
            "david/master".pull("origin/master")
            "david/master".push("david/feature-1")
            `in parallel` {
                commit("david/feature-1")
                commit("david/feature-1")
                commit("david/feature-1")
            }
            "david/feature-1".push("origin/feature-1")
            commit("david/feature-1")
            commit("david/feature-1")
            "david/feature-1".push("origin/feature-1")
            "origin/feature-1".push("origin/master")
        }.toSvg() `should paint` ""
    }

/*
        @Image("git/feature-branch-4")
        fun `4`(color: String): Tag = flow {
            repository("matthieu") {
                branch("feature-2") {
                    later = true
                }
            }
            repository("origin") {
                branch("feature-2") {
                    later = true
                }
                branch("master") {
                    later = true
                    prior = true
                }
                branch("feature-1") {
                    prior = true
                }
            }

            commit("origin/feature-1")
            commit("matthieu/feature-2")
            commit("matthieu/feature-2")
            commit("origin/feature-1")
            commit("matthieu/feature-2")
            "origin/feature-1".push("origin/master")
            "origin/master".push("matthieu/feature-2")
            commit("matthieu/feature-2")
            commit("matthieu/feature-2")
            "matthieu/feature-2".push("origin/feature-2")
        }.toSvg()
*/
}
