package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.gitflow.domain.`should be`
import com.gitlab.kigelia.gitflow.painter.options.AuthorBasedColorSelector
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions
import org.junit.jupiter.api.Test

class CommitPainterTest {

    @Test
    fun `should paint a commit at the right position`() {
        orphan {
            commit(3).toPainter().paint(this, Point(5.0, 40.0), PaintOptions())
        } `should be` """
            <circle id="origin-master-3" cx="20" cy="40" r="5" />
        """.trimIndent()
    }

    @Test
    fun `should paint a commit with a color based on the author`() {
        val options = PaintOptions().with(AuthorBasedColorSelector())
        orphan {
            g {
                commit(3, author = "Igor").toPainter().paint(this, Point(5.0, 40.0), options)
                commit(3, author = "Igor").toPainter().paint(this, Point(5.0, 40.0), options)
                commit(3, author = "Bob").toPainter().paint(this, Point(5.0, 40.0), options)
            }
        } `should be` """
            <g>
                <circle id="origin-master-3" fill="#ef5f61" cx="20" cy="40" r="5" />
                <circle id="origin-master-3" fill="#ef5f61" cx="20" cy="40" r="5" />
                <circle id="origin-master-3" fill="#c8d71e" cx="20" cy="40" r="5" />
            </g>
        """.trimIndent()
    }
}
