package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit


fun commit(index: Int, author: String = ""): Commit {
    return Commit(index, "origin-master-$index", author = author)
}


fun Number.normed(): String = this.toString().replace("\\.0$".toRegex(), "")

fun commit(x: Number, y: Number): String = """<circle id=".+" cx="${x.normed()}" cy="${y.normed()}" r="[0-9]+" />"""
fun commit(): String = """<circle cx="[0-9]+" cy="[0-9]+" r="[0-9]+" />"""

fun branch(name: String, vararg commits: Point, prior: Boolean = false, later: Boolean = false): String {
    var details = ""
    if (prior)
        details += "\n" + """<line x1="35" y1="[0-9]+" x2="55" y2="[0-9]+" stroke=".+" stroke-width="2" stroke-dasharray="4, 1" />"""
    if (later)
        details += "\n" + """<line x1="[0-9]+" y1="[0-9]+" x2="[0-9]+" y2="[0-9]+" stroke=".+" stroke-width="2" stroke-dasharray="4, 1" />"""
    return """
    <g fill=".+">$details
        <line x1="55" y1="10" x2="125" y2="10" stroke="#5fc1b7" stroke-width="2" />
        <text x="0" y="12" font-size="0.75em">
            $name
        </text>
        <g .+>
            ${commits.joinToString("\n") { (x, y) -> commit(x, y) }}
        </g>
    </g>
""".trimIndent()
}

fun branch(name: String, prior: Boolean = false, later: Boolean = false): String {
    var details = ""
    if (prior)
        details += "\n" + """<line x1="35" y1="[0-9]+" x2="55" y2="[0-9]+" stroke=".+" stroke-width="2" stroke-dasharray="4, 1" />"""
    if (later)
        details += "\n" + """<line x1="[0-9]+" y1="[0-9]+" x2="[0-9]+" y2="[0-9]+" stroke=".+" stroke-width="2" stroke-dasharray="4, 1" />"""
    return """
    <g fill=".+">
        <text x="0" y="12" font-size="0.75em">
            $name
        </text>
        (${commit()}\n)+
        <line x1="55" y1="10" x2="125" y2="10" stroke="#5fc1b7" stroke-width="2" />
    </g>
""".trimIndent()
}

fun repository(vararg branch: Branch): String {
    return """
        <g>
        ${branch.joinToString("\n") { branch(it.name, it.prior, it.later) }}
        </g>
    """.trimIndent()
}


class Expected(rx: String) {
    val rx = rx.split("\n").joinToString("\n") { "\\s*${it.trim()}" }
    override fun toString(): String {
        return rx
    }

    override fun equals(other: Any?): Boolean {
        return rx.toRegex().containsMatchIn(other.toString())
    }
}
