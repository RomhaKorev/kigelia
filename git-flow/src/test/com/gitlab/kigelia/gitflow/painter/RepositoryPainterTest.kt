package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.text
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.gitflow.domain.model.Repository
import com.gitlab.kigelia.gitflow.painter.options.AuthorBasedColorSelector
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions
import org.junit.jupiter.api.Test

class RepositoryPainterTest {

    @Test
    fun `should paint a repository with a branch`() {
            orphan {
                Repository("origin",
                    mutableListOf(Branch("origin", "master", prior=true, later=true, commits = mutableListOf(commit(1), commit(2), commit(3), commit(5))))
                ).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
            } `should paint` """
                <g>                     
                        <text x="5" y="8" font-size="0.75em" fill="#e5e5e5">
                            origin
                        </text>
                        <g fill="#5fc1b7">
                            <line x1="36" y1="[0-9]+" x2="56" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                            <line x1="146" y1="[0-9]+" x2="126" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                            <line x1="56" y1="[0-9]+" x2="126" y2="[0-9]+" stroke="#5fc1b7" stroke-width="2" />
                            <text x="0" y="[0-9]+" font-size="0.75em">
                                master
                            </text>
                            <g .+>
                                <circle id=".+" cx="[0-9]+" cy="[0-9]+" r="[0-9]+" />
                                <circle id=".+" cx="[0-9]+" cy="[0-9]+" r="[0-9]+" />
                                <circle id=".+" cx="[0-9]+" cy="[0-9]+" r="[0-9]+" />
                                <circle id=".+" cx="[0-9]+" cy="[0-9]+" r="[0-9]+" />
                            </g>
                        </g>
                </g>
            """.trimIndent()
    }

    @Test
    fun `should paint a repository with multiple branches`() {
        orphan {
            Repository("origin",
                    mutableListOf(
                            Branch("origin", "master", prior=true, later=true, commits = mutableListOf(commit(1), commit(2), commit(3), commit(5))),
                            Branch("origin", "feature-1", prior=true, later=true, commits = mutableListOf(commit(1), commit(2), commit(3), commit(5)))
                    )
            ).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        } `should paint` """
            <g>
                <text x="5" y="8" font-size="0.75em" fill="#e5e5e5">
                    origin
                </text>
                <g fill="#5fc1b7">
                    <line x1="46.5" y1="20" x2="66.5" y2="20" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                    <line x1="156.5" y1="20" x2="136.5" y2="20" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
                    <line x1="66.5" y1="20" x2="136.5" y2="20" stroke="#5fc1b7" stroke-width="2" />
                    <text x="0" y="22" font-size="0.75em">
                        master
                    </text>
                    <g stroke="#5fc1b7">
                        <circle id="origin-master-1" cx="71.5" cy="20" r="5" />
                        <circle id="origin-master-2" cx="86.5" cy="20" r="5" />
                        <circle id="origin-master-3" cx="101.5" cy="20" r="5" />
                        <circle id="origin-master-5" cx="131.5" cy="20" r="5" />
                    </g>
                </g>
                <g fill="#ef5f61">
                    <line x1="46.5" y1="40" x2="66.5" y2="40" stroke="#ef5f61" stroke-width="2" stroke-dasharray="4, 1" />
                    <line x1="156.5" y1="40" x2="136.5" y2="40" stroke="#ef5f61" stroke-width="2" stroke-dasharray="4, 1" />
                    <line x1="66.5" y1="40" x2="136.5" y2="40" stroke="#ef5f61" stroke-width="2" />
                    <text x="0" y="42" font-size="0.75em">
                        feature-1
                    </text>
                    <g stroke="#ef5f61">
                        <circle id="origin-master-1" cx="71.5" cy="40" r="5" />
                        <circle id="origin-master-2" cx="86.5" cy="40" r="5" />
                        <circle id="origin-master-3" cx="101.5" cy="40" r="5" />
                        <circle id="origin-master-5" cx="131.5" cy="40" r="5" />
                    </g>
                </g>
            </g>
        """.trimIndent()
    }

    @Test
    fun `should add id if not blank`() {
        flow {
            repository("origin") {
                id = "foobar"
                branch("master") {
                    commit()
                    commit()
                }
            }
        }.toSvg(PaintOptions().with(AuthorBasedColorSelector()).withShowInformation(false)) `should paint` """
                <g id="foobar">
            """.trimIndent()
    }

    @Test
    fun `should adapt background when adding item to repository`() {
        val svg = flow {
            repository("origin") {
                id = "foobar"
                branch("master") {
                    commit()
                    commit()
                }
            }
        }.toSvg(PaintOptions())

        svg.g {
            val repository = findSibling("foobar")
            repository?.text {
                val p = 10.below("origin-master-1")
                x(p.x)
                y(p.y)
                - "tag"
            }
        }
        svg `should paint` """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="95" height="44" viewBox="-7 -7 95 44">
                    <g>
                        <rect x="-2" y="-2" width="85" height="34" fill="#314957" rx="5" ry="5" />
                        <g id="foobar">
                            <text x="5" y="8" font-size="0.75em" fill="#e5e5e5">
                                origin
                            </text>
                            <g fill="#5fc1b7">
                                <line x1="56" y1="20" x2="81" y2="20" stroke="#5fc1b7" stroke-width="2" />
                                <text x="0" y="22" font-size="0.75em">
                                    master
                                </text>
                                <g stroke="#5fc1b7">
                                    <circle id="origin-master-1" cx="61" cy="20" r="5" />
                                    <circle id="origin-master-2" cx="76" cy="20" r="5" />
                                </g>
                            </g>
                            <text x="0" y="35">
                                tag
                            </text>
                        </g>
                    </g>
                    <g />
                </svg>
            """.trimIndent()
    }

    fun commit(index: Int): Commit {
        return Commit(index, "origin-master-$index")
    }
}
