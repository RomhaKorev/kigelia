package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.dsl.svg.Svg
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.gitflow.domain.`save as`
import com.gitlab.kigelia.gitflow.domain.model.Repository
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.nio.file.Paths

class BranchPainterTest {

    @Test
    fun `should paint a branch with all the commits`() {
        orphan {
            Branch("origin", "master", commits = mutableListOf(commit(1), commit(2), commit(3), commit(5))).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        } `should paint` branch("master", Point(60.0, 10.0), Point(75.0, 10.0), Point(90.0, 10.0), Point(120.0, 10.0))
    }

    @Test
    fun `should paint a branch with all the commits and prior, later dash line`() {
        orphan {
            Branch(
                    "origin",
                    "master",
                    prior = true,
                    later = true,
                    commits = mutableListOf(commit(1), commit(2), commit(3), commit(5))
            ).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        } `should paint` branch(
                "master",
                Point(60.0, 10.0),
                Point(75.0, 10.0),
                Point(90.0, 10.0),
                Point(120.0, 10.0),
                prior = true,
                later = true
        )
    }

    @Test
    fun `should paint a later dash line to the far right of the repository`() {
        orphan {
            Repository("origin",
                    mutableListOf(
            Branch(
                    "origin",
                    "master",
                    prior = true,
                    later = true,
                    commits = mutableListOf(commit(1), commit(2), commit(3), commit(5))
            ),
            Branch(
                    "origin",
                    "master",
                    prior = true,
                    later = true,
                    commits = mutableListOf(commit(1))
            )
            )).toPainter().paint(this, Point(0.0, 0.0), PaintOptions())
        } `should paint` """
            <line x1="146" y1="40" x2="66" y2="40" stroke="#5fc1b7" stroke-width="2" stroke-dasharray="4, 1" />
        """.trimIndent()
    }
}

infix fun Svg.`should paint`(expected: String) {
    val name = Thread.currentThread().stackTrace[2].methodName
    val directory = Thread.currentThread().stackTrace[2].className.split(".").last()
    val filename = Paths.get("target", "generated-test-sources", "painter", directory, "$name.svg").toString()
    this `save as`(filename)
    Assertions.assertEquals(Expected(expected), this.render())
}

infix fun Tag.`should paint`(expected: String) {
    val s = svg {
        add(this@`should paint`)
    }
    val name = Thread.currentThread().stackTrace[2].methodName
    val directory = Thread.currentThread().stackTrace[2].className.split(".").last()
    val filename = Paths.get("target", "generated-test-sources", "painter", directory, "$name.svg").toString()
    s `save as`(filename)
    Assertions.assertEquals(Expected(expected), this.render())
}
