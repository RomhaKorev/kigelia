package com.gitlab.kigelia.gitflow.domain.model

data class Merge(val from: Commit, val to: Commit, val branch: Branch)

data class Flow(val repositories: MutableList<Repository> = mutableListOf()) {
    private var commitIndex = 1
    val merges = mutableListOf<Merge>()


    val branches: List<Branch>
    get() = repositories.flatMap { it.branches }

    fun repository(name: String, init: Repository.() -> Unit): Repository {
        val repository = repositories.getOrCreate(name)
        repository.init()
        return repository
    }

    private fun MutableList<Repository>.getOrCreate(name: String): Repository {
        val repository = this.firstOrNull { it.name == name }
        if (repository != null)
            return repository

        val newRepository = Repository(name)
        this.add(newRepository)
        return newRepository
    }

    fun Branch.commit(): Commit {
        return commit(commitIndex++)
    }

    fun commit(name: String): Commit {
        val (repositoryName, branchName) = name.split("/")
        val repository = repositories.getOrCreate(repositoryName)
        lateinit var commit: Commit
        repository.branch(branchName) {
            commit = commit()
        }
        return commit
    }



    infix fun String.tag(name: String) {
        val (repositoryName, branchName) = this.split("/")
        val repository = repositories.getOrCreate(repositoryName)
        val branch = repository.branches.find { it.name == branchName }!!
        val lastCommit = branch.commits.last()
        repository.tags.add(Tag(name, lastCommit))
    }

    fun spareBranch(name: String) {
        val (repositoryName, branchName) = name.split("/")
        val repository = repositories.getOrCreate(repositoryName)
        repository.branch(branchName) {}
    }

    infix fun String.push(remoteBranch: String) {
        val source = branches.first { it.fullName == this }
        val sourceCommit = source.commits.last()
        commitIndex = sourceCommit.index + 3
        commit(remoteBranch)
        val destination = branches.first { it.fullName == remoteBranch }
        merges.add(Merge(source.commits.last(), destination.commits.last(), source))
    }

    infix fun String.pull(remoteBranch: String) {
        val source = branches.first { it.fullName == remoteBranch }
        val sourceCommit = source.commits.last()
        commitIndex = sourceCommit.index + 3
        commit(this)
        val destination = branches.first { it.fullName == this }
        merges.add(Merge(source.commits.last(), destination.commits.last(), source))
    }

    infix fun String.checkout(remoteBranch: String) = push(remoteBranch)

    fun `in parallel`(init: Flow.() -> Unit) {
        val saveCommitIndex = this.commitIndex
        init()
        this.commitIndex = saveCommitIndex
    }
}
