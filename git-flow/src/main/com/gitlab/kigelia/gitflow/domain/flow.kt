package com.gitlab.kigelia.gitflow.domain

import com.gitlab.kigelia.gitflow.domain.model.Flow

fun flow(init: Flow.() -> Unit): Flow {
    val flow = Flow()
    flow.init()
    return flow
}
