package com.gitlab.kigelia.gitflow.domain.model

data class Commit(val index: Int, val id: String, var author: String = "", val message: String = "") {
    infix fun by(author: String): Commit {
        this.author = author
        return this
    }
}
