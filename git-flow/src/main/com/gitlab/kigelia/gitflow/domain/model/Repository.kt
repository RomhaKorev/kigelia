package com.gitlab.kigelia.gitflow.domain.model

data class Repository(val name: String, val branches: MutableList<Branch> = mutableListOf(), val tags: MutableList<Tag> = mutableListOf(), var id: String = "") {

    fun branch(name: String, init: Branch.() -> Unit): Branch {
        val branch = branches.getOrCreate(name)
        branch.init()
        return branch
    }

    private fun MutableList<Branch>.getOrCreate(name: String): Branch {
        val branch = this.firstOrNull { it.name == name }
        if (branch != null)
            return branch
        val newbranch = Branch(this@Repository.name, name)
        this.add(newbranch)
        return newbranch
    }
}
