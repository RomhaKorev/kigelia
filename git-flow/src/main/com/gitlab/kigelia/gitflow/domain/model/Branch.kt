package com.gitlab.kigelia.gitflow.domain.model

data class Branch(val repository: String, val name: String, var prior: Boolean = false, var later: Boolean = false, val commits: MutableList<Commit> = mutableListOf()) {

    val fullName = "$repository/$name"
    fun commit(index: Int): Commit {
        commits.add(Commit(index, "$repository-$name-$index"))
        return commits.last()
    }

}
