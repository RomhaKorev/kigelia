package com.gitlab.kigelia.gitflow.domain.model

data class Tag(val value: String, val commit: Commit)
