package com.gitlab.kigelia.gitflow.painter.options

data class FixedLabelWidth(val length: Int) {
    companion object {
        val unset = FixedLabelWidth(-1)
    }
}
