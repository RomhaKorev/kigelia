package com.gitlab.kigelia.gitflow.painter.options

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.core.dsl.common.Color

class AuthorBasedColorSelector private constructor(
    private val branchProxy: ColorSelectorProxy,
    private val authorProxy: ColorSelectorProxy
): ColorSelector {
    constructor(): this(ColorSelectorProxy(values()), ColorSelectorProxy(values()))

    override fun colorOf(branch: Branch): Color {
        if (branch.name.equals("master", ignoreCase = true))
            return Color.Blue
        return branchProxy.colorOf(branch.name)
    }

    override fun colorOf(commit: Commit): Color {
        return authorProxy.colorOf(commit.author)
    }

}
