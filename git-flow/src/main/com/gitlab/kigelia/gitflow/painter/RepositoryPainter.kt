package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.text
import com.gitlab.kigelia.gitflow.domain.model.Repository
import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.geometry.DPoint
import com.gitlab.kigelia.core.geometry.Padding
import com.gitlab.kigelia.gitflow.painter.options.FixedCommitCount
import com.gitlab.kigelia.gitflow.painter.options.FixedLabelWidth
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions


data class RepositoryPainter(val repository: Repository): Painter {
    override fun paint(parent: SvgElement, offset: Point, options: PaintOptions): SvgElement {

        val commitOptions = if (options.fixedCommitCount == FixedCommitCount.unset) {
            options.with(FixedCommitCount(repository.branches.flatMap { it.commits }.map { it.index }.maxOrNull() ?: -1))
        } else {
            options
        }

        val nameWidth = if (options.fixedLabelWidth == FixedLabelWidth.unset) {
            repository.branches.maxOf { it.name.length } * 3.5 + 15.0
        } else {
            options.fixedLabelWidth.length * 3.5 + 15.0
        }

        val branchPainters = repository.branches.map {
            BranchPainter(it, nameWidth)
        }

        val element = parent.g {
            id(repository.id)
            background {
                color(Color.DarkBlue)
                "rx"("5")
                "ry"("5")
                padding(Padding(2.0, 2.0))
            }
            text {
                x(offset.x + 5.0)
                y(offset.y + 8.0)
                geometry {
                    x(offset.x)
                    y(offset.y)
                    height(8.0)
                    width(20.0)
                }
                "font-size"("0.75em")
                "fill"(Color.White)
                - repository.name
                hideIf(!options.showInformation)
            }
            var branchPosition = if (options.showInformation) offset + DPoint(0.0, 10.0) else offset
            branchPainters.forEach {
                val painted = it.paint(this, branchPosition, commitOptions)
                branchPosition += DPoint(0.0, painted.geometry().height())
            }
        }
        return element
    }
}


fun Repository.toPainter(): RepositoryPainter {
    return RepositoryPainter(this)
}
