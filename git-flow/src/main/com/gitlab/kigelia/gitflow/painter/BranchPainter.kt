package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.line
import com.gitlab.kigelia.core.dsl.svg.elements.text
import com.gitlab.kigelia.core.geometry.DPoint
import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions

class BranchPainter(val branch: Branch, val nameWidth: Double = 35.0): Painter {
    override fun paint(parent: SvgElement, offset: Point, options: PaintOptions): SvgElement {

        val startX = if (options.showInformation) nameWidth else 0.0
        val commitPainters = branch.commits.map {
            CommitPainter(it)
        }

        return parent.g {
            val padding = DPoint(startX + (1 + options.fixedCommitCount.count) * commitPadding, branchHeight / 2.0)
            val spare = paintSpare(this, offset + padding, options.fixedCommitCount)

            "fill"(options.colorOf(branch))
            text {
               x(offset.x)
               y(offset.y + branchHeight / 2.0 + 2)
                geometry {
                    x(offset.x)
                    y(offset.y)
                    width(startX)
                    height(branchHeight)
                }
                "font-size"("0.75em")
                - branch.name
                hideIf(!options.showInformation)
            }

            if (commitPainters.isEmpty())
                return@g

            val paintedCommits = g {
                "stroke"(options.colorOf(branch))
                commitPainters.map { commit ->
                    val p = DPoint(startX + (1 + commit.commit.index) * commitPadding, branchHeight / 2.0)
                    commit.paint(this, offset + p, options)
                }
                hideIf(commitPainters.isEmpty())
            }
            val g = paintedCommits.geometry()
            val p1 = g.left().point
            val p2 = g.right().point
            if (branch.prior) {
                line {
                    z(-1)
                    x1(startX)
                    y1(p1.y)
                    x2(p1.x)
                    y2(p1.y)
                    "stroke"(options.colorOf(branch))
                    "stroke-width"(2)
                    "stroke-dasharray"("4, 1")
                }
            }
            if (branch.later) {
                line {
                    z(-1)
                    if (spare == null) {
                        x1(p2.x + commitPadding * 2)
                    } else {
                        x1(spare.geometry().right().x + commitPadding * 2)
                    }
                    y1(p2.y)
                    x2(p2.x)
                    y2(p2.y)
                    "stroke"(options.colorOf(branch))
                    "stroke-width"(2)
                    "stroke-dasharray"("4, 1")
                }
            }
            if (commitPainters.size >= 2) {
                line {
                    z(-1)
                    x1(p1.x)
                    y1(p1.y)
                    x2(p2.x)
                    y2(p2.y)
                    "stroke"(options.colorOf(branch))
                    "stroke-width"(2)
                }
            }
        }
    }

    companion object {
        const val branchHeight = 20.0
        const val commitPadding = 10.0
    }

}

fun Branch.toPainter(): BranchPainter {
    return BranchPainter(this)
}
