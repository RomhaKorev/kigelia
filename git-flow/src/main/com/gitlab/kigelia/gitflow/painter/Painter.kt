package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions

fun interface Painter {
    fun paint(parent: SvgElement, offset: Point, options: PaintOptions): SvgElement
}
