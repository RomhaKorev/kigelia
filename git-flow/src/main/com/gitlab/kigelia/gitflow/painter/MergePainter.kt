package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Anchor
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.gitflow.domain.model.Merge
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions

class MergePainter(val merge: Merge): Painter {
    override fun paint(parent: SvgElement, offset: Point, options: PaintOptions): SvgElement {
        val source = parent.flatten().find {merge.from.id == it.id.value() }!!.geometry().right().point
        val destination = parent.flatten().find {merge.to.id == it.id.value() }!!.geometry().right().point

        return parent.connect(
                Anchor(
                        source,
                        Anchor.Kind.Right
                ),
                Anchor(
                        destination,
                        Anchor.Kind.Left
                ), type = ConnectorType.Curvy) {
            color(options.colorOf(merge.branch).toString())
            z(-1)
        }
    }
}


fun Merge.toPainter(): MergePainter {
    return MergePainter(this)
}
