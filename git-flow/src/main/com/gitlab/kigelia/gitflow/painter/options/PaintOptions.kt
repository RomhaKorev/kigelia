package com.gitlab.kigelia.gitflow.painter.options

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.core.dsl.common.Color

private data class OptionsDetails(
    val fixedCommitCount: FixedCommitCount = FixedCommitCount.unset,
    val fixedLabelWidth: FixedLabelWidth = FixedLabelWidth.unset,
    val showInformation: Boolean = true,
    val colorSelector: ColorSelector = BranchBasedColorSelector())

open class PaintOptions private constructor(private val d: OptionsDetails) {

    constructor(): this(OptionsDetails())

    val fixedCommitCount: FixedCommitCount
    get() = d.fixedCommitCount

    val fixedLabelWidth: FixedLabelWidth
    get() = d.fixedLabelWidth

    val showInformation: Boolean
    get() = d.showInformation

    open fun colorOf(commit: Commit): Color {
        return d.colorSelector.colorOf(commit)
    }

    open fun colorOf(branch: Branch): Color {
        return d.colorSelector.colorOf(branch)
    }

    fun with(fixedCommitCount: FixedCommitCount): PaintOptions {
        val d = this.d.copy(fixedCommitCount = fixedCommitCount)
        return PaintOptions(d)
    }

    fun with(fixedLabelWidth: FixedLabelWidth): PaintOptions {
        val d = this.d.copy(fixedLabelWidth = fixedLabelWidth)
        return PaintOptions(d)
    }

    fun with(colorSelector: ColorSelector): PaintOptions {
        val d = this.d.copy(colorSelector = colorSelector)
        return PaintOptions(d)
    }

    infix fun with(init: CustomColorSelector.() -> Unit): PaintOptions {
        val customColorSelector = CustomColorSelector(d.colorSelector)
        customColorSelector.init()
        val d = this.d.copy(colorSelector = customColorSelector)
        return PaintOptions(d)
    }

    fun withShowInformation(value: Boolean): PaintOptions {
        val d = this.d.copy(showInformation = value)
        return PaintOptions(d)
    }
}
