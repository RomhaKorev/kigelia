package com.gitlab.kigelia.gitflow.painter.options

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.core.dsl.common.Color

interface ColorSelector {
    fun colorOf(branch: Branch): Color
    fun colorOf(commit: Commit): Color
}
