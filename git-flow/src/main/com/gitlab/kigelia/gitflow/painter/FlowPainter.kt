package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.Svg
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.core.geometry.DPoint
import com.gitlab.kigelia.gitflow.domain.model.Flow
import com.gitlab.kigelia.gitflow.painter.options.BranchBasedColorSelector
import com.gitlab.kigelia.gitflow.painter.options.FixedCommitCount
import com.gitlab.kigelia.gitflow.painter.options.FixedLabelWidth
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions

data class FlowPainter(val flow: Flow): Painter {
    override fun paint(parent: SvgElement, offset: Point, options: PaintOptions): SvgElement {
        return parent.g {

            val lastIndex = flow.repositories.flatMap { it.branches }.flatMap { it.commits }.map { it.index }.maxOrNull() ?: -1
            val longestBranchName = flow.repositories.flatMap { it.branches }.map { it.name.length }.maxOrNull() ?: -1
            val fixedCommitCount = FixedCommitCount(lastIndex)
            val fixedLabelWidth = FixedLabelWidth(longestBranchName)

            val painters = flow.repositories.map { RepositoryPainter(it) }

            var repositoryPosition = offset
            painters.forEach {
                val painted = it.paint(this, repositoryPosition, options.with(fixedCommitCount).with(fixedLabelWidth))
                repositoryPosition += DPoint(0.0, painted.geometry().height() + repositoryMargins)
            }
            flow.merges.map { MergePainter(it) }.forEach {
                it.paint(this, Point(0.0, 0.0), options)
            }
        }
    }

    companion object {
        const val repositoryMargins = 15.0
    }

}

fun Flow.toPainter(): FlowPainter {
    return FlowPainter(this)
}

fun Flow.toSvg(options: PaintOptions = PaintOptions().with(BranchBasedColorSelector())): Svg {
    return svg {
        this@toSvg.toPainter().paint(this, Point(0.0, 0.0), options)

        growBy {
            horizontal(5)
            vertical(5)
        }
    }
}
