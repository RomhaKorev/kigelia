package com.gitlab.kigelia.gitflow.painter

import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.dsl.svg.helpers.spacer
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.gitflow.painter.options.FixedCommitCount
import com.gitlab.kigelia.gitflow.painter.options.PaintOptions

data class CommitPainter(val commit: Commit): Painter {
    override fun paint(parent: SvgElement, offset: Point, options: PaintOptions): SvgElement {
        val position = Point(commit.index * commitSize, 0.0) + offset.toDPoint()

        return parent.circle {
            val color = options.colorOf(commit)
            if (color != Color.None) {
                "fill"(color)
            }
            cx(position.x)
            cy(position.y)
            r(commitSize)
            id(commit.id)
        }
    }

    companion object {
        const val commitSize = 5.0
    }
}


fun Commit.toPainter(): CommitPainter {
    return CommitPainter(this)
}


fun paintSpare(parent: SvgElement, offset: Point, fixedCommitCount: FixedCommitCount): SvgElement? {
    if (fixedCommitCount == FixedCommitCount.unset)
        return null

    val position = Point(fixedCommitCount.count * CommitPainter.commitSize, 0.0) + offset.toDPoint()
    return parent.spacer {
        x(position.x)
        y(position.y)
        width(CommitPainter.commitSize)
        height(CommitPainter.commitSize)
    }
}
