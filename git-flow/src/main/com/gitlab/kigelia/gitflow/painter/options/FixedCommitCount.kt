package com.gitlab.kigelia.gitflow.painter.options

data class FixedCommitCount(val count: Int) {
    companion object {
        val unset = FixedCommitCount(-1)
    }
}
