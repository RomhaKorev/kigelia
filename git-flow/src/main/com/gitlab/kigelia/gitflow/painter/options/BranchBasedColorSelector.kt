package com.gitlab.kigelia.gitflow.painter.options

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.core.dsl.common.Color

class BranchBasedColorSelector private constructor(
    private val proxy: ColorSelectorProxy
): ColorSelector {
    override fun colorOf(branch: Branch): Color {
        if (branch.name.equals("master", ignoreCase = true))
            return Color.Blue
        return proxy.colorOf(branch.name)
    }

    override fun colorOf(commit: Commit): Color {
        return Color.None
    }

    constructor(): this(ColorSelectorProxy(values()))
}
