package com.gitlab.kigelia.gitflow.painter.options

import com.gitlab.kigelia.gitflow.domain.model.Branch
import com.gitlab.kigelia.gitflow.domain.model.Commit
import com.gitlab.kigelia.core.dsl.common.Color

class CustomColorSelector(val source: ColorSelector): ColorSelector {

    val overriddenBranchColors = mutableMapOf<String, Color>()
    val overriddenAuthorColors = mutableMapOf<String, Color>()

    override fun colorOf(branch: Branch): Color {
        return overriddenBranchColors.getOrDefault(branch.fullName, source.colorOf(branch))
    }

    override fun colorOf(commit: Commit): Color {
        return overriddenAuthorColors.getOrDefault(commit.author, source.colorOf(commit))
    }

    fun colors(function: ColorSpecification.() -> Unit) {
        ColorSpecification().function()
    }


    inner class ColorSpecification {
        fun branch(name: String, color: String)  = branch(name, Color(color))

        fun branch(name: String, color: Color) {
            overriddenBranchColors[name] = color
        }

        fun author(name: String, color: String) = author(name, Color(color))

        fun author(name: String, color: Color) {
            overriddenAuthorColors[name] = color
        }
    }

}
