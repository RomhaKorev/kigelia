package com.gitlab.kigelia.gitflow.painter.options
import com.gitlab.kigelia.core.dsl.common.Color

class ColorSelectorProxy(colors: List<Color>) {
    val mapped = mutableMapOf<String, Color>()
    var availableColors = colors.toMutableList()

    fun colorOf(value: String): Color {
        if (mapped.containsKey(value)) {
            return mapped[value]!!
        }
        return selectColor(value)
    }

    private fun selectColor(value: String): Color {
        var color = availableColors.removeAt(0)
        mapped[value] = color
        return color
    }
}


fun values(): List<Color> {
    return listOf(
            Color.Pink, Color.Green, Color.Orange, Color.Yellow, Color.Red, Color.AltBlue,
            Color.AltGreen,
            Color.Purple,
            Color.AltRed,
            Color.AltOrange,
            Color.AltYellow,
            Color.Gray,
            Color.LightBlue,
            Color.LightGreen,
            Color.LightPurple,
            Color.LightRed,
            Color.LightOrange,
            Color.LightYellow,
            Color.LightGray,
            Color.AltDarkBlue,
            Color.DarkGreen,
            Color.DarkPurple,
            Color.DarkRed,
            Color.DarkOrange,
            Color.DarkYellow,
            Color.DarkGray
    )
}
