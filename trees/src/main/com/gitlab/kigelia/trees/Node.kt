package com.gitlab.kigelia.trees

class Node<T>(val value: T) {

    private var parentNode: Node<T>? = null
    private var childNodes = mutableListOf<Node<T>>()
    fun isRoot() = parent == null

    val parent: Node<T>?
        get() = parentNode

    val children: List<Node<T>>
        get() = childNodes

    fun add(vararg children: Node<T>): Node<T> {
        children.forEach {
            it.parentNode = this
        }
        childNodes.addAll(children)
        return this
    }
}