package com.gitlab.kigelia.trees

import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.text
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Rectangle
import com.gitlab.kigelia.core.geometry.Size

data class DelegateColors(
    val background: Any = Color.AltBlue,
    val foreground: Any = Color.White,
    val borders: Any = Color.DarkBlue,
    val connectors: Any = Color.Blue
)

data class DelegateFont(
    val family: String = "monospace",
    val size: Int = 14
)

data class DelegateOptions(
    val colors: DelegateColors = DelegateColors(),
    val font: DelegateFont = DelegateFont()
)

fun rectDelegate(size: Size, margins: Size) = rectDelegate(size, margins, DelegateOptions())

fun rectDelegate(size: Size, margins: Size, options: DelegateOptions): SvgElement.(Any, Point) -> SvgElement {
    return { value, p ->
        val boundingSize = size + margins
        val position = Point(p.x * boundingSize.width, p.y * boundingSize.height)
        val r = Rectangle(position, boundingSize).shrinkedBy(margins)
        g {
            r.draw {
                fill(options.colors.background)
                stroke(options.colors.borders)
                "stroke-width"(2)
                "rx"(4)

            }
            text {
                x(r.center.x)
                y(r.center.y + options.font.size / 2)
                "text-anchor"("middle")
                -value.toString()
                "font-size"(options.font.size)
                fill(options.colors.foreground)
                "font-family"(options.font.family)
            }
        }
    }
}