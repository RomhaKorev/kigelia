package com.gitlab.kigelia.trees

import com.gitlab.kigelia.core.geometry.DPoint
import com.gitlab.kigelia.core.geometry.Point
import kotlin.math.floor
import kotlin.math.max

class Layout<T>(val root: Node<T>) {

    private val nodePositions = mutableMapOf<Node<T>, Point>()
    private var depth = 0
    private var left = 0
    init {
        scan(root, Point(0, 0)).y
    }

    private fun scan(node: Node<T>, leftDepth: Point): Point {
        var rows = node.children.count()
        var leftDepth = leftDepth

        if (rows == 0) {
            nodePositions[node] = leftDepth
            return Point(leftDepth.x + 1, 1)
        }

        var childDepth = 0.0

        node.children.forEach {
            val p = scan(it, leftDepth + DPoint(0, 1))
            leftDepth = Point(p.x, leftDepth.y)
            childDepth = max(childDepth, p.y)
        }

        var left = positionOf(node.children.first()).x
        var right = positionOf(node.children.last()).x

        if (rows >= 2) {
            val x = if (rows % 2 == 1) {
                val r = (floor(rows / 2.0f)).toInt()
                val child = node.children[r]
                positionOf(child).x
            } else {
                (right + left) / 2
            }
            nodePositions[node] = (nodePositions[node]?: Point(0, 0)).copy(x = x)
        } else {
            nodePositions[node] = (nodePositions[node]?: Point(0, 0)).copy(x = left)
        }
        nodePositions[node] = (nodePositions[node]?: Point(0, 0)).copy(y = leftDepth.y)
        return Point( right + 1, childDepth + 1 )
    }


    fun positionOf(node: Node<T>): Point {
        return nodePositions[node] ?: Point(0, 0)
    }

    fun <U> map(f: (Node<T>, Point) -> U): List<U> {
        return this.nodePositions.map { (k, v) -> f(k, v) }
    }
}