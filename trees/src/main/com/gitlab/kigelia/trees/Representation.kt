package com.gitlab.kigelia.trees

import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.core.geometry.Point
import java.io.File

fun <T> Layout<T>.draw(f: SvgElement.(T, Point) -> SvgElement): SvgElement {
    val nodesAndRepr = mutableMapOf<Node<T>, SvgElement>()
    return orphan {
        g {
            map { node, position ->
                nodesAndRepr[node] = f(node.value, position)
            }
            map { node, _ ->
                val repr = nodesAndRepr[node]!!
                node.children.forEach {
                    val childRepr = nodesAndRepr[it]!!

                    connect(repr.geometry().bottom(), childRepr.geometry().top(), ConnectorType.Squared) {
                        color("black")
                        fill("black")
                        z(-1)
                    }
                }
            }
        }
    }
}