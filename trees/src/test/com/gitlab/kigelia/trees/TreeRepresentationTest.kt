package com.gitlab.kigelia.trees

import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.geometry.Size
import org.junit.jupiter.api.Test
import java.io.File

class TreeRepresentationTest {
    @Test
    fun `should draw a tree`() {
        val root = Node("root")
        val foo = Node("foo")
        val bar = Node("bar")
        val foo1 = Node("foo1")
        val foo2 = Node("foo2")

        foo.add(foo1, foo2)
        root.add(foo, bar)

        val diagram =
            Layout(root).draw(rectDelegate(size=Size(70, 30), margins = Size(10, 50)))

        diagram.saveAs("./tree.svg")

    }
}



fun Tag.saveAs(fileName: String) {
    val myfile = File(fileName)
    myfile.printWriter().use { out ->
        out.println(this.render())
    }
}