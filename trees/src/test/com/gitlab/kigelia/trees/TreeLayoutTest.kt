package com.gitlab.kigelia.trees

import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.elements.text
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Rectangle
import com.gitlab.kigelia.core.geometry.Size
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

class TreeLayoutTest {
    @Test
    fun `should find position of a node`() {
        val root = Node("root")
        val layout = Layout(root)

        assertThat(layout.positionOf(root)).isEqualTo(Point(0, 0))
    }

    @Test
    fun `should find position of a single child`() {
        val root = Node("root")
        val foo = Node("foo")
        root.add(foo)

        val layout = Layout(root)

        assertThat(layout.positionOf(root)).isEqualTo(Point(0, 0))
        assertThat(layout.positionOf(foo)).isEqualTo(Point(0, 1))
    }

    @Test
    fun `should find position of two children`() {
        val root = Node("root")
        val foo = Node("foo")
        val bar = Node("bar")

        root.add(foo, bar)

        val layout = Layout(root)

        assertThat(layout.positionOf(root)).isEqualTo(Point(0.5, 0))
        assertThat(layout.positionOf(foo)).isEqualTo(Point(0, 1))
        assertThat(layout.positionOf(bar)).isEqualTo(Point(1, 1))
    }

    @Test
    fun `should find position of three children`() {
        val root = Node("root")
        val foo = Node("foo")
        val bar = Node("bar")
        val spam = Node("spam")

        root.add(foo, bar, spam)

        val layout = Layout(root)

        assertThat(layout.positionOf(root)).isEqualTo(Point(1, 0))
        assertThat(layout.positionOf(foo)).isEqualTo(Point(0, 1))
        assertThat(layout.positionOf(bar)).isEqualTo(Point(1, 1))
        assertThat(layout.positionOf(spam)).isEqualTo(Point(2, 1))
    }

    @Test
    fun `should move node regarding its sibling`() {
        val root = Node("root")
        val foo = Node("foo")
        val bar = Node("bar")
        val foo1 = Node("foo1")
        val foo2 = Node("foo2")

        foo.add(foo1, foo2)
        root.add(foo, bar)

        val layout = Layout(root)

        assertThat(layout.positionOf(root)).isEqualTo(Point(1.25, 0))
        assertThat(layout.positionOf(foo)).isEqualTo(Point(0.5, 1))
        assertThat(layout.positionOf(bar)).isEqualTo(Point(2, 1))
    }
}