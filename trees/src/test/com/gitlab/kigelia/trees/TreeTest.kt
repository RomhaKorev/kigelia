package com.gitlab.kigelia.trees

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TreeTest {
    @Test
    fun `should build a single node tree`() {
        val node = Node("root")
        assertThat(node.isRoot()).isTrue
        assertThat(node.parent).isNull()
    }

    @Test
    fun `should build a tree`()  {
        val root = Node("root")
        val foo = Node("foo")
        val bar = Node("bar")

        root.add(foo, bar)

        assertThat(root.isRoot()).isTrue
        assertThat(foo.isRoot()).isFalse
        assertThat(foo.parent).isEqualTo(root)
        assertThat(bar.isRoot()).isFalse
        assertThat(bar.parent).isEqualTo(root)
    }
}


