# Kigelia

Kigelia is a side project used to build SVG images -- and other content based on tags (html, xml, etc.)

See the [documentation](https://romhakorev.gitlab.io/kigelia/), for more information

## Tests

```bash
mvn test
```
