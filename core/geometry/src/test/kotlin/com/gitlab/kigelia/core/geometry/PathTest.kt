package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


sealed interface Element {
    val lastPosition: Point
    val length: Double
}

data class LineElement(val line: Line): Element {
    override val lastPosition: Point
        get() = line.p2
    override val length: Double
        get() = line.length
}

data class MoveElement(val point: Point): Element {
    override val lastPosition: Point
        get() = point
    override val length: Double = 0.0
}

class Path {
    private val elements = mutableListOf<Element>()
    val currentPosition: Point
        get() = elements.lastOrNull()?.lastPosition ?: Point()

    val length: Double
        get() = elements.sumOf { it.length }

    fun moveTo(p: Point): Path {
        elements.add(MoveElement(p))
        return this
    }

    fun add(line: Line): Path {
        elements.add(LineElement(line))
        return this
    }
}

class PathTest {
    @Test
    fun `the current position of an empty path is unknown`() {
        assertThat(Path().currentPosition).isEqualTo(Point())
    }

    @Test
    fun `should move to the given position`() {
        assertThat(Path()
            .moveTo(Point(2, 5))
            .currentPosition).isEqualTo(Point(2, 5))

        assertThat(Path()
            .moveTo(Point(2, 5))
            .moveTo(Point(12, 34))
            .currentPosition).isEqualTo(Point(12, 34))
    }

    @Test
    fun `adding a line should move current position to its end`() {
        assertThat(Path()
            .add(Line(Point(2, 5), Point(12, 34)))
            .currentPosition).isEqualTo(Point(12, 34))
    }

    @Test
    fun `an empty path should have a length of 0`() {
        assertThat(Path().length).isEqualTo(0.0)
    }

    @Test
    fun `a path with only moves should have a length of 0`() {
        assertThat(Path().moveTo(Point(2, 5)).moveTo(Point(14, 56)).length).isEqualTo(0.0)
    }

    @Test
    fun `a path should have a length of the line`() {
        assertThat(Path().add(Line(Point(10, 0), Point(25, 0))).length).isEqualTo(15.0)
    }
}
