package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class RectangleTest: ValueObjectTest<Rectangle> {
    override fun createValue() = Rectangle(Point(20, 30), Size(12, 24))
    override fun createOtherValue() = Rectangle(Point(30, 50), Size(40, 5))

    @Test
    fun `should give its center`() {
        val r = createValue()
        assertThat(r.center).isEqualTo(Point(26, 42))
    }

    @Test
    fun `should resize a rectangle`() {
        val r = `a rectangle of`(12 by 34)
        assertThat(r.resized(50 by 100))
            .isEqualTo(`a rectangle of`(50 by 100))
    }

    @Test
    fun `should resize width or height`() {
        val r = `a rectangle of`(12 by 34)
        assertThat(r.resized(width = 56))
            .isEqualTo(`a rectangle of`(56 by 34))

        assertThat(r.resized(height = 56))
            .isEqualTo(`a rectangle of`(12 by 56))
    }

    @Test
    fun `should resize a rectangle by percent`() {
        val r = Rectangle(Point(20, 30), 50 by 75)
        assertThat(r.resized(50.percent by 200.percent))
            .isEqualTo(Rectangle(Point(20, 30), Size(25, 150)))
    }

    @Test
    fun `should translate a rectangle`() {
        assertThat(
            `a rectangle at`(Point(12, 24)).moved(Point(38, 76))
        ).isEqualTo(
            `a rectangle at`(Point(38, 76))
        )
    }

    @Test
    fun `should translate a rectangle by offset`() {
        assertThat(
            `a rectangle at`(Point(12, 24)).moved(DPoint(38, 76))
        ).isEqualTo(
            `a rectangle at`(Point(50, 100))
        )
    }

    @Test
    fun `rectangle should give its corners`() {
        val r = Rectangle(Point(10, 20), Size(100, 50))
        assertThat(r.topLeft).isEqualTo(Point(10, 20))
        assertThat(r.topRight).isEqualTo(Point(10 + 100, 20))
        assertThat(r.bottomLeft).isEqualTo(Point(10, 20 + 50))
        assertThat(r.bottomRight).isEqualTo(Point(10 + 100, 20 + 50))
    }

    @Test
    fun `should shrink a rectangle`() {
        val r = Rectangle(Point(10, 20), Size(100, 50))
        assertThat(r.shrinkedBy(10 by 20)).isEqualTo(Rectangle(Point(15, 30), Size(90, 30)))
    }

    @Test
    fun `should bound a shrink to rectangle width`() {
        val r = Rectangle(Point(10, 20), Size(100, 50))
        assertThat(r.shrinkedBy(1000 by 20)).isEqualTo(Rectangle(Point(60, 30), Size(0, 30)))
    }

    @Test
    fun `should bound a shrink to rectangle height`() {
        val r = Rectangle(Point(10, 20), Size(100, 50))
        assertThat(r.shrinkedBy(10 by 2000)).isEqualTo(Rectangle(Point(15, 45), Size(90, 0)))
    }

    @Test
    fun `should widen a rectangle`() {
        val r = Rectangle(Point(10, 20), Size(100, 50))
        assertThat(r.grownBy(10 by 20)).isEqualTo(Rectangle(Point(5, 10), Size(110, 70)))
    }



    @Test
    fun `should create a rectangle from top left and bottom right points`() {
        assertThat(
            Rectangle(Point(10, 20), Point(40, 50))
        ).isEqualTo(
            Rectangle(Point(10, 20), Size(30, 30))
        )
    }


    @Test
    fun `should center a rectangle on a given point`() {
        assertThat(
            Rectangle(Point(10, 20), Size(40, 30)).centeredOn(Point(150, 500))
        ).isEqualTo(
            Rectangle(Point(130, 485), Size(40, 30))
        )
    }

    @Test
    fun `should swap bad x from top left and bottom right points`() {
        assertThat(
            Rectangle(Point(40, 20), Point(10, 50))
        ).isEqualTo(
            Rectangle(Point(10, 20), Size(30, 30))
        )
    }

    @Test
    fun `should swap bad y from top left and bottom right points`() {
        assertThat(
            Rectangle(Point(10, 50), Point(40, 20))
        ).isEqualTo(
            Rectangle(Point(10, 20), Size(30, 30))
        )
    }


    private fun `a rectangle of`(size: Size) = Rectangle(Point(20, 30), size)
    private fun `a rectangle at`(position: Point) = Rectangle(position, 20 by 20)
}
