package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PointTest {

    @Test
    fun `should verify equality`() {
        assertThat(Point(3, 12)).isEqualTo(Point(3, 12))
        assertThat(Point(3, 3)).isNotEqualTo(Point(3, 12))
        assertThat(Point(6, 12)).isNotEqualTo(Point(3, 12))
    }

    @Test
    fun `should be summed`() {
        assertThat(Point(1, 2) + DPoint(3, 4)).isEqualTo(Point(4, 6))
        assertThat(Point(1, 2) + DPoint(3.0, 4.0)).isEqualTo(Point(4, 6))

        assertThat(Point(1, 2) - Point(3, 4)).isEqualTo(DPoint(-2, -2))
        assertThat(Point(1, 2) - DPoint(3.0, 4.0)).isEqualTo(Point(-2, -2))
        assertThat(DPoint(3.0, 4.0) - Point(1.0, 2.0)).isEqualTo(Point(2, 2))
    }

    @Test
    fun `should be commutative`() {
        assertThat(Point(1, 2) + DPoint(3, 4)).isEqualTo(Point(3, 4) + DPoint(1, 2))
        assertThat(Point(1, 2) + DPoint(
            3.0,
            4.0
        )
        ).isEqualTo(DPoint(3.0, 4.0) + Point(1, 2))
    }

    @Test
    fun `should multiply by a factor`() {
        assertThat(Point(2, 3) * 4).isEqualTo(Point(8, 12))
    }

    @Test
    fun `The bounding rect of a list of points should covered all points`() {
        val points = listOf<Point>(
            Point(10.0, 5.0),
            Point(40.0, 15.0),
            Point(60.0, -35.0),
            Point(10.0, 50.0),
            Point(-10.0, 25.0),
        )

        Assertions.assertEquals(Geometry(-10.0, -35.0, 70.0, 85.0), points.boundingRect())
    }


    @Test
    fun `Should give the distance between two points`() {
        val a = Point(10.0, 5.0)
        val b = Point(10.0, 105.0)

        Assertions.assertEquals(100.0, a.distance(b))
    }

    @Test
    fun `should convert to delta`() {
        assertThat(Point(2, 5).toDPoint()).isEqualTo(DPoint(2, 5))
    }
}


