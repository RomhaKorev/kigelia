package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TranslationTest: ValueObjectTest<Translation> {
    override fun createValue() =  Translation(Point(10, 20), Point(30, 40))

    override fun createOtherValue() = Translation(Point(60, 70), Point(80, 90))


    @Test
    fun `should compute delta`() {
        assertThat(createValue().delta).isEqualTo(Point(20.0, 20.0))
        assertThat(createValue().dx).isEqualTo(20.0)
        assertThat(createValue().dy).isEqualTo(20.0)
    }

    @Test
    fun `should be valid when source != destination`() {
        assertThat(Translation(Point(10, 20), Point(30, 40)).valid).isTrue
        assertThat(Translation(Point(10, 20), Point(10, 20)).valid).isFalse
    }
}
