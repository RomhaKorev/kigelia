package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.withPrecision
import org.junit.jupiter.api.Test

class LineTest: ValueObjectTest<Line> {
    override fun createValue() = Line(Point(12, 34), Point(56, 78))
    override fun createOtherValue() = Line(Point(100, 101), Point(102, 103))

    fun assertThat(actual: Line): LineAssert {
        return LineAssert(actual)
    }

    @Test
    fun `should give length`() {
        assertThat(Line(Point(10, 120), Point(50, 250)).length).isEqualTo(136.014, withPrecision(0.001))
    }

    @Test
    fun `should give angle`() {
        val e = Ellipse(Point(10, 10), Size(10, 10))

        assertThat(Line(e.center, e.pointAtPercent(0.0)).angle).isEqualTo(Angle.degrees(0.0))
        assertThat(Line(e.center, e.pointAtPercent(1/8.0)).angle).isEqualTo(Angle.degrees(45.0))
        assertThat(Line(e.center, e.pointAtPercent(2/8.0)).angle).isEqualTo(Angle.degrees(90.0))
        assertThat(Line(e.center, e.pointAtPercent(3/8.0)).angle).isEqualTo(Angle.degrees(135.0))
        assertThat(Line(e.center, e.pointAtPercent(4/8.0)).angle).isEqualTo(Angle.degrees(180.0))
        assertThat(Line(e.center, e.pointAtPercent(5/8.0)).angle).isEqualTo(Angle.degrees(225.0))
        assertThat(Line(e.center, e.pointAtPercent(6/8.0)).angle).isEqualTo(Angle.degrees(270.0))
        assertThat(Line(e.center, e.pointAtPercent(7/8.0)).angle).isEqualTo(Angle.degrees(315.0))
        assertThat(Line(e.center, e.pointAtPercent(0.0)).angle).isEqualTo(Angle.degrees(0.0))
    }

    @Test
    fun `should change angle`() {
        val e = Ellipse(Point(10, 10), Size(10, 10))

        val base = Line(e.center, e.pointAtPercent(0.0))

        assertThat(base.withAngle(Angle.degrees(360.0 * 0/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(0/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 1/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(1/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 2/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(2/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 3/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(3/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 4/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(4/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 5/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(5/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 6/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(6/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 7/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(7/8.0)))
        assertThat(base.withAngle(Angle.degrees(360.0 * 8/8.0))).isEqualTo(Line(e.center, e.pointAtPercent(8/8.0)))
    }

    @Test
    fun `should change length`() {
        val p1 = Point(100, 200)
        val p2 = Point(200, 100)

        val base = Line(p1, p2)
        val line = base.withLength(50.0)

        assertThat(line.length).isEqualTo(50.0)
        assertThat(line.angle).isEqualTo(base.angle)
        assertThat(line.p1).isEqualTo(base.p1)
        assertThat(line.p2).isNotEqualTo(base.p2)
    }

    @Test
    fun `should normalized angle`() {
        val base = Line(Point(0, 0), Point(10.0, 0.00001))
        assertThat(base.angle).isEqualTo(Angle.degrees(0.0))
    }

    @Test
    fun `should give point at percent`() {
        val line = Line(Point(10, 10), Point(20, 20))
        assertThat(line.pointAtPercent(0.5)).isEqualTo(Point(15, 15))
    }

    @Test
    fun `should give point at length`() {
        val line = Line(Point(0, 0), Point(0, 100))
        assertThat(line.pointAtLength(10)).isEqualTo(Point(0, 10))
        assertThat(line.pointAtLength(100)).isEqualTo(Point(0, 100))
    }

    @Test
    fun `should intersect another line - bounded`() {
        val l1 = Line(Point(5, 10), Point(20, 20))
        val l2 = Line(Point(10, 20), Point(20, 10))
        val intersection = l1.intersects(l2)

        assertThat(intersection.type).isEqualTo(IntersectionType.Bounded)
        assertThat(intersection.p).isEqualTo(Point(14, 16))
    }

    @Test
    fun `should intersect another line - unbounded`() {
        var l1 = Line(Point(25, 10), Point(40, 20))
        var l2 = Line(Point(10, 20), Point(20, 10))
        var intersection = l1.intersects(l2)

        assertThat(intersection.type).isEqualTo(IntersectionType.Unbounded)
        assertThat(intersection.p).isEqualTo(Point(22, 8))
        assertThat(l2.intersects(l1)).isEqualTo(l1.intersects(l2))

        l1 = Line(Point(-1, 0), Point(-1, 1))
        l2 = Line(Point(0, 1), Point(1, 1))
        intersection = l1.intersects(l2)

        assertThat(intersection.type).isEqualTo(IntersectionType.Unbounded)
        assertThat(intersection.p).isEqualTo(Point(-1.0, 1.0))
        assertThat(l2.intersects(l1)).isEqualTo(l1.intersects(l2))
    }

    @Test
    fun `should not intersect a parallel line - unbounded`() {
        val l1 = Line(Point(10, 10), Point(20, 10))
        val l2 = Line(Point(10, 20), Point(20, 20))
        val intersection = l1.intersects(l2)

        assertThat(intersection.type).isEqualTo(IntersectionType.None)
    }
}

class LineAssert(actual: Line): AbstractAssert<LineAssert, Line>(actual, LineAssert::class.java) {
    fun assertThat(actual: Point): PointAssert {
        return PointAssert(actual)
    }
    fun isEqualTo(other: Line): LineAssert {
        assertThat(actual.p1).isEqualTo(other.p1)
        assertThat(actual.p2).isEqualTo(other.p2)
        return this
    }
}
