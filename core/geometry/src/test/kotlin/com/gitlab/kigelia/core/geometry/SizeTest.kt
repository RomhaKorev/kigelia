package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SizeTest: ValueObjectTest<Size> {
    override fun createValue() = Size(12, 34)
    override fun createOtherValue() = Size(56, 78)

    @Test
    fun `should say if it's a square`() {
        assertThat(Size(4, 4).isSquare()).isTrue
        assertThat(Size(4, 7).isSquare()).isFalse
    }

    @Test
    fun `should normalize size`() {
        assertThat(Size(-4, -5)).isEqualTo(Size(4, 5))
    }

    @Test
    fun `should sum sizes`() {
        assertThat(Size(4, 5) + Size(3, 6)).isEqualTo(Size(7, 11))
    }

    @Test
    fun `should subtract sizes`() {
        assertThat(Size(4, 5) - Size(3, 6)).isEqualTo(Size(1, 1))
    }

    @Test
    fun `should divide size`() {
        assertThat(Size(4, 5) / 2.0 ).isEqualTo(Size(2, 2.5))
    }

    @Test
    fun `should multiply size`() {
        assertThat(Size(4, 5) * 2.0 ).isEqualTo(Size(8, 10))
    }

    @Test
    fun `should destructure a size`() {
        val (w, h) = Size(10, 20)

        assertThat(w).isEqualTo(10.0)
        assertThat(h).isEqualTo(20.0)
    }

    @Test
    fun `should display a size`() {
        assertThat(Size(10, 20).toString()).isEqualTo("Size(width=10.0, height=20.0)")
    }
}
