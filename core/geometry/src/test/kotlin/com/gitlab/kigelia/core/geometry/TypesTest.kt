package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.tan
import kotlin.random.Random
import org.assertj.core.data.Offset as AOffset

class TypesTest {

    @Test
    fun `angles should be radian or degrees`() {
        assertThat(Angle.radians(PI)).isEqualTo(Angle.degrees(180.0))
        assertThat(Angle.radians(PI / 2)).isEqualTo(Angle.degrees(90.0))
        assertThat(Angle.radians(0.0)).isEqualTo(Angle.degrees(0.0))
    }

    @Test
    fun normalized() {
        Angle.radians(-PI).`should be`(Angle.radians(PI))
        Angle.radians(0).`should be`(Angle.radians(2 * PI))
        Angle.radians(PI).`should be`(Angle.radians(-PI))
    }

    @RepeatedTest(50)
    fun `angle computation`() {
        val theta = Random.nextDouble(-PI, PI)
        assertThat(Angle.radians(theta).cos).isCloseTo(cos(theta), AOffset.offset(0.000001))
        assertThat(Angle.radians(theta).sin).isCloseTo(sin(theta), AOffset.offset(0.000001))
        assertThat(Angle.radians(theta).tan).isCloseTo(tan(theta), AOffset.offset(0.000001))
    }


    @Test
    fun addition() {
        0 .`+`(0).`should be`(0)
        0 .`+`(PI).`should be`(PI)
        PI.`+`(PI).`should be`(0)
        (3.0/2 * PI).`+`(PI).`should be`(PI / 2)
    }

    @Test
    fun substraction() {
        0 .`-`(0).`should be`(0)
        0 .`-`(PI / 2.0).`should be`(3.0 / 2 * PI)
        PI.`-`(PI).`should be`(0)
        (2 * PI).`-`(PI).`should be`(PI)
    }

    @Test
    fun `should get angle value in unit`() {
        assertThat(90.degrees().degrees).isEqualTo(90.0)
        assertThat(90.degrees().radians).isEqualTo(PI / 2)

        assertThat((PI/ 2).radians().degrees).isEqualTo(90.0)
        assertThat((PI/ 2).radians().radians).isEqualTo(PI / 2)
    }

    @RepeatedTest(50)
    fun comparison() {
        val `two distinct angles`: () -> List<Angle> = {
            val a = Random.nextDouble(0.0, 170.0)
            val b = Random.nextDouble(181.0, 360.0 - a)

            listOf(Angle.degrees(a), Angle.degrees(b))
        }
        val (a, b) = `two distinct angles`()

        assertThat(a < b).isTrue
        assertThat(a > b).isFalse
        assertThat(a <= b).isTrue
        assertThat(a >= b).isFalse
        assertThat(a <= a).isTrue
        assertThat(a >= a).isTrue
        assertThat(Angle.radians(0) - a > b).isTrue
        assertThat(Angle.radians(2 * PI + a.theta) < b).isTrue

    }

    private fun Number.`+`(other: Number): Angle {
        return Angle.radians(this) + Angle.radians(other)
    }
    private fun Number.`-`(other: Number): Angle {
        return Angle.radians(this) - Angle.radians(other)
    }

    private fun Angle.`should be`(expected: Number) {
        assertThat(this).isEqualTo(Angle.radians(expected))
    }

    private fun Angle.`should be`(expected: Angle) {
        assertThat(this).isEqualTo(expected)
    }
}
