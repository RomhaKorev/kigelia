package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DPointTest {
    @Test
    fun `should multiply a delta`() {
        assertThat(
            DPoint(
                3,
                5
            ) * -2).isEqualTo(DPoint(-6, -10))
    }

    @Test
    fun `should sum a delta`() {
        assertThat(DPoint(3, 50) - DPoint(10, 20)).isEqualTo(
            DPoint(-7, 30)
        )
        assertThat(DPoint(3, 50) + DPoint(10, 20)).isEqualTo(
            DPoint(13, 70)
        )
    }

    @Test
    fun `should convert to point`() {
        assertThat(DPoint(3, 50).toPoint()).isEqualTo(Point(3, 50))
    }
}
