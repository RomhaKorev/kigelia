package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


class PolygonTest {

    @Test
    fun `should create a polygon`() {
        assertThat(
            Point(10, 20) to Point(30, 50) to Point(100, 80)
        ).isEqualTo(Polygon(Point(10, 20), Point(30, 50), Point(100, 80)))
    }

    @Test
    fun `should give its bounding rect`() {
        assertThat(
            (Point(10, 10) to
            Point(20, 20) to
            Point(15, 30)).boundingRect
        ).isEqualTo(Rectangle(Point(10, 10), Size(10, 20)))
    }

    @Test
    fun `should create polygon with deltas`() {
        assertThat(
            Point(10, 20) to DPoint(30, 50) to DPoint(100, 80)
        ).isEqualTo(Polygon(Point(10, 20), Point(40, 70), Point(140, 150)))

        assertThat(
            Polygon(Point(10, 20), DPoint(30, 50), DPoint(100, 80))
        ).isEqualTo(Polygon(Point(10, 20), Point(40, 70), Point(140, 150)))
    }
}

infix fun Point.to(next: Point) = Polygon(this, next)
infix fun Point.to(next: DPoint) = Polygon(this, this + next)
