package com.gitlab.kigelia.core.geometry

import com.gitlab.kigelia.core.geometry.Anchor
import com.gitlab.kigelia.core.geometry.Point
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AnchorTest {

    @Test
    fun `should give its coordinates`() {
        val anchor = Anchor(
            Point(10.0, 20.0),
            Anchor.Kind.Bottom
        )

        assertThat(anchor.x).isEqualTo(10.0)
        assertThat(anchor.y).isEqualTo(20.0)
    }

    @Test
    fun `should merge kind`() {
        assertThat(Anchor.Kind.Bottom or Anchor.Kind.Left).isEqualTo(setOf(
            Anchor.Kind.Bottom, Anchor.Kind.Left))
        assertThat(Anchor.Kind.Bottom or Anchor.Kind.Bottom).isEqualTo(setOf(
            Anchor.Kind.Bottom))
    }

    @Test
    fun `should be a flag`() {
        assertThat( Anchor.Kind.Bottom.`is`(Anchor.Kind.Bottom or Anchor.Kind.Left)).isTrue
        assertThat( Anchor.Kind.Top.`is`(Anchor.Kind.Bottom or Anchor.Kind.Left)).isFalse
    }

    @Test
    fun `should find the closest anchor from a given point`() {
        val rect = RectGeometry(Point(100, 200), Size(50, 100))

        assertThat(rect.anchorClosedTo(Point(150, 100))).isEqualTo(rect.top())
        assertThat(rect.anchorClosedTo(Point(500, 100))).isEqualTo(rect.right())
        assertThat(rect.anchorClosedTo(Point(150, 500))).isEqualTo(rect.bottom())
        assertThat(rect.anchorClosedTo(Point(0, 250))).isEqualTo(rect.left())
    }
}
