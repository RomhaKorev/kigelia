package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.random.Random


class GeometryTest: ValueObjectTest<Geometry> {
    @ParameterizedTest
    @MethodSource("generateValues")
    annotation class RandomTest

    override fun createValue() = Geometry(10.0, 20.0, 30.0, 40.0)

    override fun createOtherValue() = Geometry(50.0, 60.0, 70.0, 80.0)

    @Test
    fun `validity`() {
        assertThat(Geometry().valid()).isFalse
        assertThat(createValue().valid()).isTrue
    }

    @RandomTest
    fun `setting dimensions`(x: Double, y: Double, width: Double, height: Double) {
        val g = Geometry()
        g.x(x)
        g.y(y)
        g.width(width)
        g.height(height)

        assertThat(g).isEqualTo(Geometry(x, y, width, height))
        assertThat(g.valid()).isTrue
    }

    @RandomTest
    fun `should invalidate a geometry`(x: Double, y: Double, width: Double, height: Double) {
        assertThat(Geometry(x, y, width, height, valid = false).valid()).isFalse
    }

    @RandomTest
    fun `should retrieve dimensions`(x: Double, y: Double, width: Double, height: Double) {
        val g = Geometry(x, y, width, height)

        assertThat(g.x()).isEqualTo(x)
        assertThat(g.y()).isEqualTo(y)
        assertThat(g.width()).isEqualTo(width)
        assertThat(g.height()).isEqualTo(height)
    }

    @RandomTest
    fun `should retrieve dimensions if exisiting`(x: Double, y: Double, width: Double, height: Double) {
        val g = Geometry()

        val xOrNull = if (x.toInt() % 2 == 0) null else x
        val yOrNull = if (y.toInt() % 2 == 0) null else y
        val widthOrNull = if (width.toInt() % 2 == 0) null else width
        val heightOrNull = if (height.toInt() % 2 == 0) null else height

        if (xOrNull != null) g.x(xOrNull)
        if (yOrNull != null) g.y(yOrNull)
        if (widthOrNull != null) g.width(widthOrNull)
        if (heightOrNull != null) g.width(heightOrNull)

        assertThat(g.xOrNull()).isEqualTo(xOrNull)
        assertThat(g.yOrNull()).isEqualTo(yOrNull)
        assertThat(g.widthOrNull()).isEqualTo(widthOrNull)
        assertThat(g.heightOrNull()).isEqualTo(heightOrNull)
    }

    @RandomTest
    fun `should compute the anchors`(x: Double, y: Double, width: Double, height: Double) {
        val g = Geometry(x ,y, width, height)

        assertThat(g.top()).isEqualTo(
            Anchor(
                Point(x + width / 2, y),
                Anchor.Kind.Top
            )
        )
        assertThat(g.bottom()).isEqualTo(
            Anchor(
                Point(x + width / 2, y + height),
                Anchor.Kind.Bottom
            )
        )
        assertThat(g.left()).isEqualTo(
            Anchor(
                Point(x, y + height / 2),
                Anchor.Kind.Left
            )
        )
        assertThat(g.right()).isEqualTo(
            Anchor(
                Point(x + width, y + height / 2),
                Anchor.Kind.Right
            )
        )
    }

    @RandomTest
    fun `should compute the center`(x: Double, y: Double, width: Double, height: Double) {
        val g = Geometry(x ,y, width, height)
        assertThat(g.center()).isEqualTo(Point(x + width / 2, y + height / 2))
    }

    @Test
    fun `should sum geometries`() {
        val g1 = Geometry(10.0, 100.0, 100.0, 100.0)
        val g2 = Geometry(100.0, 10.0, 100.0, 100.0)

        assertThat(g1 + g2).isEqualTo(Geometry(10.0, 10.0, 190.0, 190.0))
    }

    @Test
    fun `should sum with invalid`() {
        val g1 = Geometry(10.0, 100.0, 100.0, 100.0)
        val g2 = Geometry(100.0, 10.0, 100.0, 100.0, valid = false)

        assertThat(g1 + g2).isEqualTo(g1)
        assertThat(g2 + g1).isEqualTo(g1)
    }

    @Test
    fun `should sum with offset`() {
        val g = Geometry(10.0, 100.0, 100.0, 100.0)
        val offset = Offset(10.0, 20.0, 100.0, 100.0)

        assertThat(g + offset).isEqualTo(Geometry(20.0, 120.0, 200.0, 200.0))
    }

    @Test
    fun `should sum with point`() {
        val g = Geometry(10.0, 100.0, 100.0, 100.0)
        val p = Point(10.0, 20.0)

        assertThat(g + p).isEqualTo(Geometry(20.0, 120.0, 100.0, 100.0))
    }

    @Test
    fun `should grow by given padding`() {
        val g = Geometry(10.0, 100.0, 100.0, 100.0)
        val padding = Padding(30.0, 50.0)

        assertThat(g.grownBy(padding)).isEqualTo(Geometry(-20.0, 50.0, 160.0, 200.0))
    }

    @Test
    fun `should provide the translation to the given position`() {
        val g = Geometry(10.0, 100.0, 100.0, 100.0)

        assertThat(g.to(Point(50.0, 50.0))).isEqualTo(Translation(Point(10.0, 100.0), Point(x=50.0, y=50.0)))
    }

    @Test
    fun `should provide its position & size`() {
        val g = Geometry(10.0, 100.0, 100.0, 200.0)

        assertThat(g.position()).isEqualTo(Point(10.0, 100.0))
        assertThat(g.size()).isEqualTo(Size(100.0, 200.0))
    }

    @Test
    fun `should move to the given position`() {
        val g = Geometry(10.0, 100.0, 100.0, 100.0)
        g.position(Point(50.0, 60.0))
        assertThat(g).isEqualTo(Geometry(50.0, 60.0, 100.0, 100.0))
    }

    @Test
    fun `create a geometry from a rectangle`() {
        val g = RectGeometry(Rectangle(Point(10.0, 100.0), Size(100.0, 100.0)))

        assertThat(g).isEqualTo(Geometry(10.0, 100.0, 100.0, 100.0))
    }

    @Test
    fun destructured() {
        val (x, y, width, height) = Geometry(10.0, 50.0, 100.0, 200.0)

        assertThat(x).isEqualTo(10.0)
        assertThat(y).isEqualTo(50.0)
        assertThat(width).isEqualTo(100.0)
        assertThat(height).isEqualTo(200.0)
    }

    @Test
    fun `should resize to the given size`() {
        val g = Geometry(10.0, 100.0, 100.0, 100.0)
        g.size(Size(75, 80))
        assertThat(g.width()).isEqualTo(75.0)
        assertThat(g.height()).isEqualTo(80.0)
    }
    @Test
    fun `should convert to rect`() {
        val r = Geometry(10.0, 100.0, 100.0, 100.0).toRect()
        assertThat(r).isEqualTo(Rectangle(Point(10.0, 100.0), Size(100.0, 100.0)))
    }

    companion object {
        @JvmStatic
        fun generateValues(): List<Arguments> {
            val l = mutableListOf<Arguments>()
            repeat(50) {
                val x = Random.nextDouble()
                val y = Random.nextDouble()
                val width = Random.nextDouble()
                val height = Random.nextDouble()
                l.add(Arguments.of(x, y, width, height))
            }
            return l
        }
    }
}
