package com.gitlab.kigelia.core.geometry

class PaddingTest: ValueObjectTest<Padding> {
    override fun createValue() = Padding(10.0, 20.0)
    override fun createOtherValue() = Padding(30.0, 40.0)
}

class OffsetTest: ValueObjectTest<Offset> {
    override fun createValue() = Offset(10.0, 20.0, 30.0, 40.0)
    override fun createOtherValue() = Offset(30.0, 40.0, 50.0, 60.0)
}
