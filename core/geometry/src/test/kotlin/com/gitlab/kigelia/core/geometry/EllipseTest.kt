package com.gitlab.kigelia.core.geometry

import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.junit.jupiter.api.Test
import kotlin.math.*


class EllipseTest: ValueObjectTest<Ellipse> {
    @Test
    fun `should calculate the position of a point with a percent`() {
        val ellipse = Ellipse(center = Point(50, 50), Size(100.0, 100.0))
        assertThat(ellipse.pointAtPercent(0.0)).isEqualTo(Point(100.0, 50.0))
        assertThat(ellipse.pointAtPercent(0.25)).isEqualTo(Point(50.0, 0.0))
        assertThat(ellipse.pointAtPercent(0.5)).isEqualTo(Point(0.0, 50.0))
        assertThat(ellipse.pointAtPercent(0.75)).isEqualTo(Point(50.0, 100.0))
    }

    @Test
    fun `should calculate the position of a point with an angle`() {
        val ellipse = Ellipse(center = Point(50, 50), Size(100.0, 100.0))
        assertThat(ellipse.pointAtAngle(Angle.radians(0.0))).isEqualTo(Point(100.0, 50.0))
        assertThat(ellipse.pointAtAngle(Angle.radians(PI / 2.0))).isEqualTo(Point(50.0, 0.0))
        assertThat(ellipse.pointAtAngle(Angle.radians(PI))).isEqualTo(Point(0.0, 50.0))
        assertThat(ellipse.pointAtAngle(Angle.radians(3.0 / 2 * PI))).isEqualTo(Point(50.0, 100.0))
    }

    @Test
    fun `should translate ellipse`() {
        val ellipse = Ellipse(Point(50, 50), Size(40, 40))

        assertThat(ellipse.translated(Point(25, 40))).isEqualTo(
            Ellipse(
                Point(25, 40),
                Size(40, 40)
            )
        )
    }

    @Test
    fun `should translate ellipse by an offset`() {
        val ellipse = Ellipse(Point(50, 50), Size(40, 40))

        assertThat(ellipse.translated(DPoint(25, 40))).isEqualTo(
            Ellipse(
                Point(75, 90),
                Size(40, 40)
            )
        )
    }

    @Test
    fun `should calculate the arc length`() {
        val ellipse = Ellipse(Point(0, 0), Size(3.05 * 2, 2.23 * 2))
        assertThat(ellipse.arcLength(Angle.radians(0), Angle.radians(5 * PI / 18))).isCloseTo(2.41375, Offset.offset(0.00001))
    }

    @Test
    fun `should calculate perimeter`() {
        val ellipse = Ellipse(Point(50, 50), Size(40, 40))
        assertThat(ellipse.perimeter).isEqualTo(40 * PI)
    }

    @Test
    fun `calculate its boundingRect`() {
        assertThat(
            Ellipse(Point(50, 50), Size(40, 50)).boundingRect())
        .isEqualTo(
            Rectangle(Point(30, 25), Size(40, 50))
        )

    }

    fun assertThat(actual: Point): PointAssert {
        return PointAssert(actual)
    }

    override fun createValue() = Ellipse(Point(50, 50), Size(40, 40))
    override fun createOtherValue() = Ellipse(Point(25, 30), Size(50, 60))
}

class PointAssert(actual: Point): AbstractAssert<PointAssert, Point?>(actual, PointAssert::class.java) {
        fun isEqualTo(other: Point): PointAssert {
            val u = actual as Point
            if (abs(u.x - other.x) > 0.00001 || abs(u.y - other.y) > 0.00001) {
                failWithMessage("Expected $actual to be $other")
            }
            return this
    }
}

