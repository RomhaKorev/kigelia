package com.gitlab.kigelia.core.geometry

data class Intersection(val type: IntersectionType, val p: Point)
