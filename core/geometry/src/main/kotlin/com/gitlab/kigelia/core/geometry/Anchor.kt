package com.gitlab.kigelia.core.geometry

data class Anchor(val point: Point, val kind: Kind) {
    val x: Double
        get() = point.x

    val y: Double
        get() = point.y

    enum class Kind {
        Top,
        Bottom,
        Left,
        Right;

        infix fun or(other: Kind): Set<Kind> {
            return setOf(this, other)
        }

        infix fun `is`(values: Set<Kind>): Boolean {
            return values.contains(this)
        }
    }
}
