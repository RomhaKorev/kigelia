package com.gitlab.kigelia.core.geometry

import kotlin.math.absoluteValue

data class Size private constructor(val width: Double, val height: Double) {

    constructor(width: Number, height: Number): this( width.toDouble().absoluteValue, height.toDouble().absoluteValue)

    operator fun minus(other: Size): Size {
        return Size(width - other.width, height - other.height).normalized()
    }

    operator fun div(i: Number): Size {
        return Size(width / i.toDouble(), height / i.toDouble()).normalized()
    }

    operator fun times(i: Number): Size {
        return Size(width * i.toDouble(), height * i.toDouble()).normalized()
    }

    private fun normalized() = Size(width.absoluteValue, height.absoluteValue)

    fun isSquare(): Boolean {
        return width == height
    }

    operator fun plus(other: Size): Size {
        return Size(width + other.width, height + other.height)
    }

    override fun equals(other: Any?): Boolean {
        return other is Size && this.width == other.width && this.height == other.height
    }

    override fun hashCode(): Int {
        var result = width.hashCode()
        result = 31 * result + height.hashCode()
        return result
    }

    override fun toString(): String {
        return "Size(width=$width, height=$height)"
    }
}

infix fun Number.by(h: Number) = Size(this, h)
