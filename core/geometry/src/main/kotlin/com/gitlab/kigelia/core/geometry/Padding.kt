package com.gitlab.kigelia.core.geometry

data class Padding(val horizontal: Double, val vertical: Double)
