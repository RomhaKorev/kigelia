package com.gitlab.kigelia.core.geometry


import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.*

fun simpson(f: (x: Double) -> Double, x: Double, h: Double) = (f(x) + 4.0 * f((x + h) / 2.0) + f(x + h)) / 6.0
fun integral(
    from: Double,
    to: Double,
    columns: Int,
    f: (Double) -> Double
): Double {
    val h = (to - from) / columns
    var sum = 0.0
    for (i in 0 until columns) {
        val x = from + i * h
        sum += simpson(f, x, h)
    }
    return sum * h
}

data class Ellipse(val center: Point, val size: Size) {
    val perimeter: Double
        get() {
            val a = size.width / 2.0
            val b = size.height / 2.0
            val h = (a - b).pow(2) / (a + b).pow(2)
            // Ramanujan approximation
            val p = PI * (a + b) * (1 + (3 * h) / (10 + sqrt(4 - 3 * h)))
            return p
        }

    fun pointAtPercent(percent: Number): Point {
        val angle = 2 * Math.PI * percent.toDouble()
        return pointAtAngle(Angle.Companion.radians(angle))
    }

    fun pointAtAngle(angle: Angle): Point {
        val theta = -angle.theta
        val x = BigDecimal(center.x + (size.width / 2.0) * cos(theta)).setScale(6, RoundingMode.HALF_EVEN).toDouble()
        val y = BigDecimal(center.y + (size.height / 2.0) * sin(theta)).setScale(6, RoundingMode.HALF_EVEN).toDouble()
        return Point(x, y)
    }

    fun translated(offset: Point): Ellipse {
        return Ellipse(offset, size)
    }

    fun translated(offset: DPoint): Ellipse {
        return Ellipse(center + offset, size)
    }

    fun arcLength(from: Angle, to: Angle): Double {
        val a = size.width / 2.0
        val b = size.height / 2.0

        val t0 = atan(tan(from.theta) * (a / b))
        val t1 = atan(tan(to.theta) * (a / b))
        val arc = integral(t0, t1, 250000) { t ->
            sqrt(a.pow(2) * sin(t).pow(2) + b.pow(2) * cos(t).pow(2))
        }

        return arc

    }

    fun boundingRect(): Rectangle {
        return Rectangle(
            center - DPoint(size.width / 2.0, size.height / 2.0),
            size
        )
    }
}

/*
t=t1=arctan(a/b * tan50).
 */
