package com.gitlab.kigelia.core.geometry

import kotlin.math.max
import kotlin.math.min


class RectGeometry(private var x: Double? = null,
    private var y: Double? = null,
    private var width: Double? = null,
    private var height: Double? = null,
    private var valid: Boolean = x != null || y != null || width != null || height != null): Geometry {


    constructor(topLeft: Point, size: Size): this(topLeft.x, topLeft.y, size.width, size.height)
    constructor(rectangle: Rectangle): this(rectangle.topLeft, rectangle.size)

    override fun valid(): Boolean {
        return valid
    }

    override fun x(value: Number) {
        x = value.toDouble()
        valid = true
    }

    override fun xOrNull(): Double? = x
    override fun yOrNull(): Double? = y
    override fun widthOrNull(): Double? = width
    override fun heightOrNull(): Double? = height

    override fun y(value: Number) {
        y = value.toDouble()
        valid = true
    }

    override fun width(value: Number) {
        width = value.toDouble()
        valid = true
    }

    override fun height(value: Number) {
        height = value.toDouble()
        valid = true
    }

    override fun x(): Double {
        return x ?: 0.0
    }

    override fun y(): Double {
        return y ?: 0.0
    }

    override fun width(): Double {
        return width ?: 0.0
    }

    override fun height(): Double {
        return height ?: 0.0
    }

    override operator fun plus(other: Geometry): Geometry {
        if (!other.valid())
            return this
        if (!valid)
            return other

        val farLeft = min(this.left().x, other.left().x)
        val farTop = min(this.top().y, other.top().y)
        val farRight = max(this.right().x, other.right().x)
        val farBottom = max(this.bottom().y, other.bottom().y)

        return RectGeometry( farLeft, farTop, Math.abs(farRight - farLeft), Math.abs(farTop - farBottom), true )
    }

    override operator fun plus(other: Offset): Geometry {
        return RectGeometry(
            x() + other.dx.toDouble(),
            y() + other.dy.toDouble(),
            width() + other.dw.toDouble(),
            height() + other.dh.toDouble()
        )
    }

    override operator fun plus(translation: Point): Geometry {
        return this + Offset(translation.x, translation.y, 0, 0)
    }

    override fun equals(other: Any?): Boolean {
        return other is Geometry
                && x() == other.x()
                && y() == other.y()
                && width() == other.width()
                && height() == other.height()
    }

    override fun hashCode(): Int {
        var result = x?.hashCode() ?: 0
        result = 31 * result + (y?.hashCode() ?: 0)
        result = 31 * result + (width?.hashCode() ?: 0)
        result = 31 * result + (height?.hashCode() ?: 0)
        result = 31 * result + valid.hashCode()
        return result
    }

/*    override fun toString(): String {
        return "$x $y $width $height"
    }
 */
}
