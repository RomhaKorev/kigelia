package com.gitlab.kigelia.core.geometry

data class Offset(val dx: Number, val dy: Number, val dw: Number, val dh: Number)
