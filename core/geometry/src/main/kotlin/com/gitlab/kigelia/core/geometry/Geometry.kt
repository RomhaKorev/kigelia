package com.gitlab.kigelia.core.geometry



interface Geometry {

    operator fun component1(): Double = x()
    operator fun component2(): Double = y()
    operator fun component3(): Double = width()
    operator fun component4(): Double = height()

    fun valid(): Boolean
    fun x(value: Number)
    fun y(value: Number)
    fun width(value: Number)
    fun height(value: Number)

    fun x(): Double
    fun y(): Double
    fun width(): Double
    fun height(): Double

    fun xOrNull(): Double?
    fun yOrNull(): Double?
    fun widthOrNull(): Double?
    fun heightOrNull(): Double?

    fun position(p: Point) {
        x(p.x)
        y(p.y)
    }

    fun size(size: Size) {
        width(size.width)
        height(size.height)
    }

    fun position(): Point = Point(x(), y())
    fun size(): Size = Size(width(), height())

    fun center(): Point {
        return Point(
            this.x() + width() / 2.0,
            this.y() + height() / 2.0
        )
    }

    fun top(): Anchor {
        return Anchor(
            Point(center().x, y()),
            Anchor.Kind.Top
        )
    }

    fun bottom(): Anchor {
        return Anchor(
            Point(center().x, y() + height()),
            Anchor.Kind.Bottom
        )
    }

    fun left(): Anchor {
        return Anchor(
            Point(x(), center().y),
            Anchor.Kind.Left
        )
    }

    fun right(): Anchor {
        return Anchor(
            Point(x() + width(), center().y),
            Anchor.Kind.Right
        )
    }

    fun to(destination: Point): Translation {
        val source = this.position()

        return Translation(source, destination)
    }

    fun toRect() = Rectangle(Point(x(), y()), Size(width(), height()))

    operator fun plus(other: Geometry): Geometry

    operator fun plus(other: Offset): Geometry

    operator fun plus(translation: Point): Geometry

    fun grownBy(padding: Padding): Geometry {
        return RectGeometry(this.x() - padding.horizontal,
                            this.y() - padding.vertical,
                            this.width() + padding.horizontal * 2.0,
                            this.height() + padding.vertical * 2.0
                           )
    }

    fun anchorClosedTo(point: Point): Anchor {
        return listOf(top(), bottom(), left(), right()).minByOrNull { point.distance(it.point) }!!
    }

    companion object {
        operator fun invoke(): Geometry {
            return RectGeometry()
        }

        operator fun invoke(x: Double, y: Double, width: Double, height: Double, valid: Boolean): Geometry {
            return RectGeometry(x, y, width, height, valid)
        }

        operator fun invoke(x: Double, y: Double, width: Double, height: Double): Geometry {
            return RectGeometry(x, y, width, height)
        }
    }
}


