package com.gitlab.kigelia.core.geometry

import java.lang.Double.min
import kotlin.math.max


private fun normalize(topLeft: Point, bottomRight: Point): Point {
    return Point(min(topLeft.x, bottomRight.x), min(topLeft.y, bottomRight.y))
}

data class Rectangle(val topLeft: Point, val size: Size) {

    constructor(topLeft: Point, bottomRight: Point): this(normalize(topLeft, bottomRight),
                                                          Size(topLeft.x - bottomRight.x, topLeft.y - bottomRight.y))

    fun resized(size: Size) = Rectangle(topLeft, size)
    fun resized(resize: RelativeResize) = Rectangle(topLeft, resize * size)
    fun moved(point: Point) = Rectangle(point, size)
    fun moved(delta: DPoint) = Rectangle(topLeft + delta, size)
    fun shrinkedBy(newSize: Size): Rectangle {
        val s = Size(min(size.width, newSize.width), min(size.height, newSize.height))
        val offset = DPoint(s.width / 2.0, s.height / 2.0)
        return Rectangle(topLeft + offset, size - s)
    }


    fun grownBy(newSize: Size): Rectangle {
        val offset = DPoint(newSize.width / 2.0, newSize.height / 2.0)
        return Rectangle(topLeft - offset, size + newSize)
    }


    fun resized(width: Number = size.width, height: Number = size.height): Rectangle {
        return Rectangle(topLeft, Size(width, height))
    }

    fun centeredOn(p: Point): Rectangle {
        val offset = p - center
        return moved(offset)
    }

    val topRight = topLeft + DPoint(size.width, 0)
    val bottomLeft = topLeft + DPoint(0, size.height)
    val bottomRight = topLeft + DPoint(size.width, size.height)
    val center = topLeft + DPoint(size.width / 2.0, size.height / 2.0)
    val right = center + DPoint(size.width/2.0, 0)
    val left = center + DPoint(-size.width/2.0, 0)
}
