package com.gitlab.kigelia.core.geometry

import kotlin.math.*

data class Angle private constructor(val theta: Double) {
    fun normalized(): Angle {
        if (theta < 0.0) {
            return Angle(theta + 2 * PI)
        }
        return Angle(theta)
    }

    val tan: Double
        get() = tan(theta)

    val cos: Double
        get() = cos(theta)

    val sin: Double
        get() = sin(theta)

    operator fun minus(other: Angle): Angle {
        return radians(theta - other.theta)
    }

    operator fun plus(other: Angle): Angle {
        return radians(theta + other.theta)
    }

    operator fun compareTo(right: Angle): Int {
        return this.theta.compareTo(right.theta)
    }

    val degrees: Double
        get() = theta * 360.0 / (2 * PI)
    val radians: Double
        get() = theta

    companion object {
        fun degrees(angle: Number): Angle {
            return radians(angle.toDouble() * (2 * PI) / 360.0)
        }

        fun radians(theta: Number): Angle {
            if (abs(theta.toDouble()) < 0.000001)
                return Angle(0.0)
            val normalized = theta.toDouble() - (2 * PI) * floor((theta.toDouble() + PI) / (2 * PI))
            if (abs(normalized) < 0.000001)
                return Angle(0.0)
            return Angle(normalized).normalized()
        }
    }
}


fun Number.degrees() = Angle.degrees(this)
fun Number.radians() = Angle.radians(this)

data class RelativeResize(val w: Percent, val h: Percent) {
    operator fun times(s: Size): Size {
        return Size(w * s.width, h * s.height)
    }
}


data class Percent(private val value: Double) {
    infix fun by(h: Percent) = RelativeResize(this, h)
    operator fun times(n: Number) = n.toDouble() * value / 100.0
}

val Number.percent
    get() = Percent(this.toDouble())
