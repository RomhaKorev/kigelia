package com.gitlab.kigelia.core.geometry

import kotlin.math.*

data class Line(val p1: Point, val p2: Point) {
    val length: Double
        get() = sqrt((p2.x - p1.x).pow(2) + (p2.y - p1.y).pow(2))

    val angle: Angle
        get() {
            val dx = p2.x - p1.x
            val dy = p2.y - p1.y
            return Angle.radians(atan2(-dy, dx))
        }

    fun withAngle(value: Angle): Line {
        val l = length
        val dx = value.cos * l
        val dy = -value.sin * l
        return Line(p1, p1 + DPoint(dx, dy))
    }

    fun withLength(value: Double): Line {
        return Line(p1, p1 + DPoint(value, 0)).withAngle(this.angle)
    }

    fun pointAtPercent(percent: Number): Point {
        val t = percent.toDouble()
        return Point(p1.x + (p2.x - p1.x) * t, p1.y + (p2.y - p1.y) * t)
    }

    fun pointAtLength(percent: Number): Point {
        val t = min(1.0, percent.toDouble() / this.length)
        return pointAtPercent(t)
    }

    fun intersects(other: Line): Intersection {
        // Based on Graphics Gems III's "Faster Line Segment Intersection"
        val a = p2 - p1;
        val b = other.p1 - other.p2;
        val c = p1 - other.p1;
        val denominator = a.y * b.x - a.x * b.y
        if (denominator == 0.0)
            return Intersection(IntersectionType.None, Point(Double.MIN_VALUE, Double.MIN_VALUE))
        val reciprocal = 1.0 / denominator
        val na = (b.y * c.x - b.x * c.y) * reciprocal
        val intersectionPoint = p1 + a * na
        if (na < 0 || na > 1)
            return Intersection(IntersectionType.Unbounded, intersectionPoint)
        val nb = (a.x * c.y - a.y * c.x) * reciprocal
        if (nb < 0 || nb > 1)
            return Intersection(IntersectionType.Unbounded, intersectionPoint)
        return Intersection(IntersectionType.Bounded, intersectionPoint)
    }
}
