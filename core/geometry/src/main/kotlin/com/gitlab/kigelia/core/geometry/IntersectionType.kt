package com.gitlab.kigelia.core.geometry

enum class IntersectionType {
    None,
    Unbounded,
    Bounded
}
