package com.gitlab.kigelia.core.geometry

enum class Direction {
    Clockwise,
    CounterClock
}
