package com.gitlab.kigelia.core.geometry

data class DPoint private constructor(val x: Double, val y: Double) {

    constructor(x: Number, y: Number): this(x.toDouble(), y.toDouble())
    operator fun plus(other: Point): Point {
        return Point(x + other.x, y + other.y)
    }

    operator fun minus(other: Point): Point {
        return Point(x - other.x, y - other.y)
    }

    operator fun minus(other: DPoint): DPoint {
        return DPoint(x - other.x, y - other.y)
    }

    operator fun plus(other: DPoint): DPoint {
        return DPoint(x + other.x, y + other.y)
    }

    operator fun times(i: Number): DPoint {
        return DPoint(x * i.toDouble(), y * i.toDouble())
    }

    fun toPoint(): Point {
        return Point(x, y)
    }
}
