package com.gitlab.kigelia.core.geometry

data class Polygon(val points: List<Point>) {
    val boundingRect: Rectangle

    init {
        val minX = points.minOfOrNull { it.x } ?: 0
        val maxX = points.maxOfOrNull { it.x } ?: 0
        val minY = points.minOfOrNull { it.y } ?: 0
        val maxY = points.maxOfOrNull { it.y } ?: 0

        boundingRect = Rectangle(Point(minX, minY), Point(maxX, maxY))
    }

    constructor(vararg points: Point): this(points.toList())
    constructor(point: Point, vararg offsets: DPoint): this(offsets.fold(listOf(point)) { acc, offset ->
        acc + listOf(acc.last() + offset)
    })//this(listOf(point) + offsets.map { point + it })
    infix fun to(next: Point) = Polygon(*this.points.toTypedArray(), next)
    infix fun to(next: DPoint) = Polygon(*this.points.toTypedArray(), (this.points.lastOrNull() ?: Point(0, 0)) + next)
}
