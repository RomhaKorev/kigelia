package com.gitlab.kigelia.core.geometry

import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

typealias Position = Point
data class Point(val x: Double, val y: Double) {

    constructor(x: Number, y: Number): this(x.toDouble(), y.toDouble())

    operator fun minus(other: Point): DPoint {
        return DPoint(x - other.x, y - other.y)
    }

    operator fun plus(other: DPoint): Point {
        return Point(x + other.x, y + other.y)
    }

    operator fun minus(other: DPoint): Point {
        return Point(x - other.x, y - other.y)
    }

    fun toDPoint(): DPoint {
        return DPoint(x, y)
    }

    fun distance(other: Point): Double {
        return sqrt((this.x - other.x).pow(2.0) + (this.y - other.y).pow(2.0))
    }

    operator fun times(n: Number): Point {
        return Point(x * n.toDouble(), y * n.toDouble())
    }

    companion object {
        operator fun invoke(): Point {
            return Point(Double.MIN_VALUE, Double.MIN_VALUE)
        }
    }
}


fun List<Point>.boundingRect(): Geometry {
    val minX = this.map { it.x }.minOrNull() ?: 0.0
    val minY = this.map { it.y }.minOrNull() ?: 0.0
    val maxX = this.map { it.x }.maxOrNull() ?: 0.0
    val maxY = this.map { it.y }.maxOrNull() ?: 0.0

    return RectGeometry(minX, minY, abs(maxX - minX), abs(maxY - minY))
}


data class Translation(private val source: Point, private val destination: Point) {
    val delta: Point
        get() = Point(dx, dy)
    val dx: Double
        get() = destination.x - source.x

    val dy: Double
        get() = destination.y - source.y

    val valid: Boolean
        get() = source != destination
}

