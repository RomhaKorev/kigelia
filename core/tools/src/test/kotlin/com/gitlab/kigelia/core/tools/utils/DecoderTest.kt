package com.gitlab.kigelia.core.tools.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows


class DecoderTest {
    @Test
    fun `should decode a int field`() {
        val data = listOf<Byte>(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        data class Header(val version: Int)
        val h = data.decode<Header>()
        assertThat(h.version).isEqualTo(0x01020304)
    }

    @Test
    fun `should decode a short field`() {
        val data = listOf<Byte>(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        data class Header(val version: Short)
        val h = data.decode<Header>()
        assertThat(h.version).isEqualTo(0x0102)
    }

    @Test
    fun `should decode fields one by one`() {
        val data = listOf<Byte>(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        data class Header(val version: Int, val numTable: Short)
        val h = data.decode<Header>()
        assertThat(h.version).isEqualTo(0x01020304)
        assertThat(h.numTable).isEqualTo(0x0506)
    }

    @Test
    fun `should raise an error if fields cannot be decoded`() {
        val data = listOf<Byte>(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        data class Header(val version: Int, val numTable: List<String>)

        assertThrows<CannotDecodeType> {
            val h = data.decode<Header>()
        }
    }

    @Test
    fun `decoding fixed size string`() {
        val data = listOf<Byte>(0x41, 0x42, 0x43, 0x44, 1, 2)
        data class Header(@FixedSize(4) val version: String, val numTable: Short)

        val h = data.decode<Header>()

        assertThat(h.version).isEqualTo("ABCD")
    }
}

