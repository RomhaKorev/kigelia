package com.gitlab.kigelia.core.tools.utils

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

data class SFNTHeader(val version: Int, val numTable: Short) {
    init {
        require(version == 0x00_01_00_00) { "The file format is unknown (version: $version instead of${0x00_01_00_00})" }
    }
}


class  SFNTFile private constructor(bytes: List<Byte>) {
    val header: SFNTHeader

    val version: Int
        get() = header.version
    val numTables: Short
        get() = header.numTable

    init {
        header = bytes.decode()
    }

    companion object {
        fun decode(bytes: List<Byte>): SFNTFile {
            return SFNTFile(bytes)
        }
    }
}


class SFNTDecoderTest {

    @Test
    fun `should read the headers`() {
        val data = listOf<Byte>(0, 1, 0, 0, 0, 0)
        val h = SFNTFile.decode(data)

        Assertions.assertThat(h.version).isEqualTo(0x01_00_00)
        Assertions.assertThat(h.numTables).isEqualTo(0)
    }

    @Test
    fun `should decode a table`() {
        val data = listOf(0, 1, 0, 0,
                                0, 1,
                                0x47, 0x44, 0x45, 0x46, (0xB4).toByte(), 0x42, (0xB0).toByte(), (0x82).toByte(), 0x00, 0x02, 0x13, 0x58, 0x00, 0x00, 0x02, 0x62
                                )
        val h = SFNTFile.decode(data)

        Assertions.assertThat(h.version).isEqualTo(0x01_00_00)
        Assertions.assertThat(h.numTables).isEqualTo(1)
    }

    @Test
    fun `should throw an error if not a SFNT file`() {
        val data = listOf<Byte>(0, 2, 0, 0, 0, 0)

        assertThrows<IllegalArgumentException> {
            SFNTFile.decode(data)
        }
    }
}