package com.gitlab.kigelia.core.tools.utils

object Logger {
    private val messages: MutableList<String> = mutableListOf()

    fun warn(message: String) {
        messages.add(message)
    }

    fun messages(): List<String> {
        return messages.toList()
    }

    fun clear() {
        messages.clear()
    }
}
