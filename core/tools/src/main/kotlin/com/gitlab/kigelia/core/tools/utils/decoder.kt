package com.gitlab.kigelia.core.tools.utils

import java.lang.RuntimeException
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.jvm.javaType


inline fun <reified T> List<Byte>.decode(): T {

    var offset = 0
    val params = T::class.constructors.first().parameters.map {
        val (value, size) = this.drop(offset).extract(it)
        offset += size
        value
    }

    try {
        return T::class.constructors.first().call(*params.toTypedArray())
    } catch (e : InvocationTargetException) {
        throw e.targetException
    }
}

class CannotDecodeType: RuntimeException()

fun List<Byte>.readInt(): FieldValue {
    val u =  take(4).mapIndexed {
            i, b -> b.toInt() shl ((3 -i) * 8)
    }

    return FieldValue(u.reduce { u, b -> u or b }, 4)
}

fun List<Byte>.readShort(): FieldValue {
    val u = take(2).mapIndexed {
            i, b -> b.toInt() shl ((1 -i) * 8)
    }.reduce { u, b -> u or b }.toShort()

    return FieldValue(u, 2)
}

fun List<Byte>.readString(size: Int): String {
    return take(size).map {
            b -> Char(b.toUShort())
    }.joinToString("")
}

data class FieldValue(val value: Any, val size: Int)

fun List<Byte>.extract(attribute: KParameter): FieldValue {
    val type = attribute.type
    return when(type.javaType) {
        Int::class.java -> this.readInt()
        Short::class.java -> this.readShort()
        String::class.java -> {
            val annotation = attribute.annotations.first { it is FixedSize } as FixedSize
            FieldValue(this.readString(annotation.size), annotation.size)
        }
        else -> throw CannotDecodeType()
    }
}

@Retention(AnnotationRetention.RUNTIME)
annotation class FixedSize(val size: Int)