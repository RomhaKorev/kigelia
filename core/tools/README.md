# DSL for Hexa of Content

DSL for Hexa of Content is a side project used to build SVG images -- and other content based on tags (html, xml, etc.).

It is used in particular by the Hexa of Content project

## Tests

```bash
mvn test
```

## Basic usage

### Tag with content

See [tests]("./src/test/kotlin/ari/home/hexaofcontent/dsl/svg") for more examples
```kotlin
"svg" {
    "polygon" {
        "points"("1 2, 3 4, 5 6")
    }
}
```

### Text
```kotlin
"h2" {
    - "This is a title"
}
```

### Attributes

```kotlin
    line {
        "stroke-width"("2px")
    }
```

## Helpers

The package `ari.home.hexaofcontent.dsl.svg.helpers` contains a set of helper functions to create complex items

### Boxed text
```kotlin
boxedText {
    geometry {
        x(12)
        y(23)
        width(120)
        height(90)
    }
    text("Hello")
}
```

### Connectors
#### Simple line between two items
```kotlin
"svg" {
    rect {
        id("hello")
        geometry {
            x(20)
            y(20)
            width(100)
            height(50)
        }
    }

    boxedText {
        id("world")
        geometry {
            x(150)
            y(20)
            width(100)
            height(50)
        }
    }
    connect("hello".right(), "world".left()){
        style {
            "stroke"("black")
            "stroke-width"("2")
        }
    }
}
```

#### Arrow
```kotlin
"svg" {
    rect {
        id("hello")
        geometry {
            x(20)
            y(20)
            width(100)
            height(50)
        }
    }

    boxedText {
        id("world")
        geometry {
            x(150)
            y(20)
            width(100)
            height(50)
        }
    }
    connect("hello".right(), "world".left(), ending=Marker.Arrow, , starting=Marker.Arrow){
        style {
            "stroke"("black")
            "stroke-width"("2")
        }
    }
}
```
