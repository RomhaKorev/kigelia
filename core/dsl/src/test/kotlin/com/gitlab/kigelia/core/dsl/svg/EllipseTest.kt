package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.elements.ellipse
import com.gitlab.kigelia.core.geometry.Geometry
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class EllipseTest {

    @Test
    fun `should create an ellipse`() {
        orphan {
            ellipse {
                geometry {
                    x(10)
                    y(20)
                    width(100)
                    height(50)
                }
            } `should give`
                    """
                <ellipse cx="60" cy="45" rx="50" ry="25" />
                """.trimIndent()
        }
    }

    @Test
    fun `should create an ellipse from svg attributes`() {
        orphan {
            val e = ellipse {
                    cx(60)
                    cy(45)
                    rx(50)
                    ry(25)
                }
            assertThat(e.geometry()).isEqualTo(Geometry(10.0, 20.0, 100.0, 50.0))
            }
    }

    @Test
    fun `should create an ellipse from convenience functions`() {
        orphan {
            val e = ellipse {
                at(60, 45)
                size(50, 25)
            }
            assertThat(e.geometry()).isEqualTo(Geometry(10.0, 20.0, 100.0, 50.0))
        }
    }
}
