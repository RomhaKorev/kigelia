package com.gitlab.kigelia.core.dsl.common

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TagTest {
    @Test
    fun `should hide a tag`() {
        val tag = Tag("foo")

        tag.hideIf(true)
        Assertions.assertEquals(false, tag.visible())
    }

    @Test
    fun `should hide a tag and its children`() {
        val parent = Tag("parent")
        val child = Tag("child")
        child.hideIf(true)
        val grandChildren = Tag("grand-children")
        child.add(grandChildren)
        parent.add(child)
        parent.add(Tag("Child 2"))
        Assertions.assertEquals(parent.render(), """
            <parent>
                <Child 2 />
            </parent>
        """.trimIndent())
    }

    @Test
    fun `should create container`() {
        val hidden = Tag("Child 3")
        hidden.hideIf(true)
        val parent = Tag("")
        parent.add(Tag("Child 1"))
        parent.add(Tag("Child 2"))
        parent.add(hidden)
        Assertions.assertEquals(parent.render(), """
            <Child 1 />
            <Child 2 />
        """.trimIndent())
    }

    @Test
    fun `should a text`() {
        val tag = Tag("foo")

        val addText: Tag.() -> Unit = {
            - "This is my content"
        }

        tag.addText()

        tag.forEach {
            assertThat(it is TextContent).isTrue
            assertThat((it as TextContent).render()).isEqualTo("This is my content")
        }
    }

    @Test
    fun `should replace text`() {
        val tag = Tag("foo")

        val addText: Tag.() -> Unit = {
            - "This is my content"
            - "But it should be this"
        }

        tag.addText()

        tag.forEach {
            assertThat(it is TextContent).isTrue
            assertThat((it as TextContent).render()).isEqualTo("But it should be this")
        }
    }

    @Test
    fun `should append text`() {
        val tag = Tag("foo")

        val addText: Tag.() -> Unit = {
            - "This is my content"
            + "And this should be the second content"
        }

        tag.addText()

        tag.forEach {
            assertThat(it is TextContent).isTrue
            assertThat((it as TextContent).render()).isEqualTo("This is my content And this should be the second content")
        }
    }
}
