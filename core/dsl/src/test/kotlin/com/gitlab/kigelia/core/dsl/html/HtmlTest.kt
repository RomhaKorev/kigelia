package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.html.elements.div
import com.gitlab.kigelia.core.dsl.html.elements.h
import com.gitlab.kigelia.core.dsl.svg.`should contain`
import com.gitlab.kigelia.core.dsl.svg.`should give`
import org.junit.jupiter.api.Test

class HtmlTest {

    @Test
    fun `Should Create a html content with head and body`() {
        html {
            head {
                meta {
                    name("viewport")
                    content("width=device-width")
                }
                link {
                    rel("stylesheet")
                    href("./css/foobar.css")
                }
            }
            body {
                h(1) {
                    - "Foobar"
                }
            }
        } `should give`
                """
                <html>
                    <head>
                        <meta name="viewport" name="width=device-width">
                        <link rel="stylesheet" href="./css/foobar.css">
                    </head>
                    <body>
                        <h1>Foobar</h1>
                    </body>
                </html>
                """.trimIndent()
    }



    @Test
    fun `should create custom node`() {
        html {
            body {
                "tbody" {
                    cssClass("class-1", "class-2")
                    cssClass("class-3")
                }
            }
        } `should contain` """<tbody class="class-1 class-2 class-3" />"""
    }

    @Test
    fun `should add style in head`() {
        html {
            head {
                css {
                    """
                        .foobar {
                            padding: 16px
                        }
                    """.trimIndent()
                }
            }
        } `should contain` """
            <head>
                <style>
                    .foobar {
                        padding: 16px
                    }
                </style>
            </head>
        """.trimIndent()
    }
}
