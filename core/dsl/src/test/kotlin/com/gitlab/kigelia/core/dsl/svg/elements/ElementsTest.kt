package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Renderer
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Size
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

class ElementsTest {

    @ParameterizedTest
    @MethodSource("builder")
    fun `two identical elements should be equals`(generator: () -> Renderer) {
        assertThat(generator()).isEqualTo(generator())
        assertThat(generator().hashCode()).isEqualTo(generator().hashCode())
    }

    companion object {
        @JvmStatic
        fun builder(): List<Arguments> {
            return listOf(
                Arguments.of({ orphan { ellipse { center(Point(30, 50)); size(Size(100, 50)) } } }),
                Arguments.of({ orphan { circle { center(Point(30, 50)); r(100) } } }),
                Arguments.of({ orphan { rect { x(10); y(50); width(25); height(60)} } }),
                Arguments.of({ orphan { line { p1(Point(30, 50)); p2(Point(70, 100))} } }),
                Arguments.of({ orphan { path { move(10, 20); move(Point(50.0, 50.0)) }}}),
                Arguments.of({ orphan { polygon { points(Point(2.0, 3.0), Point(15.0, 3.0), Point(15.0, 20.0), Point(2.0, 20.0), ) } } }),
                Arguments.of({ orphan { polyline { points(Point(2.0, 3.0), Point(15.0, 3.0), Point(15.0, 20.0), Point(2.0, 20.0), ) } } }),
                Arguments.of({ orphan { text { x(10); y(20); - "This is a label" } } }),
                Arguments.of({ orphan { g { rect { geometry { x(10); y(10); width(50); height(50) } }; rect { geometry { x(60); y(20); width(50); height(50); } }  } } })
            )
        }
    }
}

