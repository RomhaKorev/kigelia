package com.gitlab.kigelia.core.dsl.svg

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ViewboxTest {

    @Test
    fun `undefined value should be 0`() {
        val viewbox = Viewbox()
        viewbox.height(100)
        viewbox.width(200)

        assertThat(viewbox.render()).isEqualTo("""viewBox="0 0 200 100"""")
    }
}