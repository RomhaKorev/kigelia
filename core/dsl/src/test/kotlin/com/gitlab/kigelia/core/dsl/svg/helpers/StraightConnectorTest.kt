package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType

class StraightConnectorTest: ConnectorTest(ConnectorType.Straight) {

    override fun horizontalConnector(): String = """
                        <line x1="120" y1="45" x2="150" y2="45" style="stroke: black; stroke-width: 2" />
                """

    override fun verticalConnector(): String = """
                    <line x1="70" y1="70" x2="70" y2="150" style="stroke: black; stroke-width: 2" />
                """

    override fun rightToTopConnector(): String = """
                    <line x1="120" y1="45" x2="250" y2="150" style="stroke: black; stroke-width: 2" />
                """

    override fun bottomToLeftConnector(): String  = """
        <line x1="70" y1="70" x2="200" y2="175" />
    """.trimIndent()

    override fun diagonalConnector(): String = """
                        <line x1="70" y1="70" x2="170" y2="150" style="stroke: black; stroke-width: 2" />
                """

    override fun endingByAnArrow(): String = """
                    <defs>
                        <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                            <polygon points="0 0, 10 3.5, 0 7" />
                        </marker>
                    </defs>
                    <line x1="70" y1="70" x2="154.3826238111394" y2="137.50609904891152" marker-end="url(#endarrow-0)" style="stroke: black; stroke-width: 2" />
                """

    override fun startingAndEndingByAnArrow() = """
                    <defs>
                        <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                            <polygon points="0 0, 10 3.5, 0 7" />
                        </marker>
                        <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                            <polygon points="10 0, 10 7, 0 3.5" />
                        </marker>
                    </defs>
                    <line x1="140" y1="45" x2="160" y2="45" marker-end="url(#endarrow-0)" marker-start="url(#startarrow-0)" style="stroke: black; stroke-width: 2" />
                """

    override fun startingByAnArrow() = """
                    <defs>
                        <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                            <polygon points="10 0, 10 7, 0 3.5" />
                        </marker>
                    </defs>
                    <line x1="85.6173761888606" y1="82.49390095108848" x2="170" y2="150" marker-start="url(#startarrow-0)" style="stroke: black; stroke-width: 2" />
                """


    override fun fromBottomToTop(): String = """
                    <line x1="122" y1="32" x2="47" y2="92" style="stroke: black; stroke-width: 2" />
                """

    override fun withLabel(): String = """
            <line x1="120" y1="45" x2="250" y2="55" style="stroke: black; stroke-width: 2" />
            <text x="185" y="50" text-anchor="middle" dominant-baseline="middle">
                foobar
            </text>
    """

    override fun coloredConnector(): String = """
            <defs>
                <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
                    <polygon points="0 0, 10 3.5, 0 7" />
                </marker>
                <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto" stroke="red" fill="red">
                    <polygon points="10 0, 10 7, 0 3.5" />
                </marker>
            </defs>
            <line x1="139.48782391389238" y1="49.497190133975174" x2="230.5121760861076" y2="70.50280986602483" marker-end="url(#endarrow-0)" marker-start="url(#startarrow-0)" stroke="red" />
            <text x="185" y="60" text-anchor="middle" dominant-baseline="middle" fill="red">
                foobar
            </text>"""

    override fun connectorStyle(): String = """
        <defs>
            <marker id="endarrow-0" markerWidth="5" markerHeight="3.5" refX="0" refY="1.75" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
                <polygon points="0 0, 5 1.75, 0 3.5" />
            </marker>
        </defs>
        <line x1="20" y1="40" x2="190.51316701949486" y2="96.83772233983161" marker-end="url(#endarrow-0)" stroke="red" stroke-width="3" />
    """.trimIndent()
}
