package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.elements.text
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.orphan
import org.junit.jupiter.api.Test

class TextTest {
    @Test
    fun `should create a text`() {
        orphan {
            text {
                x(10)
                y(20)
                - "This is a label"
            }
        } `should give` """
            <text x="10" y="20">
                This is a label
            </text>
        """.trimIndent()
    }

    @Test
    fun `should hide width and height`() {
        orphan {
            text {
                geometry {
                    x(10)
                    y(20)
                    width(200)
                    height(300)
                }
                - "This is a label"
            }
        } `should give` """
            <text x="10" y="20">
                This is a label
            </text>
        """.trimIndent()
    }
}
