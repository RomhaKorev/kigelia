package com.gitlab.kigelia.core.dsl.html

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class HtmlPropertyTest {

    @Test
    fun `should be valid with a value`() {
        val property = HtmlProperty("foo", "")

        assertThat(property.valid()).isFalse
        property("bar")
        assertThat(property.valid()).isTrue
    }

    @Test
    fun `should render`() {
        val property = HtmlProperty("foo", "bar")

        assertThat(property.render()).isEqualTo("""foo="bar"""")
    }
}