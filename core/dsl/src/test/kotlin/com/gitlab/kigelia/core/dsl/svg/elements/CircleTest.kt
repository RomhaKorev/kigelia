package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.geometry.Point
import org.junit.jupiter.api.Test

class CircleTest {

    @Test
    fun `should create a circle`() {
        orphan {
            circle {
                cx(10)
                cy(50)
                r(60)
            }
        } `should give` """<circle cx="10" cy="50" r="60" />"""
    }

    @Test
    fun `should create a circle with helper`() {
        orphan {
            circle {
                center(Point(10, 50))
                radius(60)
            }
        } `should give` """<circle cx="10" cy="50" r="60" />"""
    }
}
