package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.invoke
import com.gitlab.kigelia.core.dsl.svg.`should give`
import org.junit.jupiter.api.Test

class UseTest {

    @Test
    fun `Should create a use with reference`() {
        "use" {
            "href"("hexagon")
        } `should give`
                """
                <use href="hexagon" />
                """.trimIndent()
    }
}
