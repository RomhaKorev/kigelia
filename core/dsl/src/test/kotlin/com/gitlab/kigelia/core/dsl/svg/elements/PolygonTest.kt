package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Polygon
import org.junit.jupiter.api.Test

class PolygonTest {
    @Test
    fun `should build a polygon`() {
        orphan {
            polygon {
                points(
                    Point(2.0, 3.0),
                    Point(15.0, 3.0),
                    Point(15.0, 20.0),
                    Point(2.0, 20.0),
                )
            }
        } `should give` """<polygon points="2 3, 15 3, 15 20, 2 20" />"""
    }

    @Test
    fun `should build an empty polyline`() {
        orphan {
            polygon {
                points()
            }
        }.geometry() `should give` Geometry()
    }

    @Test
    fun `should define the geometry according to the points`() {
        orphan {
            polygon {
                points(
                    Point(2.0, 3.0),
                    Point(15.0, 3.0),
                    Point(15.0, 20.0),
                    Point(2.0, 20.0),
                )
            }
        }.geometry() `should give` Geometry(2.0, 3.0, 13.0, 17.0)
    }

    @Test
    fun `should draw a polygon`() {
        val polygon = Polygon() to Point(2.0, 3.0) to
                      Point(15.0, 3.0) to
                      Point(15.0, 20.0) to
                      Point(2.0, 20.0)

        orphan {
            polygon.draw()
        } `should give` """<polygon points="2 3, 15 3, 15 20, 2 20" />"""
    }
}
