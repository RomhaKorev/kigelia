package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.svg
import org.junit.jupiter.api.Test

class PositioningTest {

    @Test
    fun `Should place an item below another one`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    x(120)
                    placeAt(10.below("source"))
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="40" viewBox="120 240 60 40">
                    <rect x="120" y="240" width="60" height="30" id="source" />
                    <rect x="120" y="280" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should place an item below and at right of another one`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(100)
                    y(200)
                    width(50)
                    height(50)
                }
            }

            rect {
                geometry {
                    placeAt(10.below("source"))
                    placeAt(20.rightOf("source"))
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70" height="60" viewBox="100 200 70 60">
                    <rect x="100" y="200" width="50" height="50" id="source" />
                    <rect x="170" y="260" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should place an item above another one`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    height(30)
                    placeAt(10.above("source"))
                    x(120)
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="70" viewBox="120 200 60 70">
                    <rect x="120" y="240" width="60" height="30" id="source" />
                    <rect x="120" y="200" height="30" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should place an item below another one but without modifying x`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    x(90)
                    placeAt(10.below("source"))
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="90" height="40" viewBox="90 240 90 40">
                    <rect x="120" y="240" width="60" height="30" id="source" />
                    <rect x="90" y="280" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should place an item at the right of another one`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    y(240)
                    placeAt(10.rightOf("source"))
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70" height="30" viewBox="120 240 70 30">
                    <rect x="120" y="240" width="60" height="30" id="source" />
                    <rect x="190" y="240" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should place an item at the left of another one`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    width(60)
                    placeAt(10.leftOf("source"))
                    y(240)
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="130" height="30" viewBox="50 240 130 30">
                    <rect x="120" y="240" width="60" height="30" id="source" />
                    <rect x="50" y="240" width="60" />
                </svg>
                """.trimIndent()
    }
}
