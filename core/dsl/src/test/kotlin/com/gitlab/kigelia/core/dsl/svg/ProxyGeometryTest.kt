package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Offset
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProxyGeometryTest {
    @Test
    fun `should match the geometry of the parent`() {
        val element = orphan {
            rect {
                x(12)
                y(34)
                width(300)
                height(100)
            }
        }

        val geometry = ProxyGeometry(element)

        assertThat(geometry.x()).isEqualTo(12.0)
        assertThat(geometry.y()).isEqualTo(34.0)
        assertThat(geometry.width()).isEqualTo(300.0)
        assertThat(geometry.height()).isEqualTo(100.0)
    }

    @Test
    fun `should control the geometry of the parent`() {
        val element = orphan {
            rect {
                x(10)
                y(20)
                width(300)
                height(100)
            }
        }

        val geometry = ProxyGeometry(element)

        geometry.x(11)
        geometry.y(21)
        geometry.width(301)
        geometry.height(101)

        assertThat(element.geometry().x()).isEqualTo(11.0)
        assertThat(element.geometry().y()).isEqualTo(21.0)
        assertThat(element.geometry().width()).isEqualTo(301.0)
        assertThat(element.geometry().height()).isEqualTo(101.0)
    }

    @Test
    fun `should not alter parent geometry`() {
        val element = orphan {
            rect {
                x(10)
                y(20)
                width(300)
                height(100)
            }
        }

        val geometry = ProxyGeometry(element)

        assertThat(geometry + Geometry(12.0, 34.0, 56.0, 78.0)).isEqualTo(geometry)
        assertThat(geometry + Offset(12.0, 34.0, 56.0, 78.0)).isEqualTo(geometry)
        assertThat(geometry + Point(12.0, 34.0)).isEqualTo(geometry)
    }
}
