package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.svg.`should contain`
import org.junit.jupiter.api.Test

class LinkTest {

    @Test
    fun `should add crossorigin flag in link`() {
        html {
            head {
                link {
                    crossOrigin()
                }
            }
        } `should contain` """<link crossorigin>"""
    }
}