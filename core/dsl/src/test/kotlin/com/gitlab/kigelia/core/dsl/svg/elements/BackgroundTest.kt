package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.dsl.svg.elements.Background
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.orphan
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class BackgroundTest {
    @Test
    fun `should have valid geometry attributes`() {
        val element = orphan {
            rect {
                x(10)
                y(20)
                height(100)
            }
        }
        val background = Background(element, Color.AltBlue)

        assertThat(background.render()).isEqualTo("""<rect x="10" y="20" height="100" />""")
    }
}