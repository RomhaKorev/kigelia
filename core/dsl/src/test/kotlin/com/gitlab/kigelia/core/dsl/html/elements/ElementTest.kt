package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.core.dsl.svg.`should contain`
import org.junit.jupiter.api.Test


class ElementTest {

    @Test
    fun `should create an image`() {
        html {
            body {
                img("./foo.png") {

                }
            }
        } `should contain` """<img src="./foo.png" />"""
    }

    @Test
    fun `should create an image with alt`() {
        html {
            body {
                img("./foo.png") {
                    alt("alternative text")
                }
            }
        } `should contain` """<img src="./foo.png" alt="alternative text" />"""
    }

    @Test
    fun `should create a figure`() {
        html {
            body {
                figure {
                    img("./elephant.png") {
                        alt("Elephant at sunset")
                    }
                }
            }
        } `should contain` """
            <figure>
                <img src="./elephant.png" alt="Elephant at sunset" />
            </figure>
        """.trimIndent()
    }

    @Test
    fun `should create a figure with caption`() {
        html {
            body {
                figure {
                    img("./elephant.png") {
                        alt("Elephant at sunset")
                    }
                    caption("An elephant at sunset")
                }
            }
        } `should contain` """
            <figure>
                <img src="./elephant.png" alt="Elephant at sunset" />
                <figcaption>An elephant at sunset</figcaption>
            </figure>
        """.trimIndent()
    }

    @Test
    fun `should overwrite figure caption`() {
        html {
            body {
                figure {
                    img("./elephant.png") {
                        alt("Elephant at sunset")
                    }
                    caption("foobar")
                    caption("An elephant at sunset")
                }
            }
        } `should contain` """
            <figure>
                <img src="./elephant.png" alt="Elephant at sunset" />
                <figcaption>An elephant at sunset</figcaption>
            </figure>
        """.trimIndent()
    }
}