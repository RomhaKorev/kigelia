package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.orphan
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TitlesTest {
    @Test
    fun `should build an empty container`() {
        (1 .. 6).forEach {
            val renderer = orphan("") {
                h(it) {
                    - "a title"
                }
            }
            Assertions.assertThat(renderer.render()).isEqualTo(
                """
                    <h$it>a title</h$it>
                """.trimIndent()
            )
        }
    }
}