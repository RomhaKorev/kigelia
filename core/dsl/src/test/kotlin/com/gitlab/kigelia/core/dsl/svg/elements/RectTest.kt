package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.orphan
import org.junit.jupiter.api.Test

class RectTest {

    @Test
    fun `should create a rect`() {
        orphan {
            rect {
                x(10)
                y(50)
                width(25)
                height(60)
            }
        } `should give` """<rect x="10" y="50" width="25" height="60" />"""
    }

    @Test
    fun `should create a rect from dsl`() {
        orphan {
            rect {
                at( 10, 50)
                size(25, 60)
            }
        } `should give` """<rect x="10" y="50" width="25" height="60" />"""
    }
}
