package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.geometry.Anchor
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.Marker
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.Identifier
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.svg
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

abstract class ConnectorTest(val type: ConnectorType) {

    @BeforeEach
    fun setup() {
        Identifier.reset()
    }

    protected abstract fun horizontalConnector(): String
    @Test
    fun `Should create an horizontal connector`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(150)
                    y(20)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".right(), "world".left(), type=type){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="230" height="50" viewBox="20 20 230 50">
                <g id="hello">
                    <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                    <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                        Hello
                    </text>
                </g>
                <g id="world">
                    <rect x="150" y="20" width="100" height="50" fill="none" stroke="black" />
                    <text x="200" y="45" text-anchor="middle" dominant-baseline="middle">
                        world
                    </text>
                </g>
                ${horizontalConnector().trimIndent().trim()}
            </svg>
        """.trimIndent()
    }

    protected abstract fun verticalConnector(): String
    @Test
    fun `Should create a vertical connector`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(20)
                    y(150)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".bottom(), "world".top(), type=type){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="180" viewBox="20 20 100 180">
                    <g id="hello">
                        <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                            Hello
                        </text>
                    </g>
                    <g id="world">
                        <rect x="20" y="150" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="175" text-anchor="middle" dominant-baseline="middle">
                            world
                        </text>
                    </g>
                    ${verticalConnector().trimIndent().trim()}
                </svg>
                """.trimIndent()
    }

    protected abstract fun rightToTopConnector(): String
    @Test
    fun `Should create a connector from right side to top side`() {
        orphan {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(200)
                    y(150)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".right(), "world".top(), type=type){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give` rightToTopConnector().trimIndent().trim()
    }

    protected abstract fun bottomToLeftConnector(): String
    @Test
    fun `Should create a connector from bottom side to left side`() {
        orphan {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
            }

            boxedText {
                id("world")
                geometry {
                    x(200)
                    y(150)
                    width(100)
                    height(50)
                }
            }
            connect("hello".bottom(), "world".left(), type=type){}
        } `should give` bottomToLeftConnector().trimIndent().trim()
    }

    protected abstract fun diagonalConnector(): String
    @Test
    fun `Should create a diagonal line`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(120)
                    y(150)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".bottom(), "world".top(), type=type){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give`
                """
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="200" height="180" viewBox="20 20 200 180">
                        <g id="hello">
                            <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                            <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                                Hello
                            </text>
                        </g>
                        <g id="world">
                            <rect x="120" y="150" width="100" height="50" fill="none" stroke="black" />
                            <text x="170" y="175" text-anchor="middle" dominant-baseline="middle">
                                world
                            </text>
                        </g>
                        ${diagonalConnector().trimIndent().trim()}
                    </svg>
                """.trimIndent()
    }

    abstract fun endingByAnArrow(): String
    @Test
    fun `Should create a diagonal line ending by an arrow`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(120)
                    y(150)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".bottom(), "world".top(), type=type, ending= Marker.Arrow){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="200" height="180" viewBox="20 20 200 180">
                    <g id="hello">
                        <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                            Hello
                        </text>
                    </g>
                    <g id="world">
                        <rect x="120" y="150" width="100" height="50" fill="none" stroke="black" />
                        <text x="170" y="175" text-anchor="middle" dominant-baseline="middle">
                            world
                        </text>
                    </g>
                    ${endingByAnArrow().trimIndent().trim()}
                </svg>
                """.trimIndent()
    }

    abstract fun startingByAnArrow(): String
    @Test
    fun `Should create a diagonal line starting with an arrow`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(120)
                    y(150)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".bottom(), "world".top(), type=type, starting= Marker.Arrow){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="200" height="180" viewBox="20 20 200 180">
                    <g id="hello">
                        <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                            Hello
                        </text>
                    </g>
                    <g id="world">
                        <rect x="120" y="150" width="100" height="50" fill="none" stroke="black" />
                        <text x="170" y="175" text-anchor="middle" dominant-baseline="middle">
                            world
                        </text>
                    </g>
                    ${startingByAnArrow().trimIndent().trim()}
                </svg>
                """.trimIndent()
    }

    abstract fun startingAndEndingByAnArrow(): String
    @Test
    fun `Should create a connector starting and ending by an arrow`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    x(180)
                    y(20)
                    width(100)
                    height(50)
                }
                text("world")
            }
            connect("hello".right(), "world".left(), type=type, ending= Marker.Arrow, starting= Marker.Arrow){
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="260" height="50" viewBox="20 20 260 50">
                    <g id="hello">
                        <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                            Hello
                        </text>
                    </g>
                    <g id="world">
                        <rect x="180" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="230" y="45" text-anchor="middle" dominant-baseline="middle">
                            world
                        </text>
                    </g>
                    ${startingAndEndingByAnArrow().trimIndent().trim()}
                </svg>
                """.trimIndent()
    }


    abstract fun fromBottomToTop(): String
    @Test
    fun `Should create connectors from bottom to top`() {
        svg {
            boxedText {
                id("explorer")
                geometry {
                    x(77)
                    y(2)
                    width(90)
                    height(30)
                    text("Explorer")
                }
            }
            boxedText {
                id("rover")
                geometry {
                    x(2)
                    y(92)
                    width(90)
                    height(30)
                    text("Rover")
                }
            }
            connect("explorer".bottom(), "rover".top(), type=type) {
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="165" height="120" viewBox="2 2 165 120">
                    <g id="explorer">
                        <rect x="77" y="2" width="90" height="30" fill="none" stroke="black" />
                    <text x="122" y="17" text-anchor="middle" dominant-baseline="middle">
                            Explorer
                        </text>
                    </g>
                    <g id="rover">
                        <rect x="2" y="92" width="90" height="30" fill="none" stroke="black" />
                        <text x="47" y="107" text-anchor="middle" dominant-baseline="middle">
                            Rover
                        </text>
                    </g>
                    ${fromBottomToTop().trim().trimIndent()}
                </svg>
                """.trimIndent()
    }

    abstract fun withLabel(): String
    @Test
    fun `Should create an connector with a label`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    y(30)
                    placeAt(130.rightOf("hello"))
                    resizeLike("hello")
                }
                text("world")
            }
            connect("hello".right(), "world".left(), type=type) {
                style {
                    "stroke"("black")
                    "stroke-width"("2")
                }

                text("foobar")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="330" height="60" viewBox="20 20 330 60">
                    <g id="hello">
                        <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                            Hello
                        </text>
                    </g>
                    <g id="world">
                        <rect x="250" y="30" width="100" height="50" fill="none" stroke="black" />
                        <text x="300" y="55" text-anchor="middle" dominant-baseline="middle">
                            world
                        </text>
                    </g>
                    ${withLabel().trimIndent().trim()}
                </svg>
                """.trimIndent()
    }


    abstract fun coloredConnector(): String
    @Test
    fun `Should change the color of the whole connector`() {
        svg {
            boxedText {
                id("hello")
                geometry {
                    x(20)
                    y(20)
                    width(100)
                    height(50)
                }
                text("Hello")
            }

            boxedText {
                id("world")
                geometry {
                    y(50)
                    placeAt(130.rightOf("hello"))
                    resizeLike("hello")
                }
                text("world")
            }
            connect("hello".right(), "world".left(), type=type, starting = Marker.Arrow, ending = Marker.Arrow){
                text("foobar")
                color("red")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="330" height="80" viewBox="20 20 330 80">
                    <g id="hello">
                        <rect x="20" y="20" width="100" height="50" fill="none" stroke="black" />
                        <text x="70" y="45" text-anchor="middle" dominant-baseline="middle">
                            Hello
                        </text>
                    </g>
                    <g id="world">
                        <rect x="250" y="50" width="100" height="50" fill="none" stroke="black" />
                        <text x="300" y="75" text-anchor="middle" dominant-baseline="middle">
                            world
                        </text>
                    </g>
                    ${coloredConnector().trim().trimEnd()}
                </svg>
                """.trimIndent()
    }

    abstract fun connectorStyle(): String
    @Test
    fun `should change style of a connector`() {
        val source = Point(20.0, 40.0)
        val destination = Point(200.0, 100.0)
        orphan {
            connect(
                    Anchor(
                            source,
                            Anchor.Kind.Right
                    ),
                    Anchor(
                            destination,
                            Anchor.Kind.Left
                    ),
                    type = type,
                    ending = Marker.Arrow,
                    endingSize = 5.0
            ) {
                color("red")
                thickness(3)
            }
        } `should give` connectorStyle()
    }


}
