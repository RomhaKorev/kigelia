package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.geometry.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PathPainterTest {
    @Test
    fun `should move to the given position`() {
        orphan {
            path {
                move(10, 20)
                move(Point(50.0, 50.0))
            }
        } `should give` """<path d="M10 20 M50 50" />"""

        orphan {
            path {
                move(DPoint(50.0, 50.0))
            }
        } `should give` """<path d="m50 50" />"""
    }

    @Test
    fun `should add line to the given position`() {
        orphan {
            path {
                lineTo(10, 20)
                lineTo(Point(50.0, 50.0))
            }
        } `should give` """<path d="L10 20 L50 50" />"""
        orphan {
            path {
                lineTo(DPoint(50.0, 50.0))
            }
        } `should give` """<path d="l50 50" />"""
    }

    @Test
    fun `should add segments`() {
        orphan {
            path {
                lineTo(DPoint(10, 20), DPoint(0, 20), DPoint(10, 0))
            }
        } `should give` """<path d="l10 20 l0 20 l10 0" />"""
    }

    @Test
    fun `should give its last position`() {
        orphan {
            assertThat(path {
                lineTo(Point(10, 20))
                lineTo(DPoint(0, 40))
            }.position).isEqualTo(Point(10, 60))
        }
    }

    @Test
    fun `should add curve to the given point`() {
        orphan {
            path {
                move(10, 10)
                curveTo(Point(20.0, 20.0), Point(40.0, 20.0), Point(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 C20 20, 40 20, 50 10" />"""

        orphan {
            path {
                move(10, 10)
                curveTo(DPoint(20.0, 20.0), DPoint(40.0, 20.0), DPoint(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 c20 20, 40 20, 50 10" />"""
    }

    @Test
    fun `should add shorthand curve to the given point`() {
        orphan {
            path {
                move(10, 10)
                shorthandCurveTo(Point(20.0, 20.0), Point(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 S20 20, 50 10" />"""

        orphan {
            path {
                move(10, 10)
                shorthandCurveTo(DPoint(20.0, 20.0), DPoint(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 s20 20, 50 10" />"""
    }

    @Test
    fun `should add quadratic bezier curve to the given point`() {
        orphan {
            path {
                move(10, 10)
                quadraticBezierCurveTo(Point(20.0, 20.0), Point(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 Q20 20, 50 10" />"""

        orphan {
            path {
                move(10, 10)
                quadraticBezierCurveTo(DPoint(20.0, 20.0), DPoint(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 q20 20, 50 10" />"""
    }

    @Test
    fun `should add shorthand quadratic bezier curve to the given point`() {
        orphan {
            path {
                move(10, 10)
                shorthandQuadraticBezierCurveTo( Point(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 T50 10" />"""

        orphan {
            path {
                move(10, 10)
                shorthandQuadraticBezierCurveTo(DPoint(50.0, 10.0))
            }
        } `should give` """<path d="M10 10 t50 10" />"""
    }

    @Test
    fun `should add an arc from angles`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), 90.0, 90.0)
            }
        } `should give` """<path d="M50 0 A 50 50 0 0 0 0 50" />"""
    }

    @Test
    fun `should create an arc in two parts when end is more than 180 deg`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), 0.0, 270.0)
            }
        } `should give` """<path d="M100 50 A 50 50 0 1 0 50 100" />"""
    }

    @Test
    fun `should create an arc in two parts when start is less than 0 deg`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), -90.0, 180.0)
            }
        } `should give` """<path d="M50 100 A 50 50 0 0 0 50 0" />"""
    }

    @Test
    fun `should create an arc starting before 0 and ending after 180`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), -90.0, 360.0)
            }
        } `should give` """<path d="M50 100 A 50 50 0 1 0 50 100" />"""
    }

    @Test
    fun `should create arc from current position`() {
        orphan {
            path {
                move(Point(50, 100))
                arcTo(Size(100, 100), -90.0, 360.0)
            }
        } `should give` """<path d="M50 100 A 50 50 0 1 0 50 100" />"""
    }

    @Test
    fun `should create an arc in clockwise direction`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), 0.0, 180.0, direction = Direction.Clockwise)
            }
        } `should give` """<path d="M100 50 A 50 50 0 0 1 0 50" />"""
    }

    @Test
    fun `should create an arc in clockwise direction when end is more than 180`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), 0.0, 270.0, direction = Direction.Clockwise)
            }
        } `should give` """<path d="M100 50 A 50 50 0 1 1 50 0" />"""
    }

    @Test
    fun `should create an arc in clockwise direction when end is more than 0`() {
        orphan {
            path {
                arcMoveTo(Ellipse(Point(50, 50), Size(100, 100)), 90.0, 180.0, direction = Direction.Clockwise)
            }
        } `should give` """<path d="M50 0 A 50 50 0 0 1 50 100" />"""
    }

    @Test
    fun `should create an relative arc`() {
        orphan {
            path {
                arcTo(Size(100, 100), 0.0, 0, 0, DPoint(20, 20))
            }
        } `should give` """<path d="a 100 100 0 0 0 20 20" />"""
    }

    @Test
    fun `should close`() {
        orphan {
            path {
                arcTo(Size(100, 100), 0.0, 0, 0, DPoint(20, 20))
                close()
            }
        } `should give` """<path d="a 100 100 0 0 0 20 20 Z" />"""
    }

}
