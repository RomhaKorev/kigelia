package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.elements.line
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.geometry.Angle
import com.gitlab.kigelia.core.geometry.Line
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LineTest {

    @Test
    fun `should create a line`() {

        val l = Line(Point(310, 350), Point(360,350)).withAngle(Angle.degrees(220))
        println(l)

        println(orphan {
            l.draw()
        }.render())

        orphan {
            line {
                p1(Point(10, 50))
                p2(Point(25, 60))
            }
        }`should give` """<line x1="10" y1="50" x2="25" y2="60" />"""
    }

    @Test
    fun `should define the geometry of a line`() {
        orphan {
            line {
                x1(40)
                y1(50)
                x2(25)
                y2(60)
            }
        }.geometry() `should give` Geometry(25.0, 50.0, 40.0 - 25.0, 60.0 - 50.0)
    }

    @Test
    fun `should define the geometry of a line through attributes`() {
        orphan {
            line {
                "x1"(40)
                "y1"(50)
                "x2"(25)
                "y2"(60)
            }
        }.geometry() `should give` Geometry(25.0, 50.0, 40.0 - 25.0, 60.0 - 50.0)
    }

    @Test
    fun `should warn when geometry is not well defined`() {
        orphan {
            line {
                x1(40)
                x2(25)
                y2(60)
            }
        }.geometry() `should give` Geometry()

        orphan {
            line {
                y1(40)
                x2(25)
                y2(60)
            }
        }.geometry() `should give` Geometry()

        orphan {
            line {
                x1(40)
                y1(25)
                y2(60)
            }
        }.geometry() `should give` Geometry()

        orphan {
            line {
                x1(40)
                y1(25)
                x2(60)
            }
        }.geometry() `should give` Geometry()
    }

    @Test
    fun `should define p1 and p2`() {
        val line = orphan {
            line {
                x1(40)
                y1(50)
                x2(25)
                y2(60)
            }
        }

        assertThat(line.p1).isEqualTo(Point(40, 50))
        assertThat(line.p2).isEqualTo(Point(25, 60))
    }

}
