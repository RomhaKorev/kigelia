package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.helpers.boxedText
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.svg
import org.junit.jupiter.api.Test

class SvgTest {

    @Test
    fun `Should generate with style`() {
        svg {
            style {
                "width"("75.5%")
                "height"("500px")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 75.5%; height: 500px" />
                """.trimIndent()
    }

    @Test
    fun `Should generate the view box`() {
        svg {
            viewbox {
                minx(-200.0)
                miny(-150.0)
                width(400.0)
                height(350.0)
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-200 -150 400 350" />
                """.trimIndent()
    }

    @Test
    fun `Should change unit for style property`() {
        svg {
            style {
                "width"("100%")
                "height"("500px")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 100%; height: 500px" />
                """.trimIndent()
    }

    @Test
    fun `Should generate with custom css parameters`() {
        svg {
            style {
                "align-self"("center")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="align-self: center" />
                """.trimIndent()
    }

    @Test
    fun `Should generate with custom css parameters and parameters`() {
        svg {
            style {
                "width"("500px")
                "height"("500px")
                "align-self"("center")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 500px; height: 500px; align-self: center" />
                """.trimIndent()
    }

    @Test
    fun `Should generate with custom parameters and parameters`() {
        svg {
            style {
                "width"("500px")
                "height"("500px")
            }
            "transform"("scale(2 2)")
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 500px; height: 500px" transform="scale(2 2)" />
                """.trimIndent()
    }

    @Test
    fun `Should generate with content`() {
        svg {
            style {
                "width"("500px")
                "height"("500px")
                "align-self"("center")
            }

            g {
                "polygon" {
                    "points"("1 2 3")
                }
            }

        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 500px; height: 500px; align-self: center">
                    <g>
                        <polygon points="1 2 3" />
                    </g>
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should add property only once`() {
        svg {
            style {
                "width"("500px")
                "width"("750px")
            }

            g {
                "polygon" {
                    "points"("1 2 3")
                }
            }

        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 750px">
                    <g>
                        <polygon points="1 2 3" />
                    </g>
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should add viewbox when using svg helper`() {
        svg {
            boxedText {
                id("explorer")
                box {
                    "stroke-width"("2")
                }
                geometry {
                    x(77)
                    y(2)
                    width(90)
                    height(30)
                    text("Explorer")
                }
            }
            boxedText {
                id("rover")
                box {
                    "stroke-width"("2")
                }
                geometry {
                    x(4)
                    y(92)
                    width(90)
                    height(30)
                    text("Rover")
                }
            }
            boxedText {
                id("drone")
                box {
                    "stroke-width"("2")
                }
                geometry {
                    x(154)
                    y(92)
                    width(90)
                    height(30)
                    text("Drone")
                }
            }
            g {
                "stroke-width"("none")
                "stroke"("black")
                connect("explorer".bottom(), "drone".top(), type = ConnectorType.Squared) {
                }
                connect("explorer".bottom(), "rover".top(), type = ConnectorType.Squared) {
                }
            }
        } `should give`
                """
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="240" height="120" viewBox="4 2 240 120">
                        <g id="explorer">
                            <rect x="77" y="2" width="90" height="30" fill="none" stroke="black" stroke-width="2" />
                            <text x="122" y="17" text-anchor="middle" dominant-baseline="middle">
                                Explorer
                            </text>
                        </g>
                        <g id="rover">
                            <rect x="4" y="92" width="90" height="30" fill="none" stroke="black" stroke-width="2" />
                            <text x="49" y="107" text-anchor="middle" dominant-baseline="middle">
                                Rover
                            </text>
                        </g>
                        <g id="drone">
                            <rect x="154" y="92" width="90" height="30" fill="none" stroke="black" stroke-width="2" />
                            <text x="199" y="107" text-anchor="middle" dominant-baseline="middle">
                                Drone
                            </text>
                        </g>
                        <g stroke-width="none" stroke="black">
                            <g>
                                <line x1="122" y1="32" x2="122" y2="62" />
                                <line stroke-linecap="round" x1="122" y1="62" x2="199" y2="62" />
                                <line stroke-linecap="round" x1="199" y1="62" x2="199" y2="92" />
                            </g>
                            <g>
                                <line x1="122" y1="32" x2="122" y2="62" />
                                <line stroke-linecap="round" x1="122" y1="62" x2="49" y2="62" />
                                <line stroke-linecap="round" x1="49" y1="62" x2="49" y2="92" />
                            </g>
                        </g>
                    </svg>
                """.trimIndent()
    }


    @Test
    fun `Should define viewbox according to geometry without transparent elements`() {
        svg {
            "defs" {
                transparent()
                g {
                    id("seat")
                    rect {
                        geometry {
                            x(2)
                            y(20)
                            width(36)
                            height(18)
                            "stroke"("none")
                            "fill"("black")
                        }
                    }
                    "path" {
                        "stroke"("red")
                        "stroke-width"("2")
                        "fill"("none")
                        "d"("M30,17 a1,1 0 0,0 -20,0")
                    }
                }
            }
            rect {
                geometry {
                    x(100)
                    y(100)
                    width(50)
                    height(50)
                }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50" viewBox="100 100 50 50">
                <defs>
                    <g id="seat">
                        <rect x="2" y="20" width="36" height="18" stroke="none" fill="black" />
                        <path stroke="red" stroke-width="2" fill="none" d="M30,17 a1,1 0 0,0 -20,0" />
                    </g>
                </defs>
                <rect x="100" y="100" width="50" height="50" />
            </svg>
        """.trimIndent()
    }

    @Test
    fun `Should override size attributes with fixed values`() {
        svg {
            "width"("100%")
            "height"("100%")
            rect {
                geometry {
                    x(10)
                    y(20)
                    width(350)
                    height(350)
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="10 20 350 350">
                    <rect x="10" y="20" width="350" height="350" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should add metadata`() {
        svg {
            "width"("100%")
            "height"("100%")
            metadata {
                "info" {
                    -"this is an info"
                }
            }
            rect {
                geometry {
                    x(10)
                    y(20)
                    width(350)
                    height(350)
                }

            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="10 20 350 350">
                    <metadata>
                        <info>
                            this is an info
                        </info>
                    </metadata>
                    <rect x="10" y="20" width="350" height="350" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should grow the viewbox by`() {
        svg {
            rect {
                x(10)
                y(30)
                width(100)
                height(50)
            }
            growBy {
                horizontal(5)
                vertical(10)
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="110" height="70" viewBox="5 20 110 70">
                    <rect x="10" y="30" width="100" height="50" />
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should define the viewbox and the size`() {
        svg {
            viewbox(-10, -20, 400, 500)
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="400" height="500" viewBox="-10 -20 400 500" />
                """.trimIndent()
    }
}
