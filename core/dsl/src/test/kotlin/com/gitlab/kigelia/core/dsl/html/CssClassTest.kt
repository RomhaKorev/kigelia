package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.html.elements.div
import com.gitlab.kigelia.core.dsl.svg.`should contain`
import org.junit.jupiter.api.Test

class CssClassTest {
    @Test
    fun `should add css class`() {
        html {
            body {
                div {
                    cssClass("class-1", "class-2")
                    cssClass("class-3")
                }
            }
        } `should contain` """<div class="class-1 class-2 class-3" />"""
    }

    @Test
    fun `should remove css class`() {
        html {
            body {
                div {
                    cssClass("class-1", "class-2")
                    cssClass("class-3")
                    removeCssClass("class-2", "class-4")
                }
            }
        } `should contain` """<div class="class-1 class-3" />"""
    }
}