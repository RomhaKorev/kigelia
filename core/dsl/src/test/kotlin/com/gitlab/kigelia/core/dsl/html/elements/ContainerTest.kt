package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.core.dsl.html.orphan
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource




class ContainerTest {
    @ParameterizedTest
    @MethodSource("container")
    fun `should build an empty container`(name: String, builder: HtmlElement.(HtmlElement.() -> Unit) -> HtmlElement) {
        val renderer = orphan("") {
            builder {}
        }
        assertThat(renderer.render()).isEqualTo(
            """
                <$name />
            """.trimIndent()
        )
    }

    @ParameterizedTest
    @MethodSource("container")
    fun `should build a container with children`(name: String, builder: HtmlElement.(HtmlElement.() -> Unit) -> HtmlElement) {
        val renderer = orphan("") {
            builder {
                p {
                    - "text 1"
                }
                p {
                    - "text 2"
                }
            }
        }
        assertThat(renderer.render()).isEqualTo(
            """
                <$name>
                    <p>text 1</p>
                    <p>text 2</p>
                </$name>
            """.trimIndent()
        )
    }

    companion object {
        @JvmStatic
        fun container(): List<Arguments> {
            return listOf<Pair<String, HtmlElement.(HtmlElement.() -> Unit) -> HtmlElement>>(
                "div" to {b -> div {b()}; this },
                "span" to {b -> span {b()}; this },
                //"p" to {b -> p {b()}; this }
            ).map {
                (name, builder)-> Arguments.of(name, builder)
            }
        }
    }
}