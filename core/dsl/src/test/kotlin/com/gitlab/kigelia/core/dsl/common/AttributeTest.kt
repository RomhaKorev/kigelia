package com.gitlab.kigelia.core.dsl.common

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class AttributeTest {
    @Test
    fun `should return an attribute`() {
        val tag = Tag("foo")

        val createAttribute: Tag.() -> Unit = {
            "number"(13)
        }

        tag.createAttribute()

        Assertions.assertEquals("""number="13"""", tag.attribute("number").render())
    }

    @Test
    fun `should change an attribute value`() {
        val tag = Tag("foo")

        val createAttribute: Tag.() -> Unit = {
            "number"(13)
        }

        tag.createAttribute()
        tag.attribute("number", "999")

        Assertions.assertEquals("""number="999"""", tag.attribute("number").render())
    }

    @Test
    fun `should throw an error if an attribute does not exist`() {
        val tag = Tag("foo")

        Assertions.assertThrows(IllegalArgumentException::class.java) {tag.attribute("number")}
    }

    @Test
    fun `should put an attribute only if not present`() {
        val tag = Tag("foo")
        tag.attribute("number", 13)

        val createAttribute: Tag.() -> Unit = {
            "number".ifNotPresent(999)
            "string".ifNotPresent("abc")
        }

        tag.createAttribute()

        Assertions.assertEquals("""number="13"""", tag.attribute("number").render())
        Assertions.assertEquals("""string="abc"""", tag.attribute("string").render())
    }

    @Test
    fun `should remove an attribute`() {
        val tag = Tag("foo")
        tag.attribute("number", 13)
        tag.removeAttribute("number")
        assertThat(tag.hasAttribute("number")).isFalse
    }
}