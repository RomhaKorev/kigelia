package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Translation
import com.gitlab.kigelia.core.dsl.svg.*
import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.geometry.Padding
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test


class GTest {

    @Test
    fun `Should bound the content`() {
        svg {
            g {
                rect {
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
                rect {
                    geometry {
                        x(60)
                        y(20)
                        width(50)
                        height(50)
                    }
                }
            } `should be at` Geometry(10.0, 10.0, 100.0, 60.0)
        }
    }

    @Test
    fun `Should translate the content`() {
        svg {
            g {
                transform {
                    translate(Point(30.0, 40.0))
                }
                rect {
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
                rect {
                    geometry {
                        x(60)
                        y(20)
                        width(50)
                        height(50)
                    }
                }
            } `should give`
                    """
                        <g transform="translate(30, 40)">
                            <rect x="10" y="10" width="50" height="50" />
                            <rect x="60" y="20" width="50" height="50" />
                        </g>
                    """.trimIndent()
        }
    }

    @Test
    fun `Should place the content below another`() {
        svg {
            g {
                id("group-1")
                rect {
                    id("r1")
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
            }
            g {
                transform {
                    translate(10.below("group-1"))
                }
                rect {
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="120" viewBox="10 10 50 120">
                    <g id="group-1">
                        <rect x="10" y="10" width="50" height="50" id="r1" />
                    </g>
                    <g transform="translate(0, 70)">
                        <rect x="10" y="10" width="50" height="50" />
                    </g>
                </svg>
                """.trimIndent()
    }

    @Test
    fun `Should add stroke width parameter`() {
        svg {
            g {
                "stroke-width"("2px")
            }
        } `should contain`
        """<g stroke-width="2px" />""".trimIndent()
    }

    @Test
    fun `Should add stroke color parameter`() {
        svg {
        g {
            "stroke"("red")
        }
        } `should contain`
        """<g stroke="red" />"""
    }

    @Test
    fun `Should add transformation`() {
        svg {
        g {
            "transform"("translate(1 2)")
        }
        } `should contain`
                """<g transform="translate(1 2)" />"""
    }


    @Test
    fun `Should add rotation`() {
        svg {
            g {
                transform {
                    rotate(45.0)
                }
            }
        } `should contain`
                """<g transform="rotate(45)" />"""
    }

    @Test
    fun `Should ignore empty transformation`() {
        svg {
            g {
                transform {
                    translate(Translation(Point(10.0, 10.0), Point(10.0, 10.0)))
                }
            }
        } `should contain`
                """<g />"""
    }

    @Test
    fun `Should add fill color`() {
        svg{
            g {
            "fill"("red")
        }} `should contain`
                """<g fill="red" />"""
    }

    @Test
    fun `Should consider the size without adding properties`() {
        svg{
            g {
                geometry {
                    width(150)
                    height(300)
                }
                "fill"("red")
            }} `should contain`
                """<g fill="red" />"""
    }

    @Test
    fun `should paint a background`() {
        svg {
            g {
                background {
                    color("red")
                }

                circle {
                    cx(20)
                    cy(20)
                    r(10)
                    "stroke"("blue")
                }

                circle {
                    cx(200)
                    cy(200)
                    r(20)
                    "stroke"("blue")
                }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="210" height="210" viewBox="10 10 210 210">
                <rect x="10" y="10" width="210" height="210" fill="red" />
                <g>
                    <circle cx="20" cy="20" r="10" stroke="blue" />
                    <circle cx="200" cy="200" r="20" stroke="blue" />
                </g>
            </svg>
        """.trimIndent()
    }


    @Test
    fun `should paint a background around each group`() {
        svg {
            g {
                background {
                    color("red")
                }

                circle {cx(20); cy(20); r(10); "stroke"("blue") }
                circle { cx(40); cy(40); r(10); "stroke"("blue") }
            }

            g {
                background {
                    color("green")
                }

                circle {cx(20); cy(120); r(10); "stroke"("blue") }
                circle { cx(40); cy(150); r(10); "stroke"("blue") }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="150" viewBox="10 10 40 150">
                    <rect x="10" y="10" width="40" height="40" fill="red" />
                    <rect x="10" y="110" width="40" height="50" fill="green" />
                    <g>
                        <circle cx="20" cy="20" r="10" stroke="blue" />
                        <circle cx="40" cy="40" r="10" stroke="blue" />
                    </g>
                    <g>
                        <circle cx="20" cy="120" r="10" stroke="blue" />
                        <circle cx="40" cy="150" r="10" stroke="blue" />
                    </g>
                </svg>
        """.trimIndent()
    }

    @Test
    fun `should paint a background with padding`() {
        svg {
            g {
                background {
                    color("red")
                    padding(Padding(5.0, 10.0))
                }

                circle {cx(20); cy(20); r(10); "stroke"("blue") }
                circle { cx(40); cy(40); r(10); "stroke"("blue") }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="60" viewBox="5 0 50 60">
                <rect x="5" y="0" width="50" height="60" fill="red" />
                <g>
                    <circle cx="20" cy="20" r="10" stroke="blue" />
                    <circle cx="40" cy="40" r="10" stroke="blue" />
                </g>
            </svg>
        """.trimIndent()
    }
}

infix fun SvgElement.`should be at`(expected: Geometry): SvgElement {
    val result = this.geometry()
    Assertions.assertEquals(expected, result, "Expecting ${result.print()} to be equals to ${expected.print()}")
    return this
}

fun Geometry.print(): String {
    return "(x=${xOrNull()}, y=${yOrNull()}, width=${widthOrNull()}, height=${heightOrNull()}, valid=${valid()})"
}
