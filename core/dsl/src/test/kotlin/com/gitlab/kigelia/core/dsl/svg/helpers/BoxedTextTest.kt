package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.dsl.svg.`should give`
import org.junit.jupiter.api.Test

class BoxedTextTest {

    @Test
    fun `Should create a rectangle and a text`() {
        boxedText {
            geometry {
                x(12)
                y(23)
                width(120)
                height(90)
            }
            text("Hello")
        } `should give`
                """
                <g>
                    <rect x="12" y="23" width="120" height="90" fill="none" stroke="black" />
                    <text x="72" y="68" text-anchor="middle" dominant-baseline="middle">
                        Hello
                    </text>
                </g>
                """.trimIndent()
    }
}
