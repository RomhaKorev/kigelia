package com.gitlab.kigelia.core.dsl.html.bulma

import com.gitlab.kigelia.core.dsl.common.Renderer
import com.gitlab.kigelia.core.dsl.html.bulma.columns.Column
import com.gitlab.kigelia.core.dsl.html.elements.p
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.core.dsl.svg.`should contain`
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class ColumnsTest {
    @Test
    fun `should create a columns container`() {
        `with bulma` {
            columns {

            }
        } `should contain` """<div class="columns" />"""
    }

    @Test
    fun `should create columns`() {
        `with bulma` {
            columns {
                column {
                    p {
                        - "Column 1"
                    }
                }
                column {
                    p {
                        - "Column 2"
                    }
                }
            }
        } `should contain` """
            |<div class="columns">
            |   <div class="column">
            |       <p>Column 1</p>
            |   </div>
            |   <div class="column">
            |       <p>Column 2</p>
            |   </div>
            |</div>
        """.trimMargin()
    }

    @ParameterizedTest
    @MethodSource("sizes")
    fun `should defined column size`(name: String, f: Column.() -> Unit) {
        `with bulma` {
            columns {
                column {
                    f()
                    p {
                        - "Column 1"
                    }
                }
                column {
                    p {
                        - "Column 2"
                    }
                }
            }
        } `should contain` """
            |<div class="columns">
            |   <div class="column $name">
            |       <p>Column 1</p>
            |   </div>
            |   <div class="column">
            |       <p>Column 2</p>
            |   </div>
            |</div>
        """.trimMargin()
    }


    companion object {
        @JvmStatic
        fun sizes(): Stream<Arguments> {

            return listOf<Pair<String, Column.() -> Unit>>(
                "is-three-quarters" to Column::`is three quarters`,
                "is-two-thirds" to Column::`is two thirds`,
                "is-half" to Column::`is half`,
                "is-one-third" to Column::`is one third`,
                "is-one-quarter" to Column::`is one quarter`,
                "is-full" to Column::`is full`,
                "is-four-fifths" to Column::`is four fifths`,
                "is-three-fifths" to Column::`is three fifths`,
                "is-two-fifths" to Column::`is two fifths`,
                "is-one-fifth" to Column::`is one fifth`
            ).map {
                (name, f) -> Arguments.of(name, f)
            }.stream()
        }
    }

    /*



The other columns will fill up the remaining space automatically.

You can now use the following multiples of 20% as well:




     */

    private fun `with bulma`(f: Bulma.() -> Unit): Renderer {
        return html {
            body {
                bulma {
                    f()
                }
            }
        }
    }
}

