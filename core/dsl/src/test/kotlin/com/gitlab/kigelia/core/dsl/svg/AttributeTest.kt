package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.elements.rect
import io.github.classgraph.ClassGraph
import org.junit.jupiter.api.Test
import java.io.File
import java.net.URI
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.kotlinFunction


class AttributeTest {
    @Test
    fun `Should reuse an attribute value`() {
        "g" {
            "width"("200px")
            "height"(attribute("width").value())
        } `should give`
                """<g width="200px" height="200px" />""".trimIndent()
    }


    @Test
    fun `Should reuse an attribute value of a sibling`() {
        "g" {
            rect {
                id("source")
                "width"("200px")
                "height"("90px")
            }
            rect {
                "width"("source".valueOf("width"))
                "height"("source".valueOf("height"))
            }
        } `should give`
                """
                <g>
                    <rect id="source" width="200px" height="90px" />
                    <rect width="200px" height="90px" />
                </g>""".trimIndent()
    }

    @Test
    fun `Should raise an error if an attribute does not exist`() {
        `should raise an error` {
            "g" {
                "height"(attribute("width").value())
            }
        }
    }

    @Test
    fun `Should include a viewbox if one of the attribute is valid`() {
        svg {
            viewbox {
                height(200)
            }
        } `should give`
                """<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 0 200" />""".trimIndent()
    }

    @Test
    fun `Should include geometry attribute`() {
        svg {
            geometry {
                x(12)
                y(45)
                width(150)
                height(200)
            }
        } `should give`
                """<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="12" y="45" width="150" height="200" />""".trimIndent()
    }
}
