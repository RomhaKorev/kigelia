package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.dsl.svg.elements.circle
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.core.dsl.svg.svg
import org.junit.jupiter.api.Test

class CurvyConnectorTest: ConnectorTest(ConnectorType.Curvy) {

    override fun horizontalConnector(): String = """
                <path d="M120 45 C140 45, 130 45, 150 45" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
                """

    override fun verticalConnector(): String = """
                <path d="M70 70 C70 140, 70 60, 70 150" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
                """

    override fun rightToTopConnector(): String  = """
        <path d="M120 45 C240 45, 250 35, 250 150" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
    """.trimIndent()

    override fun bottomToLeftConnector(): String  = """
        <path d="M70 70 C70 165, 80 175, 200 175" fill="none" stroke-width="2" />
    """.trimIndent()


    override fun diagonalConnector(): String = """
                <path d="M70 70 C70 140, 170 60, 170 150" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
                """

    override fun endingByAnArrow(): String = """
                <defs>
                    <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                        <polygon points="0 0, 10 3.5, 0 7" />
                    </marker>
                </defs>
                <path d="M70 70 C70 140, 170 60, 170 130" fill="none" stroke-width="2" marker-end="url(#endarrow-0)" style="stroke: black; stroke-width: 2" />
                """

    override fun startingByAnArrow(): String = """
                <defs>
                    <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                        <polygon points="10 0, 10 7, 0 3.5" />
                    </marker>
                </defs>
                <path d="M70 90 C70 140, 170 60, 170 150" fill="none" stroke-width="2" marker-start="url(#startarrow-0)" style="stroke: black; stroke-width: 2" />
                """

    override fun startingAndEndingByAnArrow() = """
                <defs>
                    <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                        <polygon points="0 0, 10 3.5, 0 7" />
                    </marker>
                    <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                        <polygon points="10 0, 10 7, 0 3.5" />
                    </marker>
                </defs>
                <path d="M140 45 C170 45, 130 45, 160 45" fill="none" stroke-width="2" marker-end="url(#endarrow-0)" marker-start="url(#startarrow-0)" style="stroke: black; stroke-width: 2" />
                """


    override fun fromBottomToTop(): String = """
                <path d="M122 32 C122 82, 47 22, 47 92" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
            """

    override fun withLabel(): String = """
    <path d="M120 45 C240 45, 130 55, 250 55" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
    <text x="185" y="50" text-anchor="middle" dominant-baseline="middle">
        foobar
    </text>
    """

    override fun coloredConnector(): String = """
        <defs>
            <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
                <polygon points="0 0, 10 3.5, 0 7" />
            </marker>
            <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto" stroke="red" fill="red">
                <polygon points="10 0, 10 7, 0 3.5" />
            </marker>
        </defs>
        <path d="M140 45 C240 45, 130 75, 230 75" fill="none" stroke-width="2" marker-end="url(#endarrow-0)" marker-start="url(#startarrow-0)" stroke="red" />
        <text x="185" y="60" text-anchor="middle" dominant-baseline="middle" fill="red">
            foobar
        </text>
        """

    override fun connectorStyle(): String = """
    <defs>
        <marker id="endarrow-0" markerWidth="5" markerHeight="3.5" refX="0" refY="1.75" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
            <polygon points="0 0, 5 1.75, 0 3.5" />
        </marker>
    </defs>
    <path d="M20 40 C190 40, 30 100, 180 100" fill="none" stroke-width="3" marker-end="url(#endarrow-0)" stroke="red" />
    """.trimIndent()


    @Test
    fun `should draw a curvy connector from left bottom to right top`() {
        svg {
            //<circle id="origin-master-4" cx="145" cy="85" r="5" />
            circle {
                id("c1")
                cx(145)
                cy(85)
                r(5)
            }
            //<circle id="bob-feature-1-7" cx="205" cy="25" r="5" />
            circle {
                id("c2")
                cx(205)
                cy(25)
                r(5)
            }

            connect("c1".right(), "c2".left(), type = ConnectorType.Curvy) {
                color("black")
            }
        } `should give`  """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70" height="70" viewBox="140 20 70 70">
                <circle id="c1" cx="145" cy="85" r="5" />
                <circle id="c2" cx="205" cy="25" r="5" />
                <path d="M150 85 C190 85, 160 25, 200 25" fill="none" stroke-width="2" stroke="black" />
            </svg>
        """.trimIndent()
    }
}

