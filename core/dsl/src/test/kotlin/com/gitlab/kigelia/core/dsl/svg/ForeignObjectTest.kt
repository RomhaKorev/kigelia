package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.invoke
import com.gitlab.kigelia.core.dsl.svg.`should give`
import org.junit.jupiter.api.Test


class ForeignObjectTest {

    @Test
    fun `Should add position`() {
        "foreignObject" {
            geometry {
                x(20.0)
                y(50.0)
            }
        } `should give`
                """<foreignObject x="20" y="50" />"""
    }

    @Test
    fun `Should add size`() {
        "foreignObject" {
            geometry {
                width(200.0)
                height(500.5)
            }
        } `should give`
                """<foreignObject width="200" height="500.5" />"""
    }
}


