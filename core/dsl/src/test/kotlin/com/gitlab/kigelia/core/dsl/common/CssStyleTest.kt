package com.gitlab.kigelia.core.dsl.common

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CssStyleTest {

    @Test
    fun `should render`() {
        val css = CssStyle()

        val f: CssStyle.() -> Unit = {
            "display"("flex")
            "background-color"("red")
            "min-width"("50%")
        }

        css.f()

        assertThat(css.render()).isEqualTo("""style="display: flex; background-color: red; min-width: 50%"""")
    }

    @Test
    fun `should be valid only if attribute set`() {
        val css = CssStyle()

        val f: CssStyle.() -> Unit = {
            "display"("flex")
            "background-color"("red")
            "min-width"("50%")
        }

        assertThat(css.valid()).isFalse

        css.f()

        assertThat(css.valid()).isTrue
    }

    @Test
    fun `should update attribute`() {
        val css = CssStyle()

        val f: CssStyle.(v: String) -> Unit = { v ->
            "display"(v)
        }
        css.f("flex")
        css.f("none")

        assertThat(css.render()).isEqualTo("""style="display: none"""")
    }
}