package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.HtmlFrame
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test



class FrameTest {
    @Test
    fun `a frame is visible when it has child`() {
        val frame = HtmlFrame("div")
        frame.add(HtmlElement("div"))
        assertThat(frame.visible()).isTrue
    }

    @Test
    fun `a frame is invisble when it is empty`() {
        val frame = HtmlFrame("div")
        assertThat(frame.visible()).isFalse
    }
}