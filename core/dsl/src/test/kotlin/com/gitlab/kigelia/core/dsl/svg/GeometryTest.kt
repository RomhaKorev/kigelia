package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Offset
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.RectGeometry
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GeometryTest {

    @Test
    fun `Should retrieve the geometry of a sibling`() {
        svg {
            rect {
                id("not source")
            }
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }
            rect {
                id("not source either")
            }

            Assertions.assertEquals(Geometry(120.0, 240.0, 60.0, 30.0, false), "source".geometry())
        }
    }

    @Test
    fun `Should apply the same size of a sibling`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    x(0)
                    y(0)
                    resizeLike("source")
                }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="180" height="270" viewBox="0 0 180 270">
                <rect x="120" y="240" width="60" height="30" id="source" />
                <rect x="0" y="0" width="60" height="30" />
            </svg>
        """.trimIndent()
    }

    @Test
    fun `Should ignore the geometry of a transparent element`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                transparent()
                geometry {
                    x(200)
                    y(240)
                    width(80)
                    height(30)
                }
            }

            Assertions.assertEquals(Geometry(120.0, 240.0, 60.0, 30.0, false), "source".geometry())
        }
    }


    @Test
    fun `should sum two geometries`() {
        Geometry(10.0, 20.0, 100.0, 200.0) +  Geometry(-10.0, 30.0, 100.0, 200.0) `should give` Geometry(-10.0, 20.0, 120.0, 210.0)
    }

    @Test
    fun `left + invalid = left`() {
        Geometry(10.0, 20.0, 100.0, 200.0) +  Geometry() `should give` Geometry(10.0, 20.0, 100.0, 200.0)
    }

    @Test
    fun `invalid + right = right`() {
        Geometry() + Geometry(10.0, 20.0, 100.0, 200.0) `should give` Geometry(10.0, 20.0, 100.0, 200.0)
    }

    @Test
    fun `should add an offset`() {
        Geometry(10.0, 20.0, 100.0, 200.0) + Offset(50.0, 60.0, 70.0, 80.0) `should give` Geometry(60.0, 80.0, 170.0, 280.0)
    }

    @Test
    fun `should translate`() {
        Geometry(10.0, 20.0, 100.0, 200.0) + Point(50.0, 60.0) `should give` Geometry(60.0, 80.0, 100.0, 200.0)
    }

    @Test
    fun `should reorder element by z index`() {
        svg {
            rect {
                x(100)
                y(200)
                width(100)
                height(100)
            }

            rect {
                z(-1)
                x(200)
                y(200)
                width(100)
                height(100)
            }

            rect {
                z(2)
                x(300)
                y(200)
                width(100)
                height(100)
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="300" height="100" viewBox="100 200 300 100">
                <rect x="200" y="200" width="100" height="100" />
                <rect x="100" y="200" width="100" height="100" />
                <rect x="300" y="200" width="100" height="100" />
            </svg>
        """.trimIndent()
    }

    @Test
    fun `should be destructured`() {
        val (x, y, width, height) = Geometry(10.0, 20.0, 100.0, 200.0)

        assertThat(x).isEqualTo(10.0)
        assertThat(y).isEqualTo(20.0)
        assertThat(width).isEqualTo(100.0)
        assertThat(height).isEqualTo(200.0)
    }

    @Test
    fun `should be valid when one property is not null`() {
        assertThat(RectGeometry(x=0.0).valid()).isTrue
        assertThat(RectGeometry(y=0.0).valid()).isTrue
        assertThat(RectGeometry(width=0.0).valid()).isTrue
        assertThat(RectGeometry(height=0.0).valid()).isTrue

        assertThat(RectGeometry().valid()).isFalse
    }

    @Test
    fun `should be a value object`() {
        assertThat(RectGeometry(12.0, 34.0, 56.0, 78.0))
            .isEqualTo(RectGeometry(12.0, 34.0, 56.0, 78.0))

        assertThat(RectGeometry(0.0, 34.0, 56.0, 78.0))
            .isNotEqualTo(RectGeometry(12.0, 34.0, 56.0, 78.0))

        assertThat(RectGeometry(12.0, 0.0, 56.0, 78.0))
            .isNotEqualTo(RectGeometry(12.0, 34.0, 56.0, 78.0))

        assertThat(RectGeometry(12.0, 34.0, 0.0, 78.0))
            .isNotEqualTo(RectGeometry(12.0, 34.0, 56.0, 78.0))

        assertThat(RectGeometry(12.0, 34.0, 56.0, 0.0))
            .isNotEqualTo(RectGeometry(12.0, 34.0, 56.0, 78.0))

        assertThat(RectGeometry(12.0, 34.0, 56.0, 78.0))
            .isNotEqualTo("abc")
    }

    @Test
    fun `should define geometry from another one`() {
        svg {
            rect {
                geometry(Geometry(100.0, 200.0, 100.0, 100.0))

            }
        } `should contain` """
                <rect x="100" y="200" width="100" height="100" />
        """.trimIndent()
    }
}
