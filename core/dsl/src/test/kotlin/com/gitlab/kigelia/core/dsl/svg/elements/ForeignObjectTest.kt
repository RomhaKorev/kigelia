package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Renderer
import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.HtmlStructure
import com.gitlab.kigelia.core.dsl.html.elements.div
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.`should contain`
import com.gitlab.kigelia.core.dsl.svg.svg
import org.junit.jupiter.api.Test
import com.gitlab.kigelia.core.dsl.html.orphan as htmlOrphan

class ForeignObjectPainter(parent: SvgElement): SvgElement("foreignObject", parent=parent) {
    fun content(f: HtmlStructure.HtmlBody.() -> Unit) {
        lateinit var element: Renderer
        html {
            body {
                val e = htmlOrphan(f)
                val addMetadata: Renderer.() -> Unit = {
                    "xmlns"("http://www.w3.org/1999/xhtml")
                }
                e.addMetadata()
                element = e
            }
        }
        add(element)
    }
}

private fun create(init: ForeignObjectPainter.() -> Unit, parent: SvgElement): ForeignObjectPainter {
    val element = ForeignObjectPainter(parent)
    element.init()
    return element
}

fun SvgElement.foreignObject(init: ForeignObjectPainter.() -> Unit): ForeignObjectPainter {
    val element = create(init, this)
    this.add(element)
    return element
}

class ForeignObjectTest {

    @Test
    fun `should insert foreign content`() {
        orphan {
            foreignObject {
            }
        }.`should contain`(
            """<foreignObject />"""
        )
    }

    @Test
    fun `should insert html content`() {
        orphan {
            foreignObject {
                content {
                    div {
                        -"foobar"
                    }
                }
            }
        }.`should contain`(
            """<div>foobar</div>"""
        )

        println(svg {
            foreignObject {
                content {
                    div {
                        -"foobar"
                    }
                }
            }
        }.render(4))
    }
}