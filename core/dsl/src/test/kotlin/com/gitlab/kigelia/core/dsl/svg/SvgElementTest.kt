package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.svg.elements.g
import com.gitlab.kigelia.core.tools.utils.Logger
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.geometry.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.lang.IllegalArgumentException

class SvgElementTest {
    @Test
    fun `should give its bounding rect`() {
        val element = orphan {
            rect {
                x(10)
                y(20)
                width(300)
                height(100)

                rect {
                    x(11)
                    y(21)
                    width(200)
                    height(50)
                }

                rect {
                    x(0)
                    y(0)
                    width(2000)
                    height(5000)
                    transparent()
                }
            }
        }

        assertThat(element.boundingRect()).isEqualTo(Geometry(10.0, 20.0, 300.0, 100.0))
    }

    @BeforeEach
    fun setup() {
        Logger.clear()
    }

    @ParameterizedTest
    @ValueSource(strings = ["g", "line", "rect", "circle", "polyline", "text", "path"])
    fun `should warn when using legacy way`(tag: String) {
        svg {
            tag.invoke {
            }
        }

        assertThat(Logger.messages().first()).contains(
                "should be built by using $tag {...} instead of \"$tag\"{...}"
        )
    }

    private fun a_parent_with_children(): List<SvgElement> {
        lateinit var child1: SvgElement
        lateinit var child2: SvgElement
        lateinit var child3: SvgElement
        val parent =
        svg {
            id("parent")
            child1 = rect {
                geometry {
                    x(10)
                    y(10)
                    width(100)
                    height(100)
                }
                id("child 1")

                rect {
                    geometry {
                        x(20)
                        y(20)
                        width(20)
                        height(20)
                    }
                    id("subchild 1")
                }
            }

            child2 = rect {
                geometry {
                    x(10)
                    y(10)
                    width(100)
                    height(100)
                }
                id("child 2")
                stroke("green")
            }

            child3 = rect {
                geometry {
                    x(50)
                    y(50)
                    width(100)
                    height(100)
                }
                id("child 3")
            }
        }
        return listOf(parent, child1, child2, child3)
    }

    @Test
    fun `should find sibling`() {
        val (parent, child, child2) = a_parent_with_children()

        assertThat(child.findSibling("child 2")).isEqualTo(child2)
        assertThat(parent.findSibling("child 2")).isEqualTo(child2)
        assertThat(parent.findSibling("child 45")).isNull()
    }

    @Test
    fun `should find the value of an attribute of the given sibling`() {
        val (_, child, _) = a_parent_with_children()

        val f: SvgElement.() -> String = { "child 2".valueOf("stroke") }

        assertThat(child.f()).isEqualTo("green")
    }

    @Test
    fun `should raise an error when searching the value of an attribute of the given sibling`() {
        val (_, child, _) = a_parent_with_children()

        val f: SvgElement.() -> String = { "child 45".valueOf("stroke") }

        assertThrows<IllegalArgumentException> { child.f() }
    }

    @Test
    fun `should find the geometry of a sibling`() {
        val (_, child, child2) = a_parent_with_children()

        val f: SvgElement.() -> Geometry = { "child 2".geometry() }

        assertThat(child.f()).isEqualTo(child2.geometry())
    }

    @Test
    fun `should define geometry with rectangle`() {
        assertThat(orphan {
            rect {
                geometry(Rectangle(Point(12, 34), Size(56, 78)))
            }
        }.geometry()).isEqualTo(Geometry(12.0, 34.0, 56.0, 78.0))
    }

    @Test
    fun `should raise an error when searching for the geometry of an unknown sibling`() {
        val (_, child) = a_parent_with_children()

        val f: SvgElement.() -> Geometry = { "child 45".geometry() }

        assertThrows<IllegalArgumentException> { child.f() }
    }

    @Test
    fun `should define painting properties`() {
        val element = orphan {
            g {
                fill("red")
                stroke("green")
            }
        }
        val elementWithNothing = orphan {
            g {
                noFill()
                noStroke()
            }
        }
        assertThat(element.render()).isEqualTo("""<g fill="red" stroke="green" />""")
        assertThat(elementWithNothing.render()).isEqualTo("""<g fill="none" stroke="none" />""")
    }

    @ParameterizedTest
    @MethodSource("anchors")
    fun `should give its anchor`(access: SvgElement.(String) -> Anchor, expected: Anchor) {
        val (_, child, _) = a_parent_with_children()
        assertThat(child.access("child 2")).isEqualTo(expected)
    }

    @ParameterizedTest
    @MethodSource("anchors")
    fun `should raise an error when searching for an unknown sibling anchor`(access: SvgElement.(String) -> Anchor) {
        val (_, child, _) = a_parent_with_children()
        assertThrows<IllegalArgumentException> {
            child.access("child 45")
        }
    }

    @ParameterizedTest
    @MethodSource("positionOf")
    fun `should give relative position`(access: SvgElement.(String) -> Point, expected: Point) {
        val (_, child, _) = a_parent_with_children()
        assertThat(child.access("child 2")).isEqualTo(expected)
    }

    @ParameterizedTest
    @MethodSource("positionOf")
    fun `should raise an error when searching for an unknown sibling relative position`(access: SvgElement.(String) -> Point) {
        val (_, child, _) = a_parent_with_children()
        assertThrows<IllegalArgumentException> {
            child.access("child 45")
        }
    }

    companion object {
        @JvmStatic
        fun anchors(): List<Arguments> {
            val top: SvgElement.(String) -> Anchor = { it.top() }
            val bottom: SvgElement.(String) -> Anchor = { it.bottom() }
            val left: SvgElement.(String) -> Anchor = { it.left() }
            val right: SvgElement.(String) -> Anchor = { it.right() }

            return listOf(Arguments.of(top, Anchor(Point(60.0, 10.0), Anchor.Kind.Top)),
                Arguments.of(bottom, Anchor(Point(60.0, 110.0), Anchor.Kind.Bottom)),
                Arguments.of(left, Anchor(Point(10.0, 60.0), Anchor.Kind.Left)),
                Arguments.of(right, Anchor(Point(110.0, 60.0), Anchor.Kind.Right)))
        }

        @JvmStatic
        fun positionOf(): List<Arguments> {
            val top: SvgElement.(String) -> Point = { 10.above(it) }
            val bottom: SvgElement.(String) -> Point = { 10.below(it) }
            val left: SvgElement.(String) -> Point = { 10.leftOf(it) }
            val right: SvgElement.(String) -> Point = { 10.rightOf(it) }

            return listOf(
                Arguments.of(top, Point(10.0, -100.0)),
                Arguments.of(bottom, Point(10.0, 120.0)),
                Arguments.of(left, Point(-100.0, 10.0)),
                Arguments.of(right, Point(120.0, 10.0))
            )
        }
    }

}
