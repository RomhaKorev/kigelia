package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.dsl.svg.helpers.connectors.ConnectorType

class SquaredConnectorTest: ConnectorTest(ConnectorType.Squared) {
    override fun horizontalConnector(): String {
        return """
                <g style="stroke: black; stroke-width: 2">
                    <line x1="120" y1="45" x2="135" y2="45" />
                    <line stroke-linecap="round" x1="135" y1="45" x2="135" y2="45" />
                    <line stroke-linecap="round" x1="135" y1="45" x2="150" y2="45" />
                </g>
        """.trimIndent()
    }

    override fun verticalConnector(): String {
        return """
                <g style="stroke: black; stroke-width: 2">
                    <line x1="70" y1="70" x2="70" y2="110" />
                    <line stroke-linecap="round" x1="70" y1="110" x2="70" y2="110" />
                    <line stroke-linecap="round" x1="70" y1="110" x2="70" y2="150" />
                </g>
        """.trimIndent()
    }

    override fun rightToTopConnector(): String {
        return """
                <g style="stroke: black; stroke-width: 2">
                    <line x1="120" y1="45" x2="250" y2="45" />
                    <line stroke-linecap="round" x1="250" y1="45" x2="250" y2="150" />
                </g>
        """.trimIndent()
    }

    override fun bottomToLeftConnector(): String  = """
                <g>
                    <line x1="70" y1="70" x2="70" y2="175" />
                    <line stroke-linecap="round" x1="70" y1="175" x2="200" y2="175" />
                </g>
    """.trimIndent()

    override fun diagonalConnector(): String {
        return """
                <g style="stroke: black; stroke-width: 2">
                    <line x1="70" y1="70" x2="70" y2="110" />
                    <line stroke-linecap="round" x1="70" y1="110" x2="170" y2="110" />
                    <line stroke-linecap="round" x1="170" y1="110" x2="170" y2="150" />
                </g>
        """.trimIndent()
    }

    override fun endingByAnArrow(): String {
        return """
            <defs>
                <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                    <polygon points="0 0, 10 3.5, 0 7" />
                </marker>
            </defs>
            <g style="stroke: black; stroke-width: 2">
                <line x1="70" y1="70" x2="70" y2="110" />
                <line stroke-linecap="round" x1="70" y1="110" x2="170" y2="110" />
                <line stroke-linecap="round" x1="170" y1="110" x2="170" y2="130" marker-end="url(#endarrow-0)" />
            </g>
        """.trimIndent()
    }

    override fun startingByAnArrow(): String {
        return """
                <defs>
                    <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                        <polygon points="10 0, 10 7, 0 3.5" />
                    </marker>
                </defs>
                <g style="stroke: black; stroke-width: 2">
                    <line x1="70" y1="90" x2="70" y2="110" marker-start="url(#startarrow-0)" />
                    <line stroke-linecap="round" x1="70" y1="110" x2="170" y2="110" />
                    <line stroke-linecap="round" x1="170" y1="110" x2="170" y2="150" />
                </g>
        """.trimIndent()
    }

    override fun startingAndEndingByAnArrow(): String {
        return """
                <defs>
                    <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                        <polygon points="0 0, 10 3.5, 0 7" />
                    </marker>
                    <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                        <polygon points="10 0, 10 7, 0 3.5" />
                    </marker>
                </defs>
                <g style="stroke: black; stroke-width: 2">
                    <line x1="140" y1="45" x2="150" y2="45" marker-start="url(#startarrow-0)" />
                    <line stroke-linecap="round" x1="150" y1="45" x2="150" y2="45" />
                    <line stroke-linecap="round" x1="150" y1="45" x2="160" y2="45" marker-end="url(#endarrow-0)" />
                </g>
        """.trimIndent()
    }

    override fun fromBottomToTop(): String {
        return """
                <g style="stroke: black; stroke-width: 2">
                    <line x1="122" y1="32" x2="122" y2="62" />
                    <line stroke-linecap="round" x1="122" y1="62" x2="47" y2="62" />
                    <line stroke-linecap="round" x1="47" y1="62" x2="47" y2="92" />
                </g>
        """.trimIndent()
    }

    override fun withLabel(): String {
        return """
                <g style="stroke: black; stroke-width: 2">
                    <line x1="120" y1="45" x2="185" y2="45" />
                    <line stroke-linecap="round" x1="185" y1="45" x2="185" y2="55" />
                    <line stroke-linecap="round" x1="185" y1="55" x2="250" y2="55" />
                </g>
                <text x="185" y="50" text-anchor="middle" dominant-baseline="middle">
                    foobar
                </text>
        """.trimIndent()
    }

    override fun coloredConnector(): String {
        return """
                <defs>
                    <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
                        <polygon points="0 0, 10 3.5, 0 7" />
                    </marker>
                    <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto" stroke="red" fill="red">
                        <polygon points="10 0, 10 7, 0 3.5" />
                    </marker>
                </defs>
                <g stroke="red">
                    <line x1="140" y1="45" x2="185" y2="45" marker-start="url(#startarrow-0)" />
                    <line stroke-linecap="round" x1="185" y1="45" x2="185" y2="75" />
                    <line stroke-linecap="round" x1="185" y1="75" x2="230" y2="75" marker-end="url(#endarrow-0)" />
                </g>
                <text x="185" y="60" text-anchor="middle" dominant-baseline="middle" fill="red">
                    foobar
                </text>
        """
    }

    override fun connectorStyle(): String = """
        <defs>
            <marker id="endarrow-0" markerWidth="5" markerHeight="3.5" refX="0" refY="1.75" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
                <polygon points="0 0, 5 1.75, 0 3.5" />
            </marker>
        </defs>
        <g stroke="red" stroke-width="3">
            <line x1="20" y1="40" x2="110" y2="40" />
            <line stroke-linecap="round" x1="110" y1="40" x2="110" y2="100" />
            <line stroke-linecap="round" x1="110" y1="100" x2="180" y2="100" marker-end="url(#endarrow-0)" />
        </g>
    """.trimIndent()

}
