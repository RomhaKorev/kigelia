package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.common.Renderer
import com.gitlab.kigelia.core.dsl.common.Tag
import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import java.io.File
import java.nio.file.Paths
import kotlin.math.exp

infix fun Any.`should give`(expected: Any) {
    Assertions.assertEquals(expected, this)
}

infix fun Tag.`should give`(expected: String): Tag {
    val name = Thread.currentThread().stackTrace[2].methodName
    val directory = Thread.currentThread().stackTrace[2].className.split(".").last()
    val filename = Paths.get("target", "generated-test-sources", "painter", directory, "$name.svg").toString()
    this `save as`(filename)
    assertThat(expected).isEqualToNormalizingWhitespace(this.render())
    return this
}


infix fun Renderer.`should contain`(expected: String): Renderer {
   assertThat(this.render()).containsIgnoringWhitespaces(expected)
    return this
}


fun `should raise an error`(content: () -> Unit) {
    Assertions.assertThrows(IllegalArgumentException::class.java, content)
}

infix fun Tag.`save as`(fileName: String): Tag {
    val myfile = File(fileName)
    val path = myfile.toPath()
    val directory = path.parent.toFile()
    if (!directory.exists())
        directory.mkdirs()

    myfile.printWriter().use { out ->
        out.println(this.render())
    }
    return this
}
