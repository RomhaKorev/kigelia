package com.gitlab.kigelia.core.dsl.html.bulma

import com.gitlab.kigelia.core.dsl.html.elements.figure
import com.gitlab.kigelia.core.dsl.html.elements.img
import com.gitlab.kigelia.core.dsl.html.html
import com.gitlab.kigelia.core.dsl.svg.`should contain`
import org.junit.jupiter.api.Test


class BulmaTest {

    @Test
    fun `should insert bulma deps`() {
        html {
            body {
                bulma {

                }
            }
        } `should contain` """<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">"""
    }

    @Test
    fun `should create a card`() {
        html {
            body {
                bulma {
                    card {
                    }
                }
            }
        } `should contain` """
              <div class="card" />
        """.trimIndent()
    }

    @Test
    fun `should add image to a card`() {
        html {
            body {
                bulma {
                    card {
                        image("https://bulma.io/images/placeholders/1280x960.png") {
                                alt("alternative text")
                                caption("a caption")
                            }
                        }
                }
            }
        } `should contain` """
              <div class="card-image">
                <figure class="image is-4by3">
                  <figcaption>a caption</figcaption>
                  <img src="https://bulma.io/images/placeholders/1280x960.png" alt="alternative text" />
                </figure>
              </div>
        """.trimIndent()
    }

    @Test
    fun `should add title and subtitle`() {
        html {
            body {
                bulma {
                    card {
                        title("John Smith")
                        subtitle("@johnsmith")
                    }
                }
            }
        } `should contain` """
            <div class="card">
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">John Smith</p>
                            <p class="subtitle is-6">@johnsmith</p>
                        </div>
                    </div>
                </div>
            </div>
        """.trimIndent()
    }

    @Test
    fun `should add content`() {
        html {
            body {
                bulma {
                    card {
                        content {
                            - """
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus nec iaculis mauris.
                            """.trimIndent()
                        }
                    }
                }
            }
        } `should contain` """
            <div class="card-content">
                <div class="content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Phasellus nec iaculis mauris.
                </div>
            </div>
        """.trimIndent()
    }

    @Test
    fun `should add pictogram`() {
        html {
            body {
                bulma {
                    card {
                        title("John Smith")
                        subtitle("@johnsmith")
                        leftMedia {
                            figure {
                                cssClass("image", "is-48x48")
                                img("https://bulma.io/images/placeholders/96x96.png") {
                                    alt("Placeholder image")
                                }
                            }
                        }
                    }
                }
            }
        } `should contain` """
            <div class="card">
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image" />
                            </figure>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">John Smith</p>
                            <p class="subtitle is-6">@johnsmith</p>
                        </div>
                    </div>
                </div>
            </div>
        """.trimIndent()
    }
}


