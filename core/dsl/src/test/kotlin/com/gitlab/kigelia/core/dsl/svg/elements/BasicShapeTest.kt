package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.kigelia.core.dsl.svg.`should give`
import com.gitlab.kigelia.core.geometry.*
import org.junit.jupiter.api.Test

class BasicShapeTest {

    @Test
    fun `should draw an ellipse`() {
        orphan {
            Ellipse(Point(12, 34), Size(56, 78)).draw()
        } `should give` """<ellipse cx="12" cy="34" rx="28" ry="39" />"""
    }

    @Test
    fun `should draw an ellipse with options`() {
        orphan {
            Ellipse(Point(12, 34), Size(56, 78)).draw {
                "stroke"("red")
            }
        } `should give` """<ellipse cx="12" cy="34" rx="28" ry="39" stroke="red" />"""
    }

    @Test
    fun `should draw a circle`() {
        orphan {
            Ellipse(Point(12, 34), Size(56, 56)).draw()
        } `should give` """<circle cx="12" cy="34" r="28" />"""
    }

    @Test
    fun `should draw a rectangle`() {
        orphan {
            Rectangle(Point(12, 34), Size(56, 78)).draw()
        } `should give` """<rect x="12" y="34" width="56" height="78" />"""
    }

    @Test
    fun `should draw a rectangle with options`() {
        orphan {
            Rectangle(Point(12, 34), Size(56, 78)).draw {
                "stroke"("red")
            }
        } `should give` """<rect x="12" y="34" width="56" height="78" stroke="red" />"""
    }

    @Test
    fun `should draw a line`() {
        orphan {
            Line(Point(12, 34), Point(56, 78)).draw()
        } `should give` """<line x1="12" y1="34" x2="56" y2="78" />"""
    }

    @Test
    fun `should draw a line with options`() {
        orphan {
            Line(Point(12, 34), Point(56, 78)).draw {
                "stroke"("red")
            }
        } `should give` """<line x1="12" y1="34" x2="56" y2="78" stroke="red" />"""
    }
}
