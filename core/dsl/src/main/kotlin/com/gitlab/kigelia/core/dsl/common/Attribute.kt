package com.gitlab.kigelia.core.dsl.common

abstract class Attribute(val name: String) {
    abstract fun render(): String
    abstract fun valid(): Boolean
    abstract fun value(): String
}
