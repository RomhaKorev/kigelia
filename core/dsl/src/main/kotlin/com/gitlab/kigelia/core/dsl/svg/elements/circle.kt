package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Point


class Circle(parent: SvgElement): SvgElement("circle", parent=parent) {
    private var center: Point = Point(0, 0)
    private var radius: Double  = 0.0

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    fun cx(value: Number) {
        geometry { x(value) }
        "cx"(value)
        center = Point(value.toDouble(), center.y)
    }

    fun cy(value: Number) {
        geometry { y(value) }
        "cy"(value)
        center = Point(center.x, value.toDouble())
    }

    fun center(value: Point) {
        geometry { x(value.x); y(value.y) }
        "cx"(value.x)
        "cy"(value.y)
        center = value
    }

    fun radius(v: Number) = r(v)
    fun r(v: Number) {
        val value = v.toDouble()
        geometry {
            width(value * 2)
            height(value * 2)
            x(center.x - value)
            y(center.y - value)
        }
        "r"(value)
        radius = value
    }

    override fun equals(other: Any?): Boolean {
        return other is Circle && this.center == other.center && this.radius.compareTo(radius) == 0
    }

    override fun hashCode(): Int {
        var result = center.hashCode()
        result = 31 * result + radius.hashCode()
        return result
    }


}

private fun create(init: Circle.() -> Unit, parent: SvgElement): Circle {
    val element = Circle(parent)
    element.init()
    return element
}

fun SvgElement.circle(init: Circle.() -> Unit): Circle {
    val element = create(init, this)
    this.add(element)
    return element
}
