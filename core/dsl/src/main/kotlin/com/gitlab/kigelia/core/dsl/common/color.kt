package com.gitlab.kigelia.core.dsl.common


data class Color(private val value: String) {

    constructor(r: Int, g: Int, b: Int): this("#${r.toHex()}${g.toHex()}${b.toHex()}")
    override fun toString(): String {
        return value
    }

    companion object {
        val None = Color(-1, -1, -1)

        val Blue = Color(95, 193, 183)
        val Orange = Color(237, 141, 70)
        val Pink = Color(239, 95, 97)
        val Yellow = Color(254, 210, 3)
        val Green = Color(200, 215, 30)
        val Red = Color(246, 0, 18)
        val White = Color(229, 229, 229)
        val DarkBlue = Color(49, 73, 87)
        val DarkGray = Color(54, 54, 54)

        val AltBlue = Color(96, 144, 228)
        val AltGreen = Color(154, 236, 122)
        val Purple = Color(134, 0, 200)
        val AltRed = Color(219, 0, 0)
        val AltOrange = Color(255, 128, 0)
        val AltYellow = Color(253, 239, 70)
        val Gray = Color(134, 134, 134)
        val LightBlue = Color(104, 185, 255)
        val LightGreen = Color(201, 255, 113)
        val LightPurple = Color(130, 116, 176)
        val LightRed = Color(255, 86, 86)
        val LightOrange = Color(255, 179, 102)
        val LightYellow = Color(253, 242, 133)
        val LightGray = Color(190, 190, 190)
        val AltDarkBlue = Color(8, 57, 161)
        val DarkGreen = Color(140, 208, 30)
        val DarkPurple = Color(115, 0, 171)
        val DarkRed = Color(186, 0, 0)
        val DarkOrange = Color(255, 81, 0)
        val DarkYellow = Color(255, 242, 25)
    }
}

private fun Int.toHex(): String {
    return this.toString(16).padStart(2, '0')
}
