package com.gitlab.kigelia.core.dsl.svg.helpers.connectors

object Identifier {
    private var next = 0

    fun unique(): Int {
        return next++
    }

    fun reset() {
        next = 0
    }
}
