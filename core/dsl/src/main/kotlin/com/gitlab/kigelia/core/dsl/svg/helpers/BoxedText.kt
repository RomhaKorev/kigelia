package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.rect
import com.gitlab.kigelia.core.dsl.svg.elements.text as svgText


class BoxedText(parent: SvgElement? = null): SvgElement("g", parent) {

    private val rect: SvgElement = rect {
        "fill"("none")
        "stroke"("black")
    }
    private val content = svgText {}

    fun box(init: SvgElement.() -> Unit) {
        rect.init()
    }

    override fun geometry(): Geometry {
        return rect.geometry()
    }

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    override fun geometry(init: Geometry.() -> Unit) {

        rect.geometry(init)
        val center = rect.geometry().center()

        content.geometry {
            x(center.x)
            y(center.y)
        }
    }

    fun text(value: String, init: SvgElement.() -> Unit = {}) {
        val basicStyle: SvgElement.() -> Unit = {
            - value
            "text-anchor"("middle")
            "dominant-baseline"("middle")
        }
        this.content.basicStyle()
        this.content.init()
    }
}

fun boxedText(init: BoxedText.() -> Unit): SvgElement {
    val element = BoxedText()
    element.init()
    return element
}

fun SvgElement.boxedText(init: BoxedText.() -> Unit): SvgElement {
    val element = BoxedText(this)
    element.init()
    this.add(element)
    return this
}
