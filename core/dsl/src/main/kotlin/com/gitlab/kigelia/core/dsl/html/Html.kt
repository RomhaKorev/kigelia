package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.common.Renderer
import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.dsl.html.bulma.bulma

fun orphan(tag: String, init: HtmlElement.() -> Unit): HtmlElement {
    return element(tag, init)
}

fun orphan(creator: HtmlStructure.HtmlBody.() -> Unit): Renderer {
    return html {
        body {
            creator()
        }
    }
}

internal fun element(tag: String, init: HtmlElement.() -> Unit): HtmlElement {
    val instance = HtmlElement(tag)
    instance.init()
    return instance
}

class HtmlStructure: Tag("html") {

    abstract class HtmlBody(val document: HtmlStructure): HtmlElement("body") {
        override fun visible(): Boolean { return children.isNotEmpty() }
    }
    abstract class HtmlHeader(val document: HtmlStructure): HtmlElement("head") {
        override fun visible(): Boolean { return children.isNotEmpty() }

        fun css(f: () -> String) {
            "style" {
                - f()
            }
        }
    }

    private class HtmlBodyImpl(document: HtmlStructure): HtmlBody(document)
    private class HtmlHeaderImpl(document: HtmlStructure): HtmlHeader(document)

    private var head: HtmlHeader = HtmlHeaderImpl(this)
    private var body: HtmlBody = HtmlBodyImpl(this)

    init {
        add(head)
        add(body)
    }

    fun head(init: HtmlHeader.() -> Unit): HtmlElement {
        head.init()
        return head
    }

    fun body(init: HtmlBody.() -> Unit): HtmlElement {
        body.init()
        return body
    }
}


fun html(init: HtmlStructure.() -> Unit): HtmlStructure {
    val element = HtmlStructure()
    element.init()
    return element
}
