package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.dsl.svg.*
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Padding
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Translation

class Background(parent: SvgElement, private var color: Color): SvgElement("rect", parent) {
    private val geometry = ProxyGeometry(parent)

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf(
            SvgProperty("x", xOrNull()),
            SvgProperty("y", yOrNull()),
            SvgProperty("width", widthOrNull()),
            SvgProperty("height", heightOrNull())
        ).filter { it.valid() }
    }

    init {
        z(-1)
    }

    override fun geometry(): Geometry {
        return geometry
    }

    override fun visible(): Boolean { return color != Color.None }

    fun color(value: String) = color(Color(value))
    fun color(value: Color) {
        color = value
        "fill"(color)
    }


    fun apply(init: Background.() -> Unit) {
        init()
    }
}

class G(parent: SvgElement? = null): SvgElement("g", parent=parent) {
    private var background: Background = Background(this, color= Color.None)

    fun background() = background

    fun background(init: Background.() -> Unit) {
        background.apply(init)
    }

    var translation =  Translation(Point(0.0, 0.0), Point(0.0, 0.0))
    var rotation: Number = 0.0

    fun transform(init: Transformation.() -> Unit) {
        Transformation().init()
    }

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    private var padding = Padding(0.0, 0.0)


    fun padding(padding: Padding) {
        this.padding = padding
    }

    override fun geometry(): Geometry {
        return (this.children.filterIsInstance(SvgElement::class.java).map { it.geometry() }.fold(super.geometry()) { a, b -> a + b } + translation.delta)
                .grownBy(padding)
    }

    override fun equals(other: Any?): Boolean {
        return other is G && this.children == other.children
    }

    override fun hashCode(): Int {
        return children.hashCode()
    }

    inner class Transformation {
        fun translate(newTranslation: Translation) {
            translation = newTranslation
            setTransformation()
        }
        fun translate(point: Point) {
            val translation = geometry().to(point)
            translate(translation)
        }

        fun rotate(angle: Double) {
            rotation = angle
            setTransformation()
        }

        private fun setTransformation() {
            var value = ""
            if (translation.valid) {
                value += "translate(${translation.dx}, ${translation.dy})"
            }
            if (rotation.toDouble() != 0.0) {
                value += "rotate(${rotation})"
            }

            if (value.isNotBlank()) {
                "transform"(value)
            }
        }
    }


}


fun SvgElement.g(init: G.() -> Unit): G {
    val g = G(parent=this)
    g.init()
    this.add(g)
    if (g.background().visible())
        this.add(g.background())
    return g
}
