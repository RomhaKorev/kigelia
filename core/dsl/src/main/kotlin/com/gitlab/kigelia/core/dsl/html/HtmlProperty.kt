package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.dsl.common.Tag

class HtmlProperty(name: String, private var value: Any): Attribute(name) {
    override fun render(): String {
        return "$name=\"${value()}\""
    }

    override fun value(): String {
        return value.toString().replace("\\.0$".toRegex(), "")
    }

    override fun valid(): Boolean {
        return value.toString().isNotEmpty()
    }

    operator fun invoke(value: Any): HtmlProperty {
        this.value = value
        return this
    }
}

fun Tag.htmlProperty(name: String, value: String): HtmlProperty {
    val property = HtmlProperty(name, value)
    this.add(property)
    return property
}
