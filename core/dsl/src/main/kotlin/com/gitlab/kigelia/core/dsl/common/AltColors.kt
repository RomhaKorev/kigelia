package com.gitlab.kigelia.core.dsl.common

enum class AltColors(val value: String) {
    Blue         ("#33c9e4"),
    Green        ("#459b69"),
    BlueGreen    ("#33bca5"),
    Red          ("#DB0000"),
    Orange       ("#FF8000"),
    Yellow       ("#FDEF46"),
    Gray         ("#868686"),
    LightBlue    ("#68B9FF"),
    LightGreen   ("#C9FF71"),
    LightPurple  ("#8274B0"),
    LightRed     ("#FF5656"),
    LightOrange  ("#FFB366"),
    LightYellow  ("#FDF285"),
    LightGray    ("#BEBEBE"),
    DarkBlue     ("#00947E"),
    DarkGreen    ("#8CD01E"),
    DarkPurple   ("#7300AB"),
    DarkRed      ("#BA0000"),
    DarkOrange   ("#ff5100"),
    DarkYellow   ("#FFF219"),
    DarkGray     ("#505050");

    override fun toString(): String {
        return this.value
    }

    companion object {
        operator fun get(index: Int): AltColors {
            return values()[index]
        }
    }
}


// b(0, 148, 126)