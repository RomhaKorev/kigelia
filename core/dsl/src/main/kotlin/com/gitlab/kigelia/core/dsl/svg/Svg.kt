package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.geometry.Geometry

operator fun String.invoke(init: SvgElement.() -> Unit): SvgElement {
    return element(this, null, init)
}

fun orphan(tag: String, init: SvgElement.() -> Unit): SvgElement {
    return element(tag, null, init)
}

fun <T> orphan(creator: SvgElement.() -> T): T {
    return element("", null, {}).creator()
}

internal fun element(tag: String, parent: SvgElement?, init: SvgElement.() -> Unit): SvgElement {
    val instance = SvgElement(tag, parent)
    instance.init()
    return instance
}

class Metadata: SvgElement("metadata") {
    init {
        transparent = true
    }
    override fun visible(): Boolean {
        return this.children.isNotEmpty()
    }
}

class Svg: SvgElement("svg") {
    private var viewbox = Viewbox()
    private val metadata = Metadata()

    private var horizontalMargin: Double = 0.0
    private var verticalMargin: Double = 0.0


    init {
        add(metadata)
    }

    override fun args(): String {
        return "xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" " +
                listOf(super.args(), viewbox.render()).filter { it.isNotBlank() }.joinToString(" ")
    }

    fun viewbox(init: Viewbox.() -> Unit) {
        viewbox.init()
    }

    fun viewbox(minx: Number, miny: Number, width: Number, height: Number) {
        geometry {
            width(width)
            height(height)
        }
        viewbox {
            minx(minx)
            miny(miny)
            width(width)
            height(height)
        }
    }

    fun metadata(init: SvgElement.() -> Unit) {
        metadata.init()
    }

    override fun boundingRect(): Geometry {
        val childrenGeometries = children.filterIsInstance<SvgElement>().filter { !it.transparent }.map { it.geometry() }
        return childrenGeometries.fold(Geometry(0.0, 0.0, 0.0, 0.0, false)) { acc: Geometry, elementGeometry: Geometry -> acc + elementGeometry }
    }

    fun horizontalMargin(): Double {
        return horizontalMargin
    }

    fun verticalMargin(): Double {
        return verticalMargin
    }
    inner class GrowingFunctions {
        fun horizontal(value: Number) {
            horizontalMargin = value.toDouble()
            geometry {  }
        }

        fun vertical(value: Number) {
            verticalMargin = value.toDouble()
            geometry {  }
        }
    }
    fun growBy(init: GrowingFunctions.() -> Unit) {
        GrowingFunctions().init()
    }

}


fun svg(init: Svg.() -> Unit): Svg {
    val element = Svg()
    element.init()

    val (x, y, width, height) = element.boundingRect()

    val svgInit: Svg.() -> Unit = {
        if (!geometry().valid()) {
            geometry {
                if (width != 0.0) "width".ifNotPresent(width + horizontalMargin() * 2)
                if (height != 0.0) "height".ifNotPresent(height + verticalMargin() * 2)
            }
        }

        viewbox {
            minxOr(x - horizontalMargin())
            minyOr(y - verticalMargin())
            widthOr(width + horizontalMargin() * 2)
            heightOr(height + verticalMargin() * 2)
        }
    }

    element.svgInit()

    return element
}
