package com.gitlab.kigelia.core.dsl.svg.elements

infix fun Boolean.thenWarn(message: String): Boolean {
    if (this)
        println(message)
    return this
}
