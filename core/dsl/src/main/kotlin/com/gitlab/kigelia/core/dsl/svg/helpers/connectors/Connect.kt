package com.gitlab.kigelia.core.dsl.svg.helpers.connectors

import com.gitlab.kigelia.core.geometry.Anchor
import com.gitlab.kigelia.core.dsl.svg.SvgElement

fun SvgElement.connect(source: Anchor, destination: Anchor, type: ConnectorType, ending: Marker = Marker.None, starting: Marker = Marker.None, endingSize: Double = 10.0, init: Connector.() -> Unit): Connector {
    val line = when (type) {
        ConnectorType.Squared ->  SquaredConnector(source, destination, endingSize)
        ConnectorType.Curvy ->  CurvyConnector(source, destination, endingSize)
        else -> StraightConnector(source, destination, endingSize)
    }
    line.applyEndings(starting, ending)
    line.init()

    this.add(line)
    return line
}
