package com.gitlab.kigelia.core.dsl.svg.helpers.connectors


enum class ConnectorType {
    Straight,
    Squared,
    Curvy
}





