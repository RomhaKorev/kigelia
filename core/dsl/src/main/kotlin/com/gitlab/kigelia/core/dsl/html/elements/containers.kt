package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.HtmlElement

fun HtmlElement.create(name: String, f: HtmlElement.() -> Unit, inlineContent: Boolean = false): HtmlElement {
    val element = HtmlElement(name, inlineContent)
    element.f()
    this.add(element)
    return element
}

fun HtmlElement.div(f: HtmlElement.() -> Unit) = create("div", f)
fun HtmlElement.span(f: HtmlElement.() -> Unit) = create("span", f)
fun HtmlElement.p(f: HtmlElement.() -> Unit) = create("p", f, inlineContent = true)