package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.HtmlElement

class Image(src: String): HtmlElement("img") {
    init {
        attribute("src", src)
    }

    fun alt(value: String) {
        attribute("alt", value)
    }
}

fun HtmlElement.img(src: String, f: Image.() -> Unit): HtmlElement {
    val e = Image(src)
    e.f()
    add(e)
    return e
}

class Figure: HtmlElement("figure") {
    var figcaption = HtmlElement("figcaption")
    fun caption(value: String) {
        children.remove(figcaption)
        figcaption = HtmlElement("figcaption", inlineContent = true)
        val f: HtmlElement.() -> Unit = {
            - value
        }
        figcaption.f()
        add(figcaption)
    }
}

fun HtmlElement.figure(f: Figure.() -> Unit): HtmlElement {
    val e = Figure()
    e.f()
    add(e)
    return e
}