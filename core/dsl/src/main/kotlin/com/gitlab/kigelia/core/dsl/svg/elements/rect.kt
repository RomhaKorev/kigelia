package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Size

class Rect(parent: SvgElement): SvgElement("rect", parent=parent) {

    private var position: Point = Point(0, 0)
    private var size = Size(0, 0)
    fun x(value: Number) {
        geometry { x(value) }
        position = position.copy(x = value.toDouble())
    }

    fun y(value: Number) {
        geometry { y(value) }
        position = position.copy(y = value.toDouble())
    }

    fun width(value: Number) {
        geometry { width(value) }
        size = size.copy(width = value.toDouble())
    }

    fun height(value: Number) {
        geometry { height(value) }
        size = size.copy(height = value.toDouble())
    }

    fun at(x: Number, y: Number) {
        x(x)
        y(y)
    }

    fun size(width: Number, height: Number) {
        width(width)
        height(height)
    }

    override fun equals(other: Any?): Boolean {
        return other is Rect && this.position == other.position && this.size == other.size
    }

    override fun hashCode(): Int {
        var result = position.hashCode()
        result = 31 * result + size.hashCode()
        return result
    }


}

private fun create(init: Rect.() -> Unit, parent: SvgElement): Rect {
    val element = Rect(parent)
    element.init()
    return element
}

fun SvgElement.rect(init: Rect.() -> Unit): Rect {
    val element = create(init, this)
    this.add(element)
    return element
}