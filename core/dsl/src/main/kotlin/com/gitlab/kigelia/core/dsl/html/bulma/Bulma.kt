package com.gitlab.kigelia.core.dsl.html.bulma

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.HtmlStructure
import com.gitlab.kigelia.core.dsl.html.bulma.card.Card
import com.gitlab.kigelia.core.dsl.html.bulma.columns.Columns
import com.gitlab.kigelia.core.dsl.html.link

class Bulma: HtmlElement("") {
    fun HtmlElement.card(f: Card.() -> Unit) {
        val e = Card()
        e.f()
        add(e)
    }

    fun HtmlElement.columns(f: Columns.() -> Unit): Columns {
        val e = Columns()
        e.f()
        add(e)
        return e
    }

}

fun HtmlStructure.HtmlBody.bulma(f: Bulma.() -> Unit): Bulma {
    this.document.head {
        link {
            rel("stylesheet")
            href("https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css")
        }
    }
    val e = Bulma()
    e.f()
    add(e)
    return e
}