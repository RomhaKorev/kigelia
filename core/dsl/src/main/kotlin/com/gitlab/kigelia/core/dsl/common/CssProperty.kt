package com.gitlab.kigelia.core.dsl.common

class CssProperty(val name: String, private var value: Any) {
    fun render(): String {
        val valueString = value.toString().replace("\\.0$".toRegex(), "")
        return "$name: $valueString"
    }

    operator fun invoke(value: Any): CssProperty {
        this.value = value
        return this
    }
}
