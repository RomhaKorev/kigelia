package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Ellipse
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.geometry.*

class Command(private val command: String, val points: List<Point>) {
    constructor(command: String, vararg p: Point): this(command, p.toList())

    fun render(): String {
        return "$command${points.joinToString(", ") { "${it.x} ${it.y}" }}"
    }

    override fun equals(other: Any?): Boolean {
        return other is Command && (command == other.command) && points == other.points
    }

    override fun hashCode(): Int {
        var result = command.hashCode()
        result = 31 * result + points.hashCode()
        return result
    }


}



class PathPainter(parent: SvgElement): SvgElement("path", parent=parent) {
    private val commands = mutableListOf<Command>()
    private var currentPosition = Point(0, 0)
    private var closed = false

    val position: Point
        get() = currentPosition
    private fun add(name: String, vararg p: Point) {
        commands.add(Command(name, *p))
        currentPosition = p.last()
        generate()
    }

    private fun add(name: String, vararg p: DPoint) {
        commands.add(Command(name, p.map { it.toPoint() }))
        currentPosition += p.last()
        generate()
    }

    private fun generate() {
        val d = commands.joinToString(" ") { it.render() }
        "d"(if (closed) {
            "$d Z"
        } else {
            d
        })
    }

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }


    override fun geometry(): Geometry {
        return this.commands.flatMap { it.points }.boundingRect()
    }

    fun close() {
        closed = true
        generate()
    }

    fun move(x: Number, y: Number) = move(Point(x.toDouble(), y.toDouble()))
    fun move(p: Point) = add("M", p)
    fun move(p: DPoint) = add("m", p)

    fun lineTo(x: Number, y: Number) = lineTo(Point(x.toDouble(), y.toDouble()))
    fun lineTo(p: Point) = add("L", p)
    fun lineTo(p: DPoint) = add("l", p)

    fun lineTo(vararg p: DPoint) = p.map { add("l", it) }

    fun curveTo(c1: Point, c2: Point, p: Point) = add("C", c1, c2, p)
    fun curveTo(c1: DPoint, c2: DPoint, p: DPoint) = add("c", c1, c2, p)
    fun shorthandCurveTo(c2: Point, p: Point) = add("S", c2, p)
    fun shorthandCurveTo(c2: DPoint, p: DPoint) = add("s", c2, p)

    fun quadraticBezierCurveTo(c: Point, p: Point) = add("Q", c, p)
    fun quadraticBezierCurveTo(c: DPoint, p: DPoint) = add("q", c, p)

    fun shorthandQuadraticBezierCurveTo(p: Point) = add("T", p)
    fun shorthandQuadraticBezierCurveTo(p: DPoint) = add("t", p)

    fun arcTo(size: Size, xAxisRotation: Double, largeArcFlag: Int, sweepFlag: Int, point: Point) {
        add("A ${size.width} ${size.height} $xAxisRotation $largeArcFlag $sweepFlag ", point)
    }

    fun arcTo(size: Size, xAxisRotation: Double, largeArcFlag: Int, sweepFlag: Int, point: DPoint) {
        add("a ${size.width} ${size.height} $xAxisRotation $largeArcFlag $sweepFlag ", point)
    }

    fun arcMoveTo(ellipse: Ellipse, startAngle: Double, spanAngle: Double, direction: Direction = Direction.CounterClock) {
        arcMoveTo(ellipse, startAngle, spanAngle, true, direction)
    }

    private fun arcMoveTo(ellipse: Ellipse, startAngle: Number, spanAngle: Number, moveBeforeArc: Boolean, direction: Direction) {
        fun f(startAngle: Double, spanAngle: Double) {
            if (direction == Direction.CounterClock) {
                val start = startAngle.toDouble() / 360.0
                val end = (startAngle + spanAngle) / 360.0

                val a = ellipse.pointAtPercent(start)
                val b = ellipse.pointAtPercent(end)

                if (moveBeforeArc) {
                    this.move(a)
                }

                val flag = if (spanAngle > 180) 1 else 0
                this.arcTo(ellipse.size / 2, 0.0, flag, 0, b)
            } else {
                val start = startAngle / 360.0
                val end = (startAngle - spanAngle) / 360.0

                val a = ellipse.pointAtPercent(start)
                val b = ellipse.pointAtPercent(end)

                if (moveBeforeArc) {
                    this.move(a)
                }

                val flag = if (spanAngle > 180) 1 else 0

                this.arcTo(ellipse.size / 2, 0.0, flag, 1, b)
            }
        }
        f(startAngle.toDouble(), spanAngle.toDouble())
    }

    fun arcTo(size: Size, startAngle: Number, spanAngle: Number, direction: Direction = Direction.CounterClock) {
        fun f(startAngle: Double, spanAngle: Double) {
            val ellipse = Ellipse(currentPosition, size)
            val offset = ellipse.pointAtPercent(startAngle / 360.0)
            arcMoveTo(ellipse.translated(currentPosition - offset), startAngle, spanAngle, false, direction)
        }
        f(startAngle.toDouble(), spanAngle.toDouble())
    }

    override fun equals(other: Any?): Boolean {
        return other is PathPainter && (commands == other.commands) && (currentPosition == other.currentPosition) && closed == other.closed
    }

    override fun hashCode(): Int {
        var result = commands.hashCode()
        result = 31 * result + currentPosition.hashCode()
        result = 31 * result + closed.hashCode()
        return result
    }


}


fun SvgElement.path(init: PathPainter.() -> Unit): PathPainter {
    val pathPainter = PathPainter(parent=this)
    pathPainter.init()
    this.add(pathPainter)
    return pathPainter
}
