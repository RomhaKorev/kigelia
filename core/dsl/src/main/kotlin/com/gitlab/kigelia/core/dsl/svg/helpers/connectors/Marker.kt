package com.gitlab.kigelia.core.dsl.svg.helpers.connectors

enum class Marker {
    None,
    Arrow,
}
