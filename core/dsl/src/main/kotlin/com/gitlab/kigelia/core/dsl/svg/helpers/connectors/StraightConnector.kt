package com.gitlab.kigelia.core.dsl.svg.helpers.connectors

import com.gitlab.kigelia.core.geometry.Anchor
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.LinePainter
import com.gitlab.kigelia.core.dsl.svg.elements.line
import kotlin.math.abs
import kotlin.math.min

class StraightConnector(source: Anchor, destination: Anchor, endingSize: Double = 10.0): Connector(source, destination, endingSize) {
    init {
        connector = line {
            val src = source.point
            val dst = destination.point
            x1(src.x)
            y1(src.y)
            x2(dst.x)
            y2(dst.y)
        }
    }

    override fun geometry(): Geometry {
        return Geometry(min(source.x, destination.x), min(source.y, destination.y), abs(source.x - destination.x), abs(source.y - destination.y), true)
    }

    override fun applyArrowAtStart(identifier: Int) {
        val segment = connector.toSegment()
        val newEnd = segment.pointAtPointFromStart(endingSize * 2)

        val applyArrow : LinePainter.() -> Unit = {
            x1(newEnd.x)
            y1(newEnd.y)
            "marker-start"("url(#startarrow-$identifier)")
        }

        (connector as LinePainter).applyArrow()
    }

    override fun applyArrowAtEnd(identifier: Int) {
        val segment = connector.toSegment()//Segment(source.point, destination.point)
        val newEnd = segment.pointAtPointFromEnd(endingSize * 2)

        val applyArrow : SvgElement.() -> Unit = {
            "x2"(newEnd.x)
            "y2"(newEnd.y)
            "marker-end"("url(#endarrow-$identifier)")
        }

        connector.applyArrow()
    }
}
