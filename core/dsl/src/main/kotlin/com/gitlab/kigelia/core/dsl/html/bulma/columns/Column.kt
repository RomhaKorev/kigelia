package com.gitlab.kigelia.core.dsl.html.bulma.columns

import com.gitlab.kigelia.core.dsl.html.HtmlElement

class Column: HtmlElement("div") {
    init {
        cssClass("column")
    }
    fun `is three quarters`() { cssClass("is-three-quarters") }
    fun `is two thirds`() = cssClass("is-two-thirds")
    fun `is half`() = cssClass("is-half")
    fun `is one third`() = cssClass("is-one-third")
    fun `is one quarter`() = cssClass("is-one-quarter")
    fun `is full`() = cssClass("is-full")
    fun `is four fifths`() = cssClass("is-four-fifths")
    fun `is three fifths`() = cssClass("is-three-fifths")
    fun `is two fifths`() = cssClass("is-two-fifths")
    fun `is one fifth`() = cssClass("is-one-fifth")
}