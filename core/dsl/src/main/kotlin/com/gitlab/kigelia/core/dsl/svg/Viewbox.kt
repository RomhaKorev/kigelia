package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.common.Attribute

class Viewbox: Attribute("viewBox") {
    private var minx: Number? = null
    private var miny: Number? = null
    private var width: Number? = null
    private var height: Number? = null

    fun minx(value: Number) {
        minx = value
    }

    fun miny(value: Number) {
        miny = value
    }

    fun width(value: Number) {
        width = value
    }

    fun height(value: Number) {
        height = value
    }

    fun minxOr(value: Number) {
        if (minx != null)
            return
        minx = value
    }

    fun minyOr(value: Number) {
        if (miny != null)
            return
        miny = value
    }

    fun widthOr(value: Number) {
        if (width != null)
            return
        width = value
    }

    fun heightOr(value: Number) {
        if (height != null)
            return
        height = value
    }

    override fun value(): String {
        return "${minx.format()} ${miny.format()} ${width.format()} ${height.format()}"
    }

    override fun render(): String {
        if (valid())
            return "$name=\"${value()}\""
        return ""
    }

    override fun valid(): Boolean {
        return minx.valid() || miny.valid() || width.valid() || height.valid()
    }

    private fun Number?.valid(): Boolean {
        return (this !=null && this.toDouble() != 0.0)
    }

    private fun Number?.format(): String {
        if (this == null)
            return "0"
        return this.toString().replace("\\.0$".toRegex(), "")
    }

}

