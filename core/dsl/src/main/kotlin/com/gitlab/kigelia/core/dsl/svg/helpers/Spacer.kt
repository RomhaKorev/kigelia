package com.gitlab.kigelia.core.dsl.svg.helpers

import com.gitlab.kigelia.core.dsl.svg.SvgElement

class Spacer(parent: SvgElement): SvgElement("", parent) {

    override fun visible(): Boolean { return false }

    fun x(value: Number) {
        geometry {
            x(value)
        }
    }

    fun y(value: Number) {
        geometry {
            y(value)
        }
    }

    fun width(value: Number) {
        geometry {
            width(value)
        }
    }

    fun height(value: Number) {
        geometry {
            height(value)
        }
    }
}


fun SvgElement.spacer(init: Spacer.() -> Unit): SvgElement {
    val element = Spacer(this)
    element.init()
    this.add(element)
    return this
}
