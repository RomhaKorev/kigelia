package com.gitlab.kigelia.core.dsl.html.elements

import com.gitlab.kigelia.core.dsl.html.HtmlElement

fun HtmlElement.h(level: Int, f: HtmlElement.() -> Unit): HtmlElement {
    val element = HtmlElement("h$level", inlineContent=true)
    element.f()
    this.add(element)
    return element
}

