package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import kotlin.math.abs

class PolygonPainter(parent: SvgElement): SvgElement("polygon", parent=parent) {
    private var points = listOf<Point>()

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    fun points(vararg points: Point) {
        this.points = points.toList()

        "points"(this.points.joinToString(", ") {(x, y) -> "$x $y" }.replace("(\\d+)\\.0+(\\D)".toRegex(), "$1$2"))

        if (points.isEmpty())
            return

        val x = points.minOf { it.x }
        val y = points.minOf { it.y }
        val width = abs(points.maxOf { it.x } - x)
        val height = abs(points.maxOf { it.y } - y)

        geometry {
            x(x)
            y(y)
            width(width)
            height(height)
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is PolygonPainter && points == other.points
    }

    override fun hashCode(): Int {
        return points.hashCode()
    }


}


fun SvgElement.polygon(init: PolygonPainter.() -> Unit): PolygonPainter {
    val element = PolygonPainter(this)
    element.init()
    this.add(element)
    return element
}
