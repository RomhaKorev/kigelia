package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.geometry.Geometry
import kotlin.math.abs
import kotlin.math.min

class LinePainter(parent: SvgElement): SvgElement("line", parent=parent) {
    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    private var x1: Double? = null
    private var y1: Double? = null
    private var x2: Double? = null
    private var y2: Double? = null

    val p1: Point
        get() = Point(x1!!, y1!!)

    val p2: Point
        get() = Point(x2!!, y2!!)

    fun x1(value: Number) {
        x1 = value.toDouble()
        "x1"(value)
    }

    fun x2(value: Number) {
        x2 = value.toDouble()
        "x2"(value)
    }

    fun y1(value: Number) {
        y1 = value.toDouble()
        "y1"(value)
    }

    fun y2(value: Number) {
        y2 = value.toDouble()
        "y2"(value)
    }

    fun p1(p: Point) {
        x1(p.x)
        y1(p.y)
    }

    fun p2(p: Point) {
        x2(p.x)
        y2(p.y)
    }

    internal fun defineGeometry() {
        if (x1 == null && this.hasAttribute("x1")) {
            x1(this.attribute("x1").value().toDouble())
        }

        if (x2 == null && this.hasAttribute("x2")) {
            x2(this.attribute("x2").value().toDouble())
        }

        if (y1 == null && this.hasAttribute("y1")) {
            y1(this.attribute("y1").value().toDouble())
        }

        if (y2 == null && this.hasAttribute("y2")) {
            y2(this.attribute("y2").value().toDouble())
        }

        if ((x1 == null) thenWarn "x1 is not defined!"
                ||
            (y1 == null) thenWarn "y1 is not defined!"
                ||
            (x2 == null) thenWarn "x2 is not defined!"
                ||
            (y2 == null) thenWarn "y2 is not defined!"
        )
            return

        geometry {
            x(min(x1!!, x2!!))
            y(min(y1!!, y2!!))
            width(abs(x1!! - x2!!))
            height(abs(y1!! - y2!!))
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is LinePainter && this.p1 == other.p1 && this.p2 == other.p2
    }

    override fun hashCode(): Int {
        var result = p1.hashCode()
        result = 31 * result + p2.hashCode()
        return result
    }


}

private fun create(init: LinePainter.() -> Unit, parent: SvgElement): LinePainter {
    val line = LinePainter(parent)
    line.init()
    line.defineGeometry()
    return line
}

fun SvgElement.line(init: LinePainter.() -> Unit): LinePainter {
    val line = create(init, this)
    this.add(line)
    return line
}
