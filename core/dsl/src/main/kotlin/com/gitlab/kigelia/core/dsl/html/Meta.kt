package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.common.Tag

class Meta: Tag("meta") {


    val name = htmlProperty("name", "")
    val content = htmlProperty("name", "")

    override fun render(indent: Int): String {
        return """${indent.toTab()}<meta ${args()}>"""
    }
}

fun Tag.meta(init: Meta.() -> Unit) {
    val meta = Meta()
    meta.init()
    this.add(meta)
}
