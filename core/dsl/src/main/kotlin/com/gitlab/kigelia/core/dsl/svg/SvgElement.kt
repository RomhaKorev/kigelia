package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.dsl.svg.elements.polyline
import com.gitlab.kigelia.core.dsl.svg.elements.*
import com.gitlab.kigelia.core.geometry.*
import com.gitlab.kigelia.core.tools.utils.Logger

open class SvgElement(name: String, private val parent: SvgElement? = null): Tag(name) {
    val id = svgProperty("id", "")
    var transparent: Boolean = false

    fun transparent() {
        transparent = true
    }

    operator fun String.invoke(init: SvgElement.() -> Unit): SvgElement {
        fun SvgElement.andWarn(name: String, tag: String=name): SvgElement {
            Logger.warn("A $name should be built by using $tag {...} instead of \"$tag\"{...}")
            return this
        }
        when(this) {
            "g" -> return g(init).andWarn("group", "g")
            "line" -> return line(init).andWarn("line")
            "rect" -> return rect(init).andWarn("rectangle", "rect")
            "circle" -> return circle(init).andWarn("circle")
            "polyline" -> return polyline(init).andWarn("polyline")
            "text" -> return text(init).andWarn("text")
            "path" -> return path(init).andWarn("path")
        }
        val instance = element(this, this@SvgElement, init)
        add(instance)
        return instance
    }

    private fun svgProperty(name: String, value: String): SvgProperty {
        val property = SvgProperty(name, value)
        this.attributes.add(property)
        return property
    }

    fun findSibling(sourceId: String): SvgElement? {
        return flatten().find { it.id `is` sourceId } ?: this.parent?.findSibling(sourceId)
    }

    fun flatten(): List<SvgElement> {
        return this.children.filterIsInstance<SvgElement>().flatMap { it.flatten() } + this
    }

    open fun boundingRect(): Geometry {
        val childrenGeometries = children.filterIsInstance<SvgElement>().filter { !it.transparent }.map { it.geometry() }
        return childrenGeometries.fold(this.geometry()) { acc, elementGeometry -> acc + elementGeometry }
    }

    private val geometry = Geometry()
    open fun geometry(init: Geometry.() -> Unit) {
        geometry.init()
    }
    open fun geometry(other: Geometry) {
        geometry {
            x(other.x())
            y(other.y())
            width(other.width())
            height(other.height())
        }
    }

    fun geometry(rect: Rectangle) {
        geometry(Geometry(rect.topLeft.x, rect.topLeft.y, rect.size.width, rect.size.height))
    }

    open fun geometry(): Geometry {
        return geometry
    }

    fun String.top(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().top()
    }

    fun String.bottom(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().bottom()
    }

    fun String.right(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().right()
    }

    fun String.left(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().left()
    }

    fun String.valueOf(attribute: String): String {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.attribute(attribute).value()
    }

    fun String.geometry(): Geometry {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry()
    }

    fun resizeLike(id: String) {
        val otherGeometry = id.geometry()
        geometry {
            width(otherGeometry.width())
            height(otherGeometry.height())
        }
    }

    fun Number.below(id: String): Point {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val offset = this.toDouble()
        val y = offset + siblingGeometry.bottom().y
        return Point(geometry().x(), y)
    }

    fun Number.above(id: String): Point {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val offset = this.toDouble()
        val y = siblingGeometry.top().y - geometry().height() - offset
        return Point(geometry().x(), y)
    }

    fun Number.leftOf(id: String): Point {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val offset = this.toDouble()
        val x = siblingGeometry.left().x - geometry().width() - offset
        return Point(x, geometry().y())
    }

    fun Number.rightOf(id: String): Point {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val asDouble = this.toDouble()
        val x = asDouble + siblingGeometry.right().x
        return Point(x, geometry().y())
    }

    fun placeAt(destination: Point): Translation {
        val source = geometry().position()
        geometry {
            position(destination)
        }
        return Translation(source, destination)
    }

    fun fill(color: Any) {
        "fill"(color.toString())
    }

    fun noFill() {
        "fill"("none")
    }

    fun stroke(color: Any) {
        "stroke"(color.toString())
    }

    fun noStroke() {
        "stroke"("none")
    }

    fun Ellipse.draw(init: SvgElement.() -> Unit = {}): SvgElement {
        return if (size.isSquare()) {
            circle {
                cx(center.x)
                cy(center.y)
                r(size.width / 2.0)
                init()
            }
        } else {
            ellipse {
                cx(center.x)
                cy(center.y)
                rx(size.width / 2.0)
                ry(size.height / 2.0)
                init()
            }
        }
    }

    fun Rectangle.draw(init: SvgElement.() -> Unit = {}): SvgElement {
        return rect {
            x(topLeft.x)
            y(topLeft.y)
            width(size.width)
            height(size.height)
            init()
        }
    }

    fun Line.draw(init: SvgElement.() -> Unit = {}): SvgElement {
        val self = this
        return line {
            p1(self.p1)
            p2(self.p2)
            init()
        }
    }

    fun Polygon.draw(closed: Boolean = true, init: SvgElement.() -> Unit = {}): SvgElement {
        val self = this
        return if (closed) {
            polygon {
                points(*self.points.toTypedArray())
                init()
            }
        } else {
            polyline {
                points(*self.points.toTypedArray())
                init()
            }
        }


    }

    override fun args(): String {
        return listOf(geometry().toAttributes().joinToString(" ") { it.render() }, super.args()).filter { it.isNotBlank() }.joinToString(" ")
    }

    protected open fun Geometry.toAttributes(): List<Attribute> {
        return listOf(
            SvgProperty("x", xOrNull()),
            SvgProperty("y", yOrNull()),
            SvgProperty("width", widthOrNull()),
            SvgProperty("height", heightOrNull())
        ).filter { it.valid() }
    }
}
