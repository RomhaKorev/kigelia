package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.common.Tag

class Link: Tag("link") {
    val rel = htmlProperty("rel", "")
    val href = htmlProperty("href", "")

    private var isCrossOrigin: Boolean = false

    fun crossOrigin() {
        isCrossOrigin = true
    }

    override fun render(indent: Int): String {
        val crossOrigin = if (isCrossOrigin) " crossorigin" else ""
        return """${indent.toTab()}<link ${args()}$crossOrigin>"""
    }
}


fun Tag.link(init: Link.() -> Unit) {
    val link = Link()
    link.init()
    this.add(link)
}
