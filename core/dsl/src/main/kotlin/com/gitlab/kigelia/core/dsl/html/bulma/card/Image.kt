package com.gitlab.kigelia.core.dsl.html.bulma.card

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.elements.figure
import com.gitlab.kigelia.core.dsl.html.elements.img

class Image internal constructor(): HtmlElement("div") {
    var src = ""
    var alt = ""
    var caption = ""
    var content = HtmlElement("")
    fun fillContent() {
        val f : HtmlElement.() -> Unit = {
            figure {
                cssClass("image", "is-4by3")
                if (caption.isNotBlank())
                    caption(caption)
                img(src) {
                    if (alt.isNotBlank())
                        alt(alt)
                }
            }
        }
        children.remove(content)
        content = HtmlElement("")
        content.f()
        add(content)
    }

    init {
        cssClass("card-image")
    }

    override fun visible(): Boolean = src.isNotBlank()

    fun alt(value: String) {
        alt = value
        fillContent()
    }

    fun caption(value: String) {
        caption = value
        fillContent()
    }
}