package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.SvgProperty
import com.gitlab.kigelia.core.geometry.Ellipse
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Size

class EllipsePainter(parent: SvgElement): SvgElement("ellipse", parent=parent) {
    private var center: Point = Point(0, 0)
    private var size: Size = Size(0, 0)

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf(SvgProperty("cx", center().x),
                      SvgProperty("cy", center().y),
                      SvgProperty("rx", width() / 2.0),
                      SvgProperty("ry", height() / 2.0),
            )
    }

    fun center(p: Point) = at(p.x, p.y)
    fun size(s: Size) = size(s.width, s.height)

    fun cx(value: Number) {
        geometry { x(value.toDouble() - width() / 2.0) }
        center = center.copy(x = value.toDouble() )
    }

    fun cy(value: Number) {
        geometry { y(value.toDouble() - height() / 2.0) }
        center = center.copy(y = value.toDouble() )
    }

    fun rx(value: Number) {
        val width = value.toDouble() * 2.0
        size = size.copy(width = width)
        geometry {
            width(width)
            x(center.x - value.toDouble())
        }
    }

    fun ry(value: Number) {
        val height = value.toDouble() * 2.0
        size = size.copy(height = height)
        geometry {
            height(height)
            y(center.y - value.toDouble())
        }
    }

    fun at(x: Number, y: Number) {
        cx(x)
        cy(y)
        center = Point(x, y)
    }

    fun size(width: Number, height: Number) {
        rx(width)
        ry(height)
        size = Size(width, height)
    }

    override fun equals(other: Any?): Boolean {
        return other is EllipsePainter && this.center == other.center && this.size == other.size
    }

    override fun hashCode(): Int {
        var result = center.hashCode()
        result = 31 * result + size.hashCode()
        return result
    }


}

private fun create(init: EllipsePainter.() -> Unit, parent: SvgElement): EllipsePainter {
    val element = EllipsePainter(parent)
    element.init()
    return element
}

fun SvgElement.ellipse(init: EllipsePainter.() -> Unit): EllipsePainter {
    val element = create(init, this)
    this.add(element)
    return element
}
