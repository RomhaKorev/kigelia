package com.gitlab.kigelia.core.dsl.common

class TextContent(val value: String): Renderer() {
    override fun render(indent: Int): String {
        return indent.toTab() + value
    }
}
