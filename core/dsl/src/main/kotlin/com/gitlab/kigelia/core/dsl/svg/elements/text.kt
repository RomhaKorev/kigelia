package com.gitlab.kigelia.core.dsl.svg.elements

import com.gitlab.kigelia.core.dsl.common.Attribute
import com.gitlab.kigelia.core.dsl.common.TextContent
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.SvgProperty
import com.gitlab.kigelia.core.geometry.Point

class Text(parent: SvgElement? = null): SvgElement("text", parent=parent) {

    private var position : Point = Point(0, 0)
    private var text: String = ""

    private var x: Double? = null
    private var y: Double? = null
    fun x(value: Number) {
        x = value.toDouble()
        position = position.copy(x = value.toDouble())
    }

    fun y(value: Number) {
        y = value.toDouble()
        position = position.copy(y = value.toDouble())
    }

    override fun addText(text: String) {
        this.text = text
        super.addText(text)
    }

    override fun args(): String {
        val l = mutableListOf<SvgProperty>()
        l.add(SvgProperty("x", x ?: geometry().xOrNull()))
        l.add(SvgProperty("y", y ?: geometry().yOrNull()))
        return listOf(l.filter { it.valid() }.joinToString(" ") { it.render() }, super.args()).filter { it.isNotBlank() }.joinToString(" ")
    }
    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    override fun equals(other: Any?): Boolean {
        return other is Text && (position == other.position) && text == other.text
    }

    override fun hashCode(): Int {
        var result = position.hashCode()
        result = 31 * result + text.hashCode()
        return result
    }


}

fun SvgElement.text(init: Text.() -> Unit): Text {
    val element = Text(this)
    element.init()
    this.add(element)
    return element
}
