package com.gitlab.kigelia.core.dsl.html

import com.gitlab.kigelia.core.dsl.common.Tag

open class HtmlElement(name: String, inlineContent: Boolean = false): Tag(name, inlineContent) {
    operator fun String.invoke(init: HtmlElement.() -> Unit): HtmlElement {
        val instance = element(this, init)
        add(instance)
        return instance
    }

    fun cssClass(vararg classes: String) {
        val existingClasses = attributeOr("class", "").value()
        val allClasses = listOf(existingClasses, *classes).filter { it.isNotBlank() }.joinToString(" ")
        attribute("class", allClasses)
    }

    fun removeCssClass(vararg classes: String) {
        val existingClasses = attributeOr("class", "").value()
        val remainingClasses = existingClasses.split(" ").filter { !classes.contains(it) }.joinToString(" ")
        attribute("class", remainingClasses)
    }
}

open class HtmlFrame(name: String): HtmlElement(name) {
    override fun visible(): Boolean = children.isNotEmpty()
}