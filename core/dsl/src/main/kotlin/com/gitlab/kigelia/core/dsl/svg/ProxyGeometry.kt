package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.geometry.Offset
import com.gitlab.kigelia.core.geometry.Point

class ProxyGeometry(private val parent: SvgElement): Geometry {
    override fun valid(): Boolean = parent.geometry().valid()
    override fun x(value: Number) = parent.geometry().x(value)
    override fun x(): Double = parent.geometry().x()
    override fun y(value: Number) = parent.geometry().y(value)

    override fun y(): Double = parent.geometry().y()
    override fun width(value: Number) = parent.geometry().width(value)
    override fun width(): Double = parent.geometry().width()
    override fun height(value: Number) = parent.geometry().height(value)
    override fun height(): Double = parent.geometry().height()

    override fun xOrNull(): Double? = parent.geometry().xOrNull()
    override fun yOrNull(): Double? = parent.geometry().yOrNull()
    override fun widthOrNull(): Double? = parent.geometry().widthOrNull()
    override fun heightOrNull(): Double? = parent.geometry().heightOrNull()

    override fun plus(other: Geometry): Geometry {
        return this
    }

    override fun plus(other: Offset): Geometry {
        return this
    }

    override fun plus(translation: Point): Geometry {
        return this
    }
}
