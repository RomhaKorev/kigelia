package com.gitlab.kigelia.core.dsl.svg

import com.gitlab.kigelia.core.dsl.common.Attribute

class SvgProperty(name: String, private var value: Any?): Attribute(name) {
    override fun render(): String {
        return "$name=\"${value()}\""
    }

    override fun value(): String {
        val s = value.toString()

        return s.replace("(-?\\d+)\\.0([\\D]|$)".toRegex(), "$1$2")
    }

    override fun valid(): Boolean {
        return value != null && value.toString() != ""
    }

    operator fun invoke(value: Any): SvgProperty {
        this.value = value
        return this
    }

    infix fun `is`(value: String): Boolean {
        return this.value.toString() == value
    }
}
