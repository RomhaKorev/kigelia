package com.gitlab.kigelia.core.dsl.common

import com.gitlab.kigelia.core.geometry.Point
import kotlin.math.pow
import kotlin.math.sqrt


data class Segment(private val p1: Point, private val p2: Point) {
    fun pointAt(ratio: Double): Point {
        val a = ratio
        val b = 1 - a
        val x = (b * p1.x + a * p2.x)
        val y = (b * p1.y + a * p2.y)
        return Point(x, y)
    }

    fun length(): Double {
        return sqrt((p2.x - p1.x).pow(2.0) + (p2.y - p1.y).pow(2.0))
    }

    fun pointAtPointFromEnd(distance: Double): Point {
        val ratio = (length() - distance) / length()
        return pointAt(ratio)
    }

    fun pointAtPointFromStart(distance: Double): Point {
        val ratio = distance / length()
        return pointAt(ratio)
    }
}
