package com.gitlab.kigelia.core.dsl.html.bulma.card

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.html.HtmlFrame
import com.gitlab.kigelia.core.dsl.html.elements.p

class Media internal constructor(): HtmlFrame("div") {
    var title = ""
    var subtitle = ""
    var left: HtmlElement.() -> Unit = {}

    var content = HtmlFrame("div")
    var mediaLeft = HtmlFrame("div")

    fun fillContent() {
        val f : HtmlElement.() -> Unit = {
            cssClass("media-left")
            left()
        }
            /*
            <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                            </figure>
                        </div>
             */
        val g : HtmlElement.() -> Unit = {
            cssClass("media-content")
            if (title.isNotBlank()) {
                p {
                    cssClass("title", "is-4")
                    -title
                }
            }
            if (subtitle.isNotBlank()) {
                p {
                    cssClass("subtitle", "is-6")
                    -subtitle
                }
            }
        }
        children.remove(mediaLeft)
        children.remove(content)
        mediaLeft = HtmlFrame("div")
        content = HtmlFrame("div")
        mediaLeft.f()
        content.g()
        add(mediaLeft)
        add(content)
    }

    init {
        cssClass("media")
    }

    fun title(value: String) {
        title = value
        fillContent()
    }

    fun subtitle(value: String) {
        subtitle = value
        fillContent()
    }

    fun leftMedia(value: HtmlElement.() -> Unit) {
        left = value
        fillContent()
    }
}