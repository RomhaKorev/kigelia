package com.gitlab.kigelia.core.dsl.svg.helpers.connectors

import com.gitlab.kigelia.core.geometry.Anchor
import com.gitlab.kigelia.core.dsl.common.CssStyle
import com.gitlab.kigelia.core.dsl.common.Segment
import com.gitlab.kigelia.core.dsl.common.TextContent
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Geometry
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.element
import com.gitlab.kigelia.core.dsl.svg.orphan

abstract class Connector(protected val source: Anchor, protected val destination: Anchor, protected val endingSize: Double = 10.0): SvgElement("") {

    lateinit var connector : SvgElement
    private val endpoints: MutableList<SvgElement> = mutableListOf()
    private var text: SvgElement? = null

    override fun style(init: CssStyle.() -> Unit) = connector.style(init)

    override fun boundingRect(): Geometry {
        return geometry()
    }

    fun text(text: String) {
        val center = boundingRect().center()
        this.text = element("text", this) {
            geometry {
                x(center.x)
                y(center.y)
            }
            "text-anchor"("middle")
            "dominant-baseline"("middle")
            - text
        }
        children.add(this.text!!)
    }


    private fun String.simplified() = this.replace("(\\d+)\\.0+(\\D)".toRegex(), "$1$2")

    fun applyEndings(starting: Marker, ending: Marker) {
        if (ending == Marker.None && starting == Marker.None) {
            return
        }
        val uuid = Identifier.unique()
        val arrows = orphan("defs") {
            if (ending == Marker.Arrow) {
                applyArrowAtEnd(uuid)
                val end = "marker" {
                    id("endarrow-$uuid")
                    "markerWidth"(endingSize)
                    "markerHeight"(0.7 * endingSize)
                    "refX"(0)
                    "refY"(endingSize * 0.35)
                    "orient"("auto")
                    "markerUnits"("strokeWidth")
                    "polygon" {
                        "points"("0 0, $endingSize ${0.35 * endingSize}, 0 ${0.7 * endingSize}".simplified())
                    }
                }
                endpoints.add(end)
            }
            if (starting == Marker.Arrow) {
                applyArrowAtStart(uuid)
                val start = "marker" {
                    id("startarrow-$uuid")
                    "markerWidth"(endingSize)
                    "markerHeight"(0.7 * endingSize)
                    "refX"(endingSize)
                    "refY"(endingSize * 0.35)
                    "orient"("auto")
                    "polygon" {
                        "points"("$endingSize 0, $endingSize ${0.7 * endingSize}, 0 ${0.35 * endingSize}".simplified())
                    }
                }
                endpoints.add(start)
            }
        }
        this.children.add(0, arrows)
    }

    protected abstract fun applyArrowAtEnd(identifier: Int)
    protected abstract fun applyArrowAtStart(identifier: Int)

    protected fun SvgElement.toSegment(): Segment {
        val x1 = this.attribute("x1").value().toDouble()
        val y1 = this.attribute("y1").value().toDouble()
        val x2 = this.attribute("x2").value().toDouble()
        val y2 = this.attribute("y2").value().toDouble()

        return Segment(Point(x1, y1), Point(x2, y2))
    }

    fun thickness(value: Number) {
        val applyToElements: SvgElement.() -> Unit = {
            "stroke-width"(value)
        }
        this.connector.applyToElements()
    }

    fun color(color: String) {
        val applyToElements: SvgElement.() -> Unit = {
            "stroke"(color)
        }

        val applyToEndpoint: SvgElement.() -> Unit = {
            "stroke"(color)
            "fill"(color)
        }

        if (text != null) {
            val applyToText: SvgElement.() -> Unit = {
                "fill"(color)
            }
            this.text!!.applyToText()
        }

        this.connector.applyToElements()
        this.endpoints.forEach {
            it.applyToEndpoint()
        }
    }

    override operator fun String.invoke(value: Any) {
        val self = this
        val applyToElements: SvgElement.() -> Unit = {
           self(value)
        }
        connector.applyToElements()
    }
}
