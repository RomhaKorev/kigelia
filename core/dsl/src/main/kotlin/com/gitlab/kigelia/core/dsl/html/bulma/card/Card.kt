package com.gitlab.kigelia.core.dsl.html.bulma.card

import com.gitlab.kigelia.core.dsl.html.HtmlElement


class CardContent: HtmlElement("div") {
    var media = Media()
    var body = object: HtmlElement("div") {
        override fun visible(): Boolean {
            return children.fold(false) { acc, renderer -> acc || renderer.visible() }
        }
    }
    init {
        cssClass("card-content")
        body.cssClass("content")
        add(media)
        add(body)
    }

    override fun visible(): Boolean {
        return children.fold(false) { acc, renderer -> acc || renderer.visible() }
    }

    fun title(value: String) {
        media.title(value)
    }

    fun subtitle(value: String) {
        media.subtitle(value)
    }

    fun body(f: HtmlElement.() -> Unit) {
        body.f()
    }

    fun leftMedia(value: HtmlElement.() -> Unit) {
        media.leftMedia(value)
    }
}

class Card: HtmlElement("div") {
    var content = CardContent()
    var image = Image()
    init {
        cssClass("card")
        add(image)
        add(content)
     }

    fun image(src: String, f: Image.() -> Unit) {
        image.src = src
        image.f()
        image.fillContent()
    }

    fun title(value: String) {
        content.title(value)
    }

    fun subtitle(value: String) {
        content.subtitle(value)
    }

    fun leftMedia(value: HtmlElement.() -> Unit) {
        content.leftMedia(value)
    }


    fun content(f: HtmlElement.() -> Unit) {
        content.body(f)
    }
}