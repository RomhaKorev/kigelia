package com.gitlab.kigelia.core.dsl.html.bulma.columns

import com.gitlab.kigelia.core.dsl.html.HtmlElement

class Columns: HtmlElement("div") {
    init {
        cssClass("columns")
    }

    fun column(f: Column.() -> Unit): HtmlElement {
        val e = Column()

        e.f()
        add(e)
        return e
    }
}

