package com.gitlab.kigelia.core.dsl.common

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.svg.SvgProperty


abstract class Renderer {
    private var zIndex: Int = 0
    private var visible: Boolean = true

    fun z(): Int {
        return zIndex
    }

    fun z(index: Int) {
        zIndex = index
    }

    abstract fun render(indent: Int=0): String
    open fun visible(): Boolean { return visible }
    fun hideIf(predicate: Boolean) {
        visible = !predicate
    }
}

open class Tag(private val tag: String, private val inlineContent: Boolean = false): Renderer() {
    protected val attributes = mutableListOf<Attribute>()
    protected val children = mutableListOf<Renderer>()

    fun forEach(f: (Renderer) -> Unit) = children.forEach(f)
    fun add(element: Renderer) = children.add(element)
    fun add(element: Attribute) = attributes.add(element)

    open fun style(init: CssStyle.() -> Unit) {
        val style = CssStyle()
        style.init()
        attributes.add(style)
    }

    fun attribute(name: String, value: Any) {
        val attr = attributes.filterIsInstance<SvgProperty>().find { it.name == name }
        if (attr == null) {
            attributes.add(SvgProperty(name, value))
            return
        }
        attr(value)
    }

    fun attribute(name: String): Attribute {
        return attributes.filterIsInstance<SvgProperty>().find { it.name == name }
            ?: throw IllegalArgumentException("Argument $name does not exist")
    }

    fun attributeOr(name: String, defaultValue: String): Attribute {
        val u = attributes.filterIsInstance<SvgProperty>().find { it.name == name }
        if (u == null)
            attribute(name, defaultValue)
        return attribute(name)
    }

    fun hasAttribute(name: String): Boolean {
        return attributes.filterIsInstance<SvgProperty>().find { it.name == name } != null
    }

    fun removeAttribute(name: String) {
        attributes.removeIf { it.name == name }
    }

    open operator fun String.invoke(value: Any) {
        attribute(this, value)
    }

    infix fun String.ifNotPresent(value: Any) {
        if (!hasAttribute(this)) {
            attribute(this, value)
        }
    }

    protected open fun args(): String {
        return attributes.filter { it.valid() }.joinToString(" ") { it.render() }
    }

    protected fun Int.toTab(): String {
        return "    ".repeat(this)
    }

    protected fun String.toTag(args: String, indent: Int, content: () -> String): String {
        val inner = content()
        val inlineArgs = if (args.isBlank()) "" else " $args"
        if (inner == "") {
            return indent.toTab() + "<$this$inlineArgs />"
        }

        if (inlineContent)
            return indent.toTab() + "<$this$inlineArgs>${inner}</$this>"
        return indent.toTab() + "<$this$inlineArgs>\n${inner}\n" + indent.toTab() +  "</$this>"
    }


    override fun render(indent: Int): String {
        if (tag == "") {
            return this.children.sortedBy { it.z() }.filter { it.visible() }.joinToString("\n") { it.render(indent) }
        }
        return tag.toTag(args(), indent) {
            this.children.sortedBy { it.z() }.filter { it.visible() }.joinToString("\n") {
                if (inlineContent)
                    it.render(0)
                else
                    it.render(indent + 1)
            }
        }
    }

    open operator fun String.unaryMinus() {
        addText(this)
    }

    protected open fun addText(text: String) {
        children.removeIf { it is TextContent }
        children.add(TextContent(text))
    }

    open operator fun String.unaryPlus() {
        val text = (children.firstOrNull { it is TextContent } as TextContent?)?.value ?: ""
        children.removeIf { it is TextContent }
        children.add(TextContent(listOf(text, this).joinToString(" ")))
    }
}

fun Int.toTab(): String {
    return "    ".repeat(this)
}
