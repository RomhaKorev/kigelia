package com.gitlab.kigelia.graphs

import com.gitlab.kigelia.core.geometry.DPoint
import com.gitlab.kigelia.core.geometry.Position
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.sqrt

fun interface Constraints {
    fun apply(node: Electron<*>, all: List<Electron<*>>)
}



data class Electron<T>(val value: T, var position: Position, val edges: MutableList<Electron<T>>)

class Algorithm<T>(graph: Graph<T>) {
    private val electrons: List<Electron<T>>

    val nodes: List<Node<T>>
        get() = electrons.map { Node(it.value, it.position) }

    private var distance: Double = 0.0

    init {
        val nbRows = floor(sqrt(graph.values.size + 0.0)).toInt()

        var x = 10
        var y = 10
        var i = 0
        val positionOf : (index: Int) -> Position = {
            _ ->
            val p = Position(x, y)
            x += 70
            ++i
            if ( i > nbRows ) {
                x = 10
                y += 40
                i = 0
            }
            p
        }

        electrons = graph.values.sortedByDescending {
            graph.edges.count { e -> e.first == it }
        }.mapIndexed { index, e -> Electron(e, positionOf(index), mutableListOf()) }
        graph.edges.forEach {edge ->
            val left = electrons.first { it.value == edge.first }
            val right = electrons.first { it.value == edge.second }
            if (left !=  right) {
                left.edges.add(right)
                right.edges.add(left)
            }
        }
    }

    fun run(vararg constraints: Constraints) {
        do {
            electrons.forEach { applyForcesOn(it, constraints.toList()) }
            var p1 = electrons.first().position
            val d = electrons.drop(1).map { it.position }.fold(0.0) { acc, p ->
                val r = acc + p1.distance(p)
                p1= p
                r
            }
            if ((abs(distance - d) > 0.0001)) {
                distance = d
            } else {
                break
            }
        } while(true)
    }

    private fun applyForcesOn(electron: Electron<T>, constraints: List<Constraints>) {
        var velocity = Position(0, 0)
        electrons.filter { electron != it }.forEach { other ->
            val vector = electron.position - other.position
            val dx = vector.x
            val dy = vector.y
            val l = 2.0 * (dx * dx + dy * dy)
            if (l > 0) {
                velocity += DPoint((dx * 500.0) / l, (dy * 500.0) / l)
            }
        }

        /* Calculate forces pulling items relative to their weight */
        val weight = ( electron.edges.size + 1 ) * weight
        electron.edges.forEach {other ->
            val vector = electron.position - other.position
            velocity -= DPoint(vector.x / weight, vector.y / weight)
        }

        if (abs(velocity.x) < 0.1 && (abs(velocity.y) >= 0.1)) {
            return
        }
        electron.position += velocity.toDPoint()
        constraints.forEach { it.apply(electron, electrons) }
    }

    companion object {
        private const val weight = 9.81
    }
}


fun String.toGraph(): Graph<String> {
    val graph = Graph<String>()
    val lines = this.trimIndent().split("\n")
    lines.flatMap {
        it.split(" -> ").flatMap { n -> n.split(", ") }
    }.distinct().sorted().forEach { graph.with(it) }

    lines.forEach {
        val (src, dst) = it.split(" -> ")
        dst.split(", ").forEach {
                d -> graph.connect(src, d)
        }
    }
    return graph
}