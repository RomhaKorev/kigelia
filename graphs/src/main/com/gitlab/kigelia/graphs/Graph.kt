package com.gitlab.kigelia.graphs

import com.gitlab.kigelia.core.geometry.Position

class Graph<T> private constructor(val values: MutableList<T>, val edges: MutableList<Pair<T, T>>) {
    constructor(): this(mutableListOf(), mutableListOf())

    fun with(value: T): Graph<T> {
        values.add(value)
        return this
    }

    fun connect(left: T, right: T): Graph<T> {
        edges.add(left to right)
        return this
    }

    fun reorder(): Representation<T> {
        return Representation(values.map {
            Node(
                it,
                Position(0, 0)
            )
        }, edges)
    }
}
