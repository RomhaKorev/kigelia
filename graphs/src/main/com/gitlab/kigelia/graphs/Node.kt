package com.gitlab.kigelia.graphs

import com.gitlab.kigelia.core.geometry.Position

data class Node<T>(val value: T, val position: Position)
