package com.gitlab.kigelia.graphs

data class Representation<T>(val nodes: List<Node<T>>, val edges: List<Pair<T, T>>)