package com.gitlab.kigelia.graphs

import com.gitlab.kigelia.core.geometry.Position
import com.gitlab.kigelia.core.dsl.common.Tag
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

fun Tag.saveAs(fileName: String) {
    val myfile = File(fileName)
    myfile.printWriter().use { out ->
        out.println(this.render())
    }
}

class GraphTest {

    @Test
    fun `should build a graph with a single node`() {
        val graph = Graph<String>().with("node")
        assertThat(graph.reorder()).isEqualTo(Representation(listOf(Node("node", Position(0, 0))), emptyList()))
    }

    @Test
    fun `should connect two nodes`() {
        val graph = Graph<String>().with("node 1").with("node 2").connect("node 1", "node 2")
        assertThat(graph.reorder().nodes.map { it.value }).isEqualTo(listOf("node 1", "node 2"))
        assertThat(graph.reorder().edges).isEqualTo(listOf("node 1" to "node 2"))
    }

    @Test
    fun `should place all nodes`() {
        val graph = Graph<String>().with("node 1").with("node 2").connect("node 1", "node 2")
        assertThat(graph.reorder().nodes.map { it.value }).isEqualTo(listOf("node 1", "node 2"))
        assertThat(graph.reorder().edges).isEqualTo(listOf("node 1" to "node 2"))
    }
}
