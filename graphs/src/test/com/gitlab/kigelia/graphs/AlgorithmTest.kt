package com.gitlab.kigelia.graphs

import com.gitlab.kigelia.core.geometry.Position
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.math.abs

class AlgorithmTest {
    @Test
    fun `forces on a single node should be null`() {
        val graph = Graph<String>().with("node")
        val algorithm = Algorithm(graph)
        algorithm.run()
        Assertions.assertThat(algorithm.nodes).isEqualTo(listOf(Node("node", Position(10, 10))))
    }

    fun Node<String>.noNodeWithin(others: List<Node<String>>, distance: Double): Boolean {
        return others.minOf { abs(it.position.distance(position)) } > distance
    }

    @Test
    fun `a node should push its neighbour`() {

        val graph = """
            A -> B
            B -> D, F, C
            C -> E
            F -> C
            D -> C
        """.toGraph()

        val algorithm = Algorithm(graph)
        algorithm.run()

        algorithm.nodes.forEach {node ->
            Assertions.assertThat(node.noNodeWithin(algorithm.nodes.filter { it != node }, 50.0)).isTrue
        }
    }
}