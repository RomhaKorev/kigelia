package com.gitlab.kigelia.indicators

import com.gitlab.kigelia.core.dsl.common.AltColors
import com.gitlab.kigelia.core.dsl.common.Color
import com.gitlab.kigelia.core.dsl.common.Tag
import com.gitlab.kigelia.core.geometry.Ellipse
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.Direction
import com.gitlab.kigelia.core.dsl.svg.elements.path
import com.gitlab.kigelia.core.dsl.svg.svg
import java.io.File


fun Tag.saveAs(fileName: String) {
    val myfile = File(fileName)
    myfile.printWriter().use { out ->
        out.println(this.render())
    }
}

class ActivityRing(parent: SvgElement): SvgElement("g", parent=parent) {
    var progress = 0.0
    var endAngles = -30.0
}

fun SvgElement.activityRing(init: ActivityRing.() -> Unit): ActivityRing {
    val element = ActivityRing(this)
    element.init()

    val geometry = element.geometry()
    val center = Point(geometry.width() / 2.0, geometry.height() / 2.0)
    val outerEllipse = Ellipse(center= center,
                               size= Size(geometry.width(), geometry.height())
    )
    val innerEllipse = Ellipse(center= center,
                               size= Size(geometry.width() * 0.8, geometry.height() * 0.8)
    )

    val capSize = Size(geometry.width() * 0.1, geometry.height() * 0.1)
    val cap = Ellipse(center= center,
                      size= Size(geometry.width() * 0.9, geometry.height() * 0.9)
    )

    val build: SvgElement.()  -> Unit = {
        val start = element.endAngles
        val end = 180.0 - element.endAngles
        val startProgress = (1.0 - element.progress) * (end - start) + start

        val capEndEllipse = Ellipse(cap.pointAtPercent(start / 360.0), capSize)
        val progressEndEllipse = Ellipse(cap.pointAtPercent(startProgress / 360.0), capSize)

        path {
            "stroke"("none")
            "fill"(Color.LightGray)
            "fill-rule"("evenodd")

            arcMoveTo(outerEllipse, startAngle=start, spanAngle = end - start)
            arcTo(capEndEllipse.size, startAngle=end, spanAngle = 180.0)
            arcTo(innerEllipse.size, startAngle=end, spanAngle = end - start, direction = Direction.Clockwise)
            arcMoveTo(capEndEllipse, startAngle=start, spanAngle = 180.0, direction = Direction.Clockwise)
        }

        path {
            "stroke"("none")
            "fill"(AltColors.Blue)
            "fill-rule"("evenodd")

            arcMoveTo(outerEllipse, startAngle=startProgress, spanAngle = end - startProgress)
            arcTo(capEndEllipse.size, startAngle=end, spanAngle = 180.0)
            arcTo(innerEllipse.size, startAngle=end, spanAngle = end - startProgress, direction = Direction.Clockwise)
            arcMoveTo(progressEndEllipse, startAngle=startProgress, spanAngle = 180.0, direction = Direction.Clockwise)
        }

        path {
            "fill"("none")
            "stroke"("#F0F0F0")
            "stroke-width"(2)
            "stroke-linecap"("round")
            "stroke-linejoin"("round")

            arcMoveTo(outerEllipse, startAngle=start, spanAngle = end - start)
            arcTo(capEndEllipse.size, startAngle=end, spanAngle = 180.0)
            arcMoveTo(innerEllipse, startAngle=end, spanAngle = end - start, direction = Direction.Clockwise)
            arcMoveTo(capEndEllipse, startAngle=start, spanAngle = 180.0, direction = Direction.Clockwise)
        }
    }

    element.build()

    this.add(element)
    return element
}

fun main() {
    svg {
        viewbox {
            minx(-10)
            miny(-10)
            width(120)
            height(120)
        }

        geometry {
            width(120)
            height(120)
        }

        val angle = 37.0

        val e = Ellipse(Point(50.0, 50.0), Size(80.0, 80.0))
        val p1 = e.pointAtPercent(angle / 360.0)

        path {
            "stroke"("red")
            move(100, 50)
            lineTo(p1)
            //arcTo(Size(80, 80), angle, 270.0)
            arcMoveTo(
                e,
                angle, 270.0
            )
            lineTo(100, 50)
        }


    }.saveAs("./gauge.svg")
    //activityRing()
}
