package com.gitlab.kigelia.charts.linechart

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


data class Tick(val x: Double) {
    companion object {
        val NONE = Tick(Double.NaN)
    }
}
class HorizontalAxis(val minimum: Double, val maximum: Double) {
    var currentTick = minimum
    constructor(minimum: Number, maximum: Number): this(minimum.toDouble(), maximum.toDouble())

    val tick: Tick
        get() {
            val t = Tick(currentTick)
            currentTick += 1
            return t
        }

}

class HorizontalAxisTest {

    @Test
    fun `should give the first tick`() {
        val axis = HorizontalAxis(0, 10)
        assertThat(axis.tick).isEqualTo(Tick(0.0))
    }

    @Test
    fun `should give the next tick until end`() {
        val axis = HorizontalAxis(0, 10)

        assertThat(axis.tick).isEqualTo(Tick(0.0))
    }
}
