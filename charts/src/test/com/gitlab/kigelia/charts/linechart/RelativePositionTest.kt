package com.gitlab.kigelia.charts.linechart

import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.geometry.Point
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class RelativePositionTest {
    @Test
    fun `should give the absolute position`() {
        assertThat(RelativePosition(0.5, 0.2).toAbsolute(Size(100.0, 100.0)))
            .isEqualTo(Point(50.0, 80.0))
    }
}
