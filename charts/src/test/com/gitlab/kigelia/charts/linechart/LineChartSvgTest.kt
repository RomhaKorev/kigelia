package com.gitlab.kigelia.charts.linechart

import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.LinePainter
import com.gitlab.kigelia.core.dsl.svg.orphan
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

fun SvgElement.the_line(p1: Point, p2: Point) {
    val lines = this.flatten().filterIsInstance<LinePainter>()
    assertThat(lines.first { it.p1 == p1 && it.p2 == p2 }).isNotNull
}

fun LinePainter.isThin(): Boolean
    = this.hasAttribute("stroke-width")  && this.attribute("stroke-width").value().toDouble() == 0.05

fun LinePainter.isNormal(): Boolean
        = !this.hasAttribute("stroke-width")


fun SvgElement.a_line_matching(predicate: (element: LinePainter) -> Boolean) {
    assertThat(this.flatten().filterIsInstance<LinePainter>().first { predicate(it) }).isNotNull
}

infix fun SvgElement.`should paint`(f: SvgElement.() -> Unit) {
    f()
}

class LineChartSvgTest {

    @Test
    fun `should draw axis`() {
        val size = Size(20.0, 30.0)
        val axis = LineChartAxis()
        axis.setLabels(IntRange(0, 10).toList())
        axis.resizeVertically(0.0, 10.0)

        orphan {
            drawAxis(size, axis, false)
        } `should paint` {
            the_line(Point(0.0, -5.0), Point(0.0, size.height))
            the_line(Point(0.0, size.height), Point(size.width + 5.0, size.height))

            IntRange(0, 10).map { it.toDouble() * 3 }.forEach {y ->
                a_line_matching { line ->
                    line.p1.y == y && line.p2.y == y
                    line.isThin()
                }
            }

            IntRange(0, 10).map { it.toDouble() * 2 }.forEach { x ->
                a_line_matching{ line ->
                    line.p1.x == x && line.p2.x == x
                    && line.isNormal()
                }
            }
        }
    }

    @Test
    fun `should draw labels on horizontal axis`() {
        val size = Size(20.0, 30.0)
        val axis = LineChartAxis()
        axis.setLabels(IntRange(0, 10).toList())
        axis.resizeVertically(0.0, 10.0)

        orphan {
            drawAxis(size, axis, false)
        } `should paint` {
            the_line(Point(0.0, -5.0), Point(0.0, size.height))
            the_line(Point(0.0, size.height), Point(size.width + 5.0, size.height))

            IntRange(0, 10).map { it.toDouble() * 3 }.forEach {y ->
                a_line_matching { line ->
                    line.p1.y == y && line.p2.y == y
                    line.isThin()
                }
            }

            IntRange(0, 10).map { it.toDouble() * 2 }.forEach { x ->
                a_line_matching{ line ->
                    line.p1.x == x && line.p2.x == x
                            && line.isNormal()
                }
            }
        }
    }

    @Test
    fun `should leave space for vertical axis labels`() {
        val width = 105.0
        val height = 105.0

        val u = LineChart().with("",
            listOf(
                Value("1", 1.0),
                Value("2", 4.0),
                Value("3", 2.0),
                Value("4", 5.0),
                Value("5", 3.0),
            )
        ).toSvg(Size(width, height))
    }

    @Test
    fun `should display a serie`() {
        val width = 155.0
        val height = 505.0

        val u = LineChart().with("",
            listOf(
                Value("1", 1.0),
                Value("2", 4.0),
                Value("a long label 3", 2.0),
                Value("4", 5.0),
                Value("5", 3.0),
            )
        ).toSvg(Size(width, height))

        assertThat(u.render())
            .isEqualTo("""
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="155" height="505" viewBox="0 0 155 505">
                    <g font-size="6" transform="translate(35, 5)">
                        <g stroke="#868686" stroke-width="0.25">
                            <line x1="0" y1="-5" x2="0" y2="57" />
                            <line x1="0" y1="57" x2="120" y2="57" />
                            <line x1="0" y1="55" x2="0" y2="59" />
                            <text x="0" y="65" stroke="none" text-anchor="end" transform="rotate(-30, 0, 61)">
                                1
                            </text>
                            <line x1="28.75" y1="55" x2="28.75" y2="59" />
                            <text x="28.75" y="65" stroke="none" text-anchor="end" transform="rotate(-30, 28.75, 61)">
                                2
                            </text>
                            <line x1="57.5" y1="55" x2="57.5" y2="59" />
                            <text x="57.5" y="65" stroke="none" text-anchor="end" transform="rotate(-30, 57.5, 61)">
                                a long label 3
                            </text>
                            <line x1="86.25" y1="55" x2="86.25" y2="59" />
                            <text x="86.25" y="65" stroke="none" text-anchor="end" transform="rotate(-30, 86.25, 61)">
                                4
                            </text>
                            <line x1="115" y1="55" x2="115" y2="59" />
                            <text x="115" y="65" stroke="none" text-anchor="end" transform="rotate(-30, 115, 61)">
                                5
                            </text>
                            <line x1="-2" y1="57" x2="2" y2="57" />
                            <line stroke="#BEBEBE" stroke-width="0.05" x1="0" y1="57" x2="120" y2="57" />
                            <text x="-5" y="57" text-anchor="end" stroke="none">
                                0.0
                            </text>
                            <line x1="-2" y1="45.6" x2="2" y2="45.6" />
                            <line stroke="#BEBEBE" stroke-width="0.05" x1="0" y1="45.6" x2="120" y2="45.6" />
                            <text x="-5" y="45.6" text-anchor="end" stroke="none">
                                0.2
                            </text>
                            <line x1="-2" y1="34.2" x2="2" y2="34.2" />
                            <line stroke="#BEBEBE" stroke-width="0.05" x1="0" y1="34.2" x2="120" y2="34.2" />
                            <text x="-5" y="34.2" text-anchor="end" stroke="none">
                                0.4
                            </text>
                            <line x1="-2" y1="22.800000000000004" x2="2" y2="22.800000000000004" />
                            <line stroke="#BEBEBE" stroke-width="0.05" x1="0" y1="22.800000000000004" x2="120" y2="22.800000000000004" />
                            <text x="-5" y="22.800000000000004" text-anchor="end" stroke="none">
                                0.6
                            </text>
                            <line x1="-2" y1="11.399999999999999" x2="2" y2="11.399999999999999" />
                            <line stroke="#BEBEBE" stroke-width="0.05" x1="0" y1="11.399999999999999" x2="120" y2="11.399999999999999" />
                            <text x="-5" y="11.399999999999999" text-anchor="end" stroke="none">
                                0.8
                            </text>
                            <line x1="-2" y1="0" x2="2" y2="0" />
                            <line stroke="#BEBEBE" stroke-width="0.05" x1="0" y1="0" x2="120" y2="0" />
                            <text x="-5" y="0" text-anchor="end" stroke="none">
                                1.0
                            </text>
                        </g>
                        <g stroke="#33c9e4" fill="none">
                            <path d="M0 45.6 L28.75 11.399999999999999 L57.5 34.2 L86.25 0 L115 22.800000000000004" />
                            <circle cx="0" cy="45.6" r="2" stroke-width="0.75" fill="white" />
                            <circle cx="28.75" cy="11.399999999999999" r="2" stroke-width="0.75" fill="white" />
                            <circle cx="57.5" cy="34.2" r="2" stroke-width="0.75" fill="white" />
                            <circle cx="86.25" cy="0" r="2" stroke-width="0.75" fill="white" />
                            <circle cx="115" cy="22.800000000000004" r="2" stroke-width="0.75" fill="white" />
                        </g>
                    </g>
                </svg>
            """.trimIndent())
    }

}
