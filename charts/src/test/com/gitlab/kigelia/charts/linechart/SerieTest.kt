package com.gitlab.kigelia.charts.linechart

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


class SerieTest {
    @Test
    fun `should provide markers for each value`() {

        val axis = LineChartAxis()
        axis.setLabels(IntRange(0, 2).toList())
        axis.resizeVertically(0.0, 100.0)

        assertThat(Serie("foobar", listOf(
                Value("Foo", 10.0),
                Value("Bar", 20.0),
                Value("Spam", 30.0)
        ), axis).markers.map { it.position }).isEqualTo(
                listOf(
                        RelativePosition(0.0, 0.1),
                        RelativePosition(0.5, 0.2),
                        RelativePosition(1.0, 0.3)
                )
        )
    }
}
