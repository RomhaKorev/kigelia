package com.gitlab.kigelia.charts.linechart

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LineChartAxisTest {
    @Test
    fun `should return the position of a value`() {
        val lineChartAxis = LineChartAxis()
        lineChartAxis.setLabels(listOf("l1", "l2", "l3", "l4", "l5"))
        lineChartAxis.resizeVertically(0.0, 10.0)

        val p = lineChartAxis.getPosition(1.0, 10.0)

        assertThat(p).isEqualTo(RelativePosition(1.0 / 4.0, 1.0))
    }
}