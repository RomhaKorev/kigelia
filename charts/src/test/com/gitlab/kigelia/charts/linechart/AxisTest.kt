package com.gitlab.kigelia.charts.linechart

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


class AxisTest {

    @Test
    fun `should give the position of a value in percentage`() {
        val axis = RangeAxis(-20, 20)

        assertThat(axis(-20.0)).isEqualTo(0.0)
        assertThat(axis(-10.0)).isEqualTo(0.25)
        assertThat(axis(0.0)).isEqualTo(0.5)
        assertThat(axis(10.0)).isEqualTo(0.75)
        assertThat(axis(20.0)).isEqualTo(1.0)
    }

    @Test
    fun `maximum should be greater than minimum`() {
        val axis = RangeAxis(20, -20)

        assertThat(axis.minimum()).isEqualTo(-20.0)
        assertThat(axis.maximum()).isEqualTo(20.0)
    }

    @Test
    fun `should update minimum when resizing`() {
        val axis = RangeAxis(-20, 20)

        axis.resize(-100.0, 20.0)

        assertThat(axis.minimum()).isEqualTo(-100.0)
        assertThat(axis.maximum()).isEqualTo(20.0)
    }

    @Test
    fun `should update maximum when resizing`() {
        val axis = RangeAxis(-20, 20)

        axis.resize(0.0, 100.0)

        assertThat(axis.minimum()).isEqualTo(-20.0)
        assertThat(axis.maximum()).isEqualTo(100.0)
    }

    @Test
    fun `should have tick based on interval`() {
        assertThat(RangeAxis(0, 10).tick).isEqualTo(1.0)

        assertThat(RangeAxis(0, 0.5).tick).isEqualTo(0.1)

        assertThat(RangeAxis(-5.0, 0.0).tick).isEqualTo(1.0)

        assertThat(RangeAxis(0, 100).tick).isEqualTo(10.0)
        assertThat(RangeAxis(0, 1).tick).isEqualTo(0.1)
        assertThat(RangeAxis(-100, 10).tick).isEqualTo(10.0)

        assertThat(RangeAxis(13, 59).tick).isEqualTo(10.0)
    }

    @Test
    fun `should force tick to given value`() {
        assertThat(RangeAxis(0, 10).tick(1.0).tick).isEqualTo(1.0)

        assertThat(RangeAxis(0, 100).tick(2.0).tick).isEqualTo(2.0)
        assertThat(RangeAxis(0, 1).tick(3.0).tick).isEqualTo(3.0)
        assertThat(RangeAxis(-100, 10).tick(4.0).tick).isEqualTo(4.0)

        assertThat(RangeAxis(13, 59).tick(5.0).tick).isEqualTo(5.0)
    }

    @Test
    fun `should return each label`() {
        val axis = LabelAxis()
        axis.labels = (IntRange(0, 10).map { "label $it" }.toList())

        assertThat(axis.minimum()).isEqualTo(0.0)
        assertThat(axis.maximum()).isEqualTo(10.0)

        IntRange(0, 10).forEach {
            assertThat(axis.labelAt(it)).isEqualTo("label $it")
        }

    }
}
