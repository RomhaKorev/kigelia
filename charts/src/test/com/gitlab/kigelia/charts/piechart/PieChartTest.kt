package com.gitlab.kigelia.charts.piechart

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class PieChartTest {
    @Test
    fun `should create a pie chart with only one value`() {
        val pieChart = PieChart(listOf(Value("Foo", 12)))
        assertEquals(
                listOf(Slice(Value("Foo", 12), Angle.inDegrees(0.0), Angle.inDegrees(360.0))),
                pieChart.slices()
        )
    }

    @Test
    fun `should create a pie chart with multiple value`() {
        val pieChart = PieChart(listOf(Value("Foo", 12), Value("Bar", 48)))
        assertEquals(
                listOf(
                        Slice(Value("Foo", 12), startAngle = Angle.inDegrees(0.0), spanAngle = Angle.inDegrees(72.0)),
                        Slice(Value("Bar", 48), startAngle = Angle.inDegrees(72.0), spanAngle = Angle.inDegrees(288.0))
                ),
                pieChart.slices()
        )
    }

    @Test
    fun `all parts should give a circle`() {
        val pieChart = PieChart(listOf(Value("Foo", 12), Value("Bar", 48), Value("Bar", 73)))
        assertEquals(
                Angle.inDegrees(360.0),
                pieChart.slices().last().endAngle
        )

        assertEquals(
                Angle.inDegrees(360.0),
                pieChart.slices().fold(Angle.inRadians(0)) { acc, v -> acc + v.spanAngle }
        )
    }
}
