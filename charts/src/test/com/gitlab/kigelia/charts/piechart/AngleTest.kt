package com.gitlab.kigelia.charts.piechart

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class AngleTest {

    @Test
    fun `should convert degrees to radians`() {
        assertEquals(
                Math.PI,
                Angle.inDegrees(180.0).radians
        )
    }

    @Test
    fun `should convert radians to degrees`() {
        assertEquals(
                180.0,
                Angle.inRadians(Math.PI).degrees
        )
    }
}
