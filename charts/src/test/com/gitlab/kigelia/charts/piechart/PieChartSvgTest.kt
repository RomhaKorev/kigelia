package com.gitlab.kigelia.charts.piechart

import com.gitlab.kigelia.core.dsl.common.AltColors
import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.dsl.svg.svg
import kotlin.test.Test
import kotlin.test.assertEquals

class PieChartSvgTest {

    @Test
    fun `should render a slice`() {
        val slice = Slice(Value("Foobar", 12), Angle.inDegrees(45.0), Angle.inDegrees(45.0))
        val parent = svg {}
        val render = slice.toSvg(parent, 200.0, AltColors.Blue)

        assertEquals(
                """<path d="M 141.4213562373095 141.42135623730948 A 200 200 0 0 1 1.2246467991473532E-14 200 L 0 0z" fill="#33c9e4" stroke="white" stroke-width="1" />""",
                render.render()
        )
    }

    @Test
    fun `should render a piechart`() {
        val render = PieChart(
                Value("Foo", 12),
                Value("Bar", 12),
                Value("Bar", 25)
        ).toSvg(Size(200.0, 200.0))

        assertEquals(
                """
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="204" height="204" viewBox="-78 -78 204 204">
                        <g transform="translate(24, 0)">
                            <path d="M 76 0 A 76 76 0 0 1 2.4359198954458052 75.96095243125228 L 0 0z" fill="#33c9e4" stroke="white" stroke-width="1" />
                            <path d="M 2.4359198954458052 75.96095243125228 A 76 76 0 0 1 -75.84384984902556 4.869336718534205 L 0 0z" fill="#459b69" stroke="white" stroke-width="1" />
                            <path d="M -75.84384984902556 4.869336718534205 A 76 76 0 1 1 76 -1.8614631347039768E-14 L 0 0z" fill="#33bca5" stroke="white" stroke-width="1" />
                        </g>
                        <g>
                            <rect x="-68" y="80" width="10" height="10" stroke="white" fill="#33c9e4" />
                            <text x="-54" y="90">
                                Foo
                            </text>
                            <rect x="-68" y="94" width="10" height="10" stroke="white" fill="#459b69" />
                            <text x="-54" y="104">
                                Bar
                            </text>
                            <rect x="-68" y="108" width="10" height="10" stroke="white" fill="#33bca5" />
                            <text x="-54" y="118">
                                Bar
                            </text>
                        </g>
                    </svg>""".trimIndent(),
                render.render()
        )
    }

    @Test
    fun `should render a piechart with a single non null value`() {
        val render = PieChart(
                Value("Foo", 0),
                Value("Bar", 12),
                Value("Bar", 0)
        ).toSvg(Size(200.0, 200.0))

        assertEquals(
                """
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="204" height="204" viewBox="-78 -78 204 204">
                        <circle cx="24" cy="0" r="76" fill="#33c9e4" stroke="white" stroke-width="1" />
                        <circle cx="24" cy="0" r="76" fill="#33c9e4" stroke="white" stroke-width="1" />
                        <circle cx="24" cy="0" r="76" fill="#33c9e4" stroke="white" stroke-width="1" />
                        <g>
                            <rect x="-68" y="80" width="10" height="10" stroke="white" fill="#33c9e4" />
                            <text x="-54" y="90">
                                Foo
                            </text>
                            <rect x="-68" y="94" width="10" height="10" stroke="white" fill="#459b69" />
                            <text x="-54" y="104">
                                Bar
                            </text>
                            <rect x="-68" y="108" width="10" height="10" stroke="white" fill="#33bca5" />
                            <text x="-54" y="118">
                                Bar
                            </text>
                        </g>
                    </svg>""".trimIndent(),
                render.render()
        )
    }
}
