package com.gitlab.kigelia.charts.linechart

data class Value(val label: Any, val value: Double)
