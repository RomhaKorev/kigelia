package com.gitlab.kigelia.charts.linechart

import com.gitlab.kigelia.core.dsl.common.AltColors
import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.Svg
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.*
import com.gitlab.kigelia.core.dsl.svg.svg



fun LineChart.toSvg(size: Size): Svg {
    return svg {
        viewbox {
            minx(0)
            miny(0)
            width(size.width)
            height(size.height)
        }
        geometry {
            width(size.width)
            height(size.height)
        }
        var chartSize = size - Size(40.0, 0.0)

        val longestLabelLength = axis.labels().maxOf { it.toString().length } * 8.0
        val labelSpacing = (axis.getPosition(axis.horizontal.tick * 2, 0.0).toAbsolute(chartSize) - axis.getPosition(axis.horizontal.tick, 0.0).toAbsolute(chartSize)).x

        val rotateLabels = longestLabelLength > labelSpacing

        chartSize -= Size(0.0, longestLabelLength * 4.0)

        g {
            "font-size"(6)
            transform {
                translate(Point(35.0, 5.0))
            }

            drawAxis(chartSize, this@toSvg.axis, rotateLabels)
            series.mapIndexed { i, serie ->
                drawSerie(chartSize, serie, AltColors[i])
            }
        }
    }
}


fun SvgElement.drawSerie(
    size: Size,
    serie: Serie,
    color: AltColors
): SvgElement {
    return g {
        "stroke"(color)
        "fill"("none")
        val points = serie.markers
            .map { it.position.toAbsolute(size) }

        path {
            move(points.first())
            points.drop(1).forEach {
                lineTo(it)
            }
        }
        points.forEach { p ->
            circle {
                cx(p.x)
                cy(p.y)
                r(2)
                "stroke-width"(0.75)
                "fill"("white")
            }
        }
    }
}


fun SvgElement.drawAxis(
    chartSize: Size,
    axis: LineChartAxis,
    rotateLabels: Boolean
): SvgElement {
    return g {
        "stroke"(AltColors.Gray)
        "stroke-width"(0.25)
        line {
            x1(0)
            y1(-5)
            x2(0)
            y2(chartSize.height)
        }
        line {
            x1(0)
            y1(chartSize.height)
            x2(chartSize.width + 5)
            y2(chartSize.height)
        }

        val center = RelativePosition(
            0.0,
            axis.vertical(axis.vertical.minimum())
        ).toAbsolute(chartSize)

        axis.horizontal.forEachTick { value, label ->
            val p = RelativePosition(value, 1.0).toAbsolute(chartSize)
            line {
                x1(p.x)
                y1(center.y - 2)
                x2(p.x)
                y2(center.y + 2)
            }

            text {
                x(p.x)
                y(center.y + 8)
                - "$label"
                "stroke"("none")
                "text-anchor"("middle")
                if (rotateLabels) {
                    "transform"("rotate(-30, ${p.x}, ${center.y + 4})")
                    "text-anchor"("end")
                }
            }
        }

        axis.vertical.forEachTick { value ->
            val p = RelativePosition(0.0, value).toAbsolute(chartSize)
            line {
                x1(center.x - 2)
                y1(p.y)
                x2(center.x + 2)
                y2(p.y)
            }
            line {
                "stroke"(AltColors.LightGray)
                "stroke-width"(0.05)
                x1(center.x)
                y1(p.y)
                x2(center.x + chartSize.width + 5)
                y2(p.y)
            }
            text {
                "text-anchor"("end")
                "stroke"("none")
                x(center.x - 5)
                y(p.y)
                - "$value"
            }
        }
    }
}

fun <T> LabelAxis.forEachTick(f: (value: Double, label: Any) -> T) {
    var current = minimum()
    while (current <= maximum()) {
        f(this(current), this.labelAt(current))
        current += tick
    }
}

fun <T> RangeAxis.forEachTick(f: (value: Double) -> T) {
    var current = minimum()
    while (current <= maximum()) {
        f(this(current))
        current += tick
    }
}
