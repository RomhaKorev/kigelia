package com.gitlab.kigelia.charts.linechart

data class Marker(val position: RelativePosition, val label: String, val value: Double)
