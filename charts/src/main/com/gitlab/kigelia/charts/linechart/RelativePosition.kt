package com.gitlab.kigelia.charts.linechart

import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.geometry.Point

data class RelativePosition(val x: Double, val y: Double) {
    fun toAbsolute(size: Size): Point {
        return Point(size.width * x, size.height - size.height * y)
    }
}
