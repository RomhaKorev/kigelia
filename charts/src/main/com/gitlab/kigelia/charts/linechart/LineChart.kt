package com.gitlab.kigelia.charts.linechart

class LineChartAxis {
    val horizontal = LabelAxis()
    val vertical = RangeAxis(0, 0)

    fun setLabels(labels: List<Any>) {
        horizontal.labels = labels
    }

    fun labels(): List<Any> = horizontal.labels

    fun resizeVertically(minimum: Double, maximum: Double) {
        vertical.resize(minimum, maximum)
    }

    fun getPosition(x: Double, y: Double): RelativePosition {
        return RelativePosition(horizontal(x), vertical(y))
    }
}

class LineChart private constructor(private val valueSeries: MutableList<Serie>) {
    constructor(): this(mutableListOf())

    val series: List<Serie>
        get() = valueSeries

    val axis = LineChartAxis()

    fun with(name: String, values: List<Value>): LineChart {

        axis.setLabels(values.map { it.label })
        axis.resizeVertically(values.minimum(), values.maximum())

        valueSeries.add(Serie(name, values, axis))
        return this
    }
}

fun List<Value>.minimum() = this.minOf { it.value }
fun List<Value>.maximum() = this.maxOf { it.value }
