package com.gitlab.kigelia.charts.linechart

import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

interface Axis {
    fun minimum(): Double
    fun maximum(): Double

    operator fun invoke(value: Number): Double {
        return (value.toDouble() - minimum()) / abs(maximum() - minimum())
    }

}

class LabelAxis: Axis {
    var labels: List<Any> = listOf()

    fun labelAt(value: Number): Any {
        return labels[value.toInt()]
    }

    override fun minimum(): Double {
        return 0.0
    }

    override fun maximum(): Double {
        return labels.size - 1.0
    }

    val tick: Double = 1.0

}

class RangeAxis private constructor(private var min: Double, private var max: Double): Axis {

    private var forcedTick: Double? = null

    override fun minimum() = min

    override fun maximum() = max

    constructor(minimum: Number, maximum: Number): this(
            min(minimum.toDouble(), maximum.toDouble()),
            max(minimum.toDouble(), maximum.toDouble())
    )

    fun resize(minimum: Double, maximum: Double) {
        this.min = min(this.min, minimum)
        this.max = max(this.max, maximum)
    }

    fun tick(value: Double): RangeAxis {
        forcedTick = value
        return this
    }

    val tick: Double
        get() {
            if (forcedTick != null)
                return forcedTick!!

            var orderMin = calculateOrder( min )
            var orderMax = calculateOrder( max )

            if ( floor( abs(min) / orderMin ) > 12.0 ) {
                orderMin *= 10.0;
            } else if ( min == 0.0 ) {
                orderMin = -1.0
            }

            if ( floor( abs(max) / orderMax ) > 12 ) {
                orderMax *= 10.0
            } else if ( max == 0.0 ) {
                orderMax = -1.0
            }

            return max(orderMin, orderMax)
    }
}


fun calculateOrder(value: Double): Double
{
	var order = 1.0
	var v = abs( value )
	if ( v > 10 ) {
		while ( v > 1 ) {
			order *= 10.0
			v /= 10.0
		}

		if ( value < ( order * 0.75 ) )
			order /= 10.0
		order /= 10.0
	} else if ( v > 0 && v < 1 ) {
		while ( v < 1 ) {
			order /= 10.0
			v *= 10.0
		}
	} else if ( value == 1.0 ) {
		order = 0.1
	}
	return order
}