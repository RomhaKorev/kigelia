package com.gitlab.kigelia.charts.linechart

class Serie(val name: String, values: List<Value>, private val axis: LineChartAxis) {
    val markers = values.mapIndexed { index, value ->
                        Marker(
                                axis.getPosition(index.toDouble(), value.value),
                                value.label.toString(),
                                value.value
                        )
                  }
}
