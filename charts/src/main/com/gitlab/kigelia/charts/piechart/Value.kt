package com.gitlab.kigelia.charts.piechart

data class Value(val label: String, val value: Number)
