package com.gitlab.kigelia.charts.piechart

data class Angle private constructor(val radians: Double) {

    val degrees: Double
        get() = radians * 180 / Math.PI

    operator fun plus(other: Angle): Angle {
        return Angle(this.radians + other.radians)
    }
    companion object {
        fun inDegrees(v: Number): Angle {
            return Angle(v.toDouble() * Math.PI/180)
        }

        fun inRadians(v: Number): Angle {
            return Angle(v.toDouble())
        }
    }
}

data class Slice(val value: Value, val startAngle: Angle, val spanAngle: Angle) {
    val endAngle: Angle
        get() = startAngle + spanAngle
}
