package com.gitlab.kigelia.charts.piechart

import com.gitlab.kigelia.core.dsl.common.AltColors
import com.gitlab.kigelia.core.geometry.Size
import com.gitlab.kigelia.core.geometry.Point
import com.gitlab.kigelia.core.dsl.svg.Svg
import com.gitlab.kigelia.core.dsl.svg.SvgElement
import com.gitlab.kigelia.core.dsl.svg.elements.*
import com.gitlab.kigelia.core.dsl.svg.invoke
import com.gitlab.kigelia.core.dsl.svg.svg
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin

fun PieChart.toSvg(size: Size): Svg {
    val w = size.width
    val h = size.height - 16 * slices().count()
    val radius = min(w, h ) / 2.0
    return svg {
        viewbox {
            minx(-radius - 2)
            miny(-radius - 2)
            width(size.width + 4)
            height(size.height + 4)
        }
        geometry {
            width(size.width + 4)
            height(size.height + 4)
        }
        val legend = G()
        if (slices().count { it.value.value != 0 } == 1) {
            slices().mapIndexed { index, slice ->
                if (slice.value.value != 0.0) {
                    circle {
                        cx((w - (radius * 2)) / 2)
                        cy((h - (radius * 2)) / 2)
                        r(radius)
                        "fill"(AltColors[0])
                        "stroke"("white")
                        "stroke-width"(1)
                    }
                }
                slice.toLegendSvg(legend, -radius + 8, radius + 14 * (1 + index), AltColors[index])
            }
        } else {
            g {
                transform {
                    translate(Point((w - (radius * 2)) / 2, (h - (radius * 2)) / 2))
                }
                slices().mapIndexed { index, slice ->
                    slice.toSvg(this, radius, AltColors[index])
                    slice.toLegendSvg(legend, -radius + 8, radius + 14 * (1 + index), AltColors[index])
                }
            }
        }
        this.add(legend)
    }
}

fun Slice.toLegendSvg(parent: SvgElement, x: Double, y: Double, color: AltColors): SvgElement {
    parent.rect {
        geometry {
            x(x)
            y(y - 10)
            width(10)
            height(10)
        }
        "stroke"("white")
        "fill"(color)
    }
    val u = parent.text {
        "x"(x + 14)
        "y"(y)
        - value.label
    }
    return u
}


fun Slice.toSvg(parent: SvgElement, radius: Double, color: AltColors): SvgElement {
    val t = startAngle.radians
    val dt = spanAngle.radians

    val x1 = radius * cos(t)
    val y1 = radius * sin(t)

    val x2 = radius * cos(t + dt)
    val y2 = radius * sin(t + dt)
    val fA = if (dt > Math.PI) 1 else 0
    val fS = if (dt > 0.0) 1 else 0
    val u = "path" {
        "d"("M $x1 $y1 A $radius $radius 0 $fA $fS $x2 $y2 L 0 0z")
        "fill"(color)
        "stroke"("white")
        "stroke-width"(1)
    }

    parent.add(u)
    return u
}
