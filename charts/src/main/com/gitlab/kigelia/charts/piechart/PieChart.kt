package com.gitlab.kigelia.charts.piechart

class PieChart(private val values: List<Value>) {

    constructor(vararg values: Value): this(values.toList())

    private val total: Double
        get() = values.sumOf { it.value.toDouble() }

    fun slices(): List<Slice> {
        var angle = 0.0
        return values.map {
            val spanAngle = (it.value.toDouble() / total) * 360.0
            val slice = Slice(it, startAngle = Angle.inDegrees(angle), Angle.inDegrees(spanAngle))
            angle += spanAngle
            slice
        }
    }
}
